/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/
#include "astra/Float32DataSource.h"
#include "astra/PlatformDepSystemCode.h"
#include <limits>

namespace astra {

	CFloat32DataSource::CFloat32DataSource() : m_bInitialized(false), m_eMode(NONE) {

	}
	CFloat32DataSource::~CFloat32DataSource() {}

	CFloat32DataFile::CFloat32DataFile() {

	}
	CFloat32DataFile::~CFloat32DataFile() {
		close();
	}
	void CFloat32DataFile::open(std::string _sFileName, CFloat32DataSource::ECapability _eMode) {
		ASTRA_ASSERT(!m_bInitialized);
		switch(_eMode) {
			case READ:
				m_pFile = std::fopen(_sFileName.c_str(), "rb");
				break;
			case WRITE:
				m_pFile = std::fopen(_sFileName.c_str(), "wb");
				break;
			case OVERWRITE:
				m_pFile = std::fopen(_sFileName.c_str(), "w+b");
				break;
		}
		ASTRA_ASSERT(m_pFile);
		m_sFileName = _sFileName;
		m_eMode = _eMode;
	}
	void CFloat32DataFile::close() {
		if (m_pFile) {
			std::fclose(m_pFile);
			m_pFile = 0;
			m_sFileName.clear();
		}
	}

	CFloat32RawDataFile::CFloat32RawDataFile() : m_uiDimensions(0u), m_iSize(0), m_offset(0) {

	}

	CFloat32RawDataFile::~CFloat32RawDataFile() {
		close();
	}

	void CFloat32RawDataFile::open(std::string _sFileName, CFloat32DataSource::ECapability _eMode) {
		ASTRA_ASSERT(!m_bInitialized);
		CFloat32DataFile::open(_sFileName, _eMode);
		ASTRA_ASSERT(m_pFile);
		if (m_eMode & READ) {
			std::size_t iCount;
			// read the number of dimensions
			iCount = std::fread(&m_uiDimensions, sizeof(std::uint32_t), 1u, m_pFile);
			ASTRA_ASSERT(iCount == 1);
			// read the individual sizes of the dimensions
			m_vSizes.resize(m_uiDimensions);
			iCount = std::fread(&m_vSizes[0], sizeof(std::uint32_t), m_uiDimensions, m_pFile);
			ASTRA_ASSERT(iCount == m_uiDimensions);
			// update variables
			m_iSize = 1;
			for (const std::uint32_t& i : m_vSizes) {
				m_iSize *= i;
			}
			m_offset = sizeof(std::uint32_t) * (m_uiDimensions + 1u);
		} else {
			// update variables
			ASTRA_ASSERT(m_uiDimensions > 0 && "Tried to open a file for exclusive writing, but the dimensions were not set.");
			ASTRA_ASSERT(m_uiDimensions == m_vSizes.size() && "Tried to open a file for exclusive writing, but the dimensions vector has an incorrect size.");
			m_iSize = 1;
			for (const std::uint32_t& i : m_vSizes) {
				ASTRA_ASSERT(i != 0 && "Tried to open a file for exclusive writing, but one dimension has size 0");
				m_iSize *= i;
			}
			m_offset = sizeof(std::uint32_t) * (m_uiDimensions + 1u);
			// write the header
			std::size_t iCount;
			// read the number of dimensions
			iCount = std::fwrite(&m_uiDimensions, sizeof(std::uint32_t), 1u, m_pFile);
			ASTRA_ASSERT(iCount == 1);
			// read the individual sizes of the dimensions
			m_vSizes.resize(m_uiDimensions);
			iCount = std::fwrite(&m_vSizes[0], sizeof(std::uint32_t), m_uiDimensions, m_pFile);
			ASTRA_ASSERT(iCount == m_uiDimensions);
		}
		// done
		m_bInitialized = true;
	}

	void CFloat32RawDataFile::close() {
		CFloat32DataFile::close();
		m_uiDimensions = 0u;
		m_iSize = 0;
		m_offset = 0;
		m_vSizes.clear();
		m_bInitialized = false;
		m_eMode = NONE;
	}

	void CFloat32RawDataFile::readData(std::size_t _iStart, std::size_t _iAmount, float32* _pfData) {
		//std::cout << "Reading data:\n";
		//std::cout << "  offset: " << _iStart << '\n';
		//std::cout << "  amount: " << _iAmount << '\n';
		//std::cout << "  to: " << _pfData << '\n';
		if (!(m_eMode & READ)) return;
		CPlatformDepSystemCode::fseek64(m_pFile, m_offset + _iStart * sizeof(float32), SEEK_SET);
		fread(_pfData, sizeof(float32), _iAmount, m_pFile);
		//std::cout << "  first value: " << _pfData[0] << '\n';
	}
	void CFloat32RawDataFile::writeData(std::size_t _iStart, std::size_t _iAmount, const float32* _pfData) {
		if (!(m_eMode & WRITE)) return;
		_iAmount = std::min(_iAmount, m_iSize - _iStart);
		CPlatformDepSystemCode::fseek64(m_pFile, m_offset + _iStart * sizeof(float32), SEEK_SET);
		fwrite(_pfData, sizeof(float32), _iAmount, m_pFile);
	}
	int CFloat32RawDataFile::readDimensionCount() {
		ASTRA_ASSERT(m_bInitialized);
		return m_uiDimensions;
	}
	int CFloat32RawDataFile::readSize() {
		ASTRA_ASSERT(m_bInitialized);
		return m_iSize;
	}
	int CFloat32RawDataFile::readDimensionSize(int _iDim) {
		ASTRA_ASSERT(m_bInitialized);
		ASTRA_ASSERT(_iDim >= 0 && size_t(_iDim) < m_uiDimensions);
		return m_vSizes[_iDim];
	}

	void CFloat32RawDataFile::setDimensionCount(int _iDims) {
		ASTRA_ASSERT(!m_bInitialized);
		m_uiDimensions = _iDims;
		m_vSizes.resize(_iDims);
	}
	void CFloat32RawDataFile::setDimensionSize(int _iDim, int _iSize) {
		ASTRA_ASSERT(!m_bInitialized);
		ASTRA_ASSERT(_iDim >= 0 && size_t(_iDim) < m_uiDimensions);
		m_vSizes[_iDim] = _iSize;
	}

	CFloat32DataSource::ECapability CFloat32RawDataFile::getCapability() {
		return CFloat32DataSource::ECapability(CFloat32DataSource::READ | CFloat32DataSource::WRITE | CFloat32DataSource::OVERWRITE);
	}
	CFloat32DataSource* CFloat32RawDataFile::makeCopy() const {
		ASTRA_ASSERT(m_bInitialized);
		CFloat32RawDataFile* copy = new CFloat32RawDataFile();
		copy->m_bInitialized = m_bInitialized;
		copy->m_iSize = m_iSize;
		copy->m_offset = m_offset;
		copy->m_sFileName = m_sFileName;
		copy->m_uiDimensions = m_uiDimensions;
		copy->m_vSizes = m_vSizes;
		copy->m_pFile = std::fopen(m_sFileName.c_str(), "rb");
		copy->m_eMode = m_eMode;
		return copy;
	}

	CFloat32DataBufferedFile::CFloat32DataBufferedFile() : m_iBufferSize(iDefaultBufferSize) {
		_clear();
		m_bInitialized = false;
	}
	CFloat32DataBufferedFile::CFloat32DataBufferedFile(CFloat32DataSource* _pSource) : m_iBufferSize(iDefaultBufferSize) {
		_clear();
		m_bInitialized = initialize(_pSource);
	}
	CFloat32DataBufferedFile::~CFloat32DataBufferedFile() {
		if (m_bInitialized)
		{
			_unInit();
		}
	}

	void CFloat32DataBufferedFile::_allocate() {
		// basic checks
		ASTRA_ASSERT(!m_bInitialized);

		ASTRA_ASSERT(m_iBufferSize > 0);
		ASTRA_ASSERT(m_buffer.m_ppfBuffers[0] == NULL);
		ASTRA_ASSERT(m_buffer.m_ppfBuffers[1] == NULL);
		// allocate contiguous block
#ifdef _MSC_VER
		m_buffer.m_ppfBuffers[0] = (float32*)_aligned_malloc(sizeof(float32) * m_iReadSize, 16);
		m_buffer.m_ppfBuffers[1] = (float32*)_aligned_malloc(sizeof(float32) * m_iReadSize, 16);
		memset(m_buffer.m_ppfBuffers[0], 0, sizeof(float32) * m_iReadSize);
		memset(m_buffer.m_ppfBuffers[1], 0, sizeof(float32) * m_iReadSize);
#else
		int ret = posix_memalign((void**)&m_buffer.m_ppfBuffers[0], 16, sizeof(float32) * m_iReadSize);
		ASTRA_ASSERT(ret == 0);
		ret = posix_memalign((void**)&m_buffer.m_ppfBuffers[1], 16, sizeof(float32) * m_iReadSize);
		ASTRA_ASSERT(ret == 0);
#endif
	}

	void CFloat32DataBufferedFile::_freeData() {
		// basic checks
		ASTRA_ASSERT(m_buffer.m_ppfBuffers[0] != NULL);
		ASTRA_ASSERT(m_buffer.m_ppfBuffers[1] != NULL);

		// free memory for data block
#ifdef _MSC_VER
		_aligned_free(m_buffer.m_ppfBuffers[0]);
		_aligned_free(m_buffer.m_ppfBuffers[1]);
#else
		free(m_buffer.m_ppfBuffers[0]);
		free(m_buffer.m_ppfBuffers[1]);
#endif
		m_buffer.m_ppfBuffers[0] = NULL;
		m_buffer.m_ppfBuffers[1] = NULL;
	}

	void CFloat32DataBufferedFile::_clear() {
		m_buffer.m_ppfBuffers[0] = 0;
		m_buffer.m_ppfBuffers[1] = 0;
		m_buffer.m_iCurrentHead = 0;
		m_buffer.m_changedFlags = 0x00;
		m_buffer.m_pSource = 0;
		m_buffer.m_future = std::future<void>();
	}

	void CFloat32DataBufferedFile::_unInit() {
		ASTRA_ASSERT(m_bInitialized);
		//make sure we're not doing any asynchronous operations anymore
		if (m_buffer.m_future.valid()) m_buffer.m_future.get();
		if (m_bChanged) {
			if (m_buffer.m_iCurrentHead < m_iSize) {
				m_buffer.m_pSource->writeData(m_buffer.m_iCurrentHead, m_iReadSize, m_buffer.m_ppfBuffers[0]);
			}
			m_bChanged = false;
		}

		_freeData();
		_clear();
		m_bInitialized = false;
		m_bChanged = false;
	}

	bool CFloat32DataBufferedFile::initialize(CFloat32DataSource* _pSource) {
		// basic checks
		ASTRA_ASSERT(_pSource);

		if (m_bInitialized)
		{
			_unInit();
		}
		m_bChanged = false;
		m_buffer.m_iCurrentHead = std::numeric_limits<std::size_t>::max();
		m_buffer.m_iBaseOffset = 0u;
		m_buffer.m_future = std::future<void>();
		m_buffer.m_changedFlags = 0x00;
		m_buffer.m_pSource = _pSource;

		int iDim = m_buffer.m_pSource->readDimensionCount();
		ASTRA_ASSERT(iDim > 1);
		m_iSize = m_buffer.m_pSource->readSize();
		m_iSliceSize = m_iSize / m_buffer.m_pSource->readDimensionSize(iDim - 1);
		m_iReadSize = m_iSliceSize * m_iBufferSize;
		

		_allocate();
		m_bInitialized = true;
		return true;
	}

	void CFloat32DataBufferedFile::_update2ndBuffer(std::size_t _iWriteStart, std::size_t _iReadStart) {
		if (m_bChanged) {
			if (_iWriteStart < m_iSize) {
				m_buffer.m_pSource->writeData(_iWriteStart, m_iReadSize, m_buffer.m_ppfBuffers[1]);
			}
			m_bChanged = false;
		}
		if (_iReadStart < m_iSize) {
			m_buffer.m_pSource->readData(_iReadStart, m_iReadSize, m_buffer.m_ppfBuffers[1]);
		}
	}

	float32* CFloat32DataBufferedFile::readSlice(std::size_t _iSliceIndex) {
		ASTRA_ASSERT(m_bInitialized);
		size_t iIndex = _iSliceIndex * m_iSliceSize;
		if (iIndex >= m_buffer.m_iCurrentHead && iIndex < m_buffer.m_iCurrentHead + m_iReadSize) {
			return m_buffer.m_ppfBuffers[0] + iIndex - m_buffer.m_iCurrentHead;
		} else if (iIndex >= m_buffer.m_iCurrentHead + m_iReadSize && iIndex < m_buffer.m_iCurrentHead + 2 * m_iReadSize) {
			//wait for potential previous read to end
			if (m_buffer.m_future.valid()) m_buffer.m_future.get();
			//swap buffers
			std::swap(m_buffer.m_ppfBuffers[0], m_buffer.m_ppfBuffers[1]);
			//update m_iCurrentHead
			std::size_t oldHead = m_buffer.m_iCurrentHead;
			m_buffer.m_iCurrentHead += m_iReadSize;
			//async read in second buffer if necessary
			if (m_bChanged || m_buffer.m_iCurrentHead + m_iReadSize  < m_iSize) {
				m_buffer.m_future = std::async(std::launch::async, std::mem_fn(&CFloat32DataBufferedFile::_update2ndBuffer),
											   this, oldHead, m_buffer.m_iCurrentHead + m_iReadSize);
			}
			//return value
			return m_buffer.m_ppfBuffers[0] + iIndex - m_buffer.m_iCurrentHead;
		} else {
			//wait for potential previous read to end
			if (m_buffer.m_future.valid()) m_buffer.m_future.get();
			//swap buffers
			std::swap(m_buffer.m_ppfBuffers[0], m_buffer.m_ppfBuffers[1]);
			//read the correct slice in the first buffer
			size_t iStartIndex = iIndex - (iIndex % m_iReadSize);
			m_buffer.m_pSource->readData(iStartIndex, m_iReadSize, m_buffer.m_ppfBuffers[0]);
			//update m_iCurrentHead
			std::size_t oldHead = m_buffer.m_iCurrentHead;
			m_buffer.m_iCurrentHead = iStartIndex;
			//async read in second buffer if necessary
			if (m_bChanged || m_buffer.m_iCurrentHead + m_iReadSize  < m_iSize) {
				m_buffer.m_future = std::async(std::launch::async, std::mem_fn(&CFloat32DataBufferedFile::_update2ndBuffer),
											   this, oldHead, m_buffer.m_iCurrentHead + m_iReadSize);
			}
			//return value
			return m_buffer.m_ppfBuffers[0] + iIndex - m_buffer.m_iCurrentHead;
		}

	}
	float32 CFloat32DataBufferedFile::readValue(std::size_t _iValueIndex) {
		std::size_t iSliceindex = _iValueIndex - (_iValueIndex % m_iReadSize);
		float32* buf = readSlice(iSliceindex);
		return buf[_iValueIndex - iSliceindex];
	}
	void CFloat32DataBufferedFile::writeSlice(std::size_t _iSliceIndex, const float32* _pfData) {
		ASTRA_ASSERT(m_bInitialized);
		size_t iIndex = _iSliceIndex * m_iSliceSize;
		//bool bWasRead = iIndex >= m_buffer.m_iCurrentHead && iIndex < m_buffer.m_iCurrentHead + m_iReadSize;
		//ASTRA_ASSERT(bWasRead && "Before writing a slice, it must have been read from file first");
		if (!(iIndex >= m_buffer.m_iCurrentHead && iIndex < m_buffer.m_iCurrentHead + m_iReadSize)) {
			readSlice(_iSliceIndex);
		}
		//wait for potential previous read to end
		if (m_buffer.m_future.valid()) m_buffer.m_future.get();
		// copy data into current buffer
		memcpy(m_buffer.m_ppfBuffers[0] + iIndex - m_buffer.m_iCurrentHead, _pfData, m_iSliceSize * sizeof(float32));
		m_bChanged = true;
	}
	void CFloat32DataBufferedFile::writeValue(std::size_t _iValueIndex, float32 _fValue) {
		std::size_t iSliceindex = _iValueIndex - (_iValueIndex % m_iReadSize);
		float32* buf = readSlice(iSliceindex);
		buf[_iValueIndex - iSliceindex] = _fValue;
		writeSlice(iSliceindex, buf);
	}


	CFloat32DataMultiBufferedFile::CFloat32DataMultiBufferedFile() : m_iBufferSize(iDefaultBufferSize) {
		_clear();
		m_bInitialized = false;
	}
	CFloat32DataMultiBufferedFile::CFloat32DataMultiBufferedFile(CFloat32DataSource* _pSource) : m_iBufferSize(iDefaultBufferSize) {
		_clear();
		m_bInitialized = initialize(_pSource);
	}
	CFloat32DataMultiBufferedFile::~CFloat32DataMultiBufferedFile() {
		if (m_bInitialized)
		{
			_unInit();
		}
	}

	void CFloat32DataMultiBufferedFile::_allocate() {
		// basic checks
		ASTRA_ASSERT(!m_bInitialized);

		ASTRA_ASSERT(m_iBufferSize > 0);
		for (const CFloat32DataBuffer<std::shared_future>& buffer : m_buffers) {
			ASTRA_ASSERT(buffer.m_ppfBuffers[0] == NULL);
			ASTRA_ASSERT(buffer.m_ppfBuffers[1] == NULL);
			ASTRA_ASSERT(buffer.m_pSource == NULL);
		}
		// allocate contiguous block
		for (CFloat32DataBuffer<std::shared_future>& buffer : m_buffers) {
	#ifdef _MSC_VER
			buffer.m_ppfBuffers[0] = (float32*)_aligned_malloc(sizeof(float32) * m_iReadSize, 16);
			buffer.m_ppfBuffers[1] = (float32*)_aligned_malloc(sizeof(float32) * m_iReadSize, 16);
	#else
			int ret = posix_memalign((void**)&buffer.m_ppfBuffers[0], 16, sizeof(float32) * m_iReadSize);
			ASTRA_ASSERT(ret == 0);
			ret = posix_memalign((void**)&buffer.m_ppfBuffers[1], 16, sizeof(float32) * m_iReadSize);
			ASTRA_ASSERT(ret == 0);
#endif
			buffer.m_pSource = m_pSource->makeCopy();
		}
	}

	void CFloat32DataMultiBufferedFile::_freeData() {
		// basic checks
		for (const CFloat32DataBuffer<std::shared_future>& buffer : m_buffers) {
			ASTRA_ASSERT(buffer.m_ppfBuffers[0] != NULL);
			ASTRA_ASSERT(buffer.m_ppfBuffers[1] != NULL);
		}

		// free memory for data block

		for (CFloat32DataBuffer<std::shared_future>& buffer : m_buffers) {
#ifdef _MSC_VER
			_aligned_free(buffer.m_ppfBuffers[0]);
			_aligned_free(buffer.m_ppfBuffers[1]);
#else
			free(m_buffer.m_ppfBuffers[0]);
			free(m_buffer.m_ppfBuffers[1]);
#endif
			delete buffer.m_pSource;
			buffer.m_ppfBuffers[0] = NULL;
			buffer.m_ppfBuffers[1] = NULL;
			buffer.m_pSource = 0;
		}
	}

	void CFloat32DataMultiBufferedFile::_clear() {
		for (CFloat32DataBuffer<std::shared_future>& buffer : m_buffers) {
			buffer.m_ppfBuffers[0] = NULL;
			buffer.m_ppfBuffers[1] = NULL;
			buffer.m_iCurrentHead = 0;
			buffer.m_iBaseOffset = 0u;
			buffer.m_changedFlags = 0x00;
			buffer.m_future = std::future<void>();
			buffer.m_pSource = 0;
		}
	}

	void CFloat32DataMultiBufferedFile::_unInit() {
		ASTRA_ASSERT(m_bInitialized);

		_freeData();
		_clear();
		m_bInitialized = false;
	}

	bool CFloat32DataMultiBufferedFile::initialize(CFloat32DataSource* _pSource) {
		// basic checks
		ASTRA_ASSERT(_pSource);

		if (m_bInitialized)
		{
			_unInit();
		}
		m_pSource = _pSource;

		int iDim = m_pSource->readDimensionCount();
		ASTRA_ASSERT(iDim > 2);
		m_iSize = m_pSource->readSize() / m_pSource->readDimensionSize(iDim - 1);
		m_iSliceSize = 1;
		for (std::size_t i = 0; i < iDim - 2; ++i) {
			m_iSliceSize *= size_t(m_pSource->readDimensionSize(int(i)));
		}
		m_iReadSize = m_iSliceSize * m_iBufferSize;

		m_buffers.resize(m_pSource->readDimensionSize(iDim - 1));

		//std::cout << "image size: " << m_pSource->readDimensionSize(0) << ", "
		//	<< m_pSource->readDimensionSize(1) << ", "
		//	<< m_pSource->readDimensionSize(2) << "\n";
		//std::cout << "total size: " << m_iSize << '\n';
		//std::cout << "slice size: " << m_iSliceSize << '\n';
		//std::cout << "read size: " << m_iReadSize << '\n';
		//std::cout << "buffer amount: " << m_buffers.size() << '\n';

		std::size_t offsetCounter = 0u;
		for (CFloat32DataBuffer<std::shared_future>& buffer : m_buffers) {
			//std::cout << "buffer offset: " << offsetCounter << '\n';
			buffer.m_iCurrentHead = std::numeric_limits<std::size_t>::max();
			buffer.m_future = std::future<void>();
			buffer.m_iBaseOffset = offsetCounter;
			buffer.m_changedFlags = 0x00;
			buffer.m_ppfBuffers[0] = NULL;
			buffer.m_ppfBuffers[1] = NULL;
			offsetCounter += m_iSize;
		}

		_allocate();
		m_bInitialized = true;
		return true;
	}
	float32* CFloat32DataMultiBufferedFile::readSlice(std::size_t _iBufferIndex, std::size_t _iSliceIndex) {
		ASTRA_ASSERT(m_bInitialized);
		CFloat32DataBuffer<std::shared_future>& buffer = m_buffers[_iBufferIndex];
		//std::cout << "Fetching slice " << _iSliceIndex << " into buffer " << _iBufferIndex << '\n';
		size_t iIndex = _iSliceIndex * m_iSliceSize;
		if (iIndex >= buffer.m_iCurrentHead && iIndex < buffer.m_iCurrentHead + m_iReadSize) {
			return buffer.m_ppfBuffers[0] + iIndex - buffer.m_iCurrentHead;
		} else if (iIndex >= buffer.m_iCurrentHead + m_iReadSize && iIndex < buffer.m_iCurrentHead + 2 * m_iReadSize) {
			//wait for potential previous read to end
			if (buffer.m_future.valid()) buffer.m_future.wait();
			//swap buffers
			std::swap(buffer.m_ppfBuffers[0], buffer.m_ppfBuffers[1]);
			//std::cout << "swapped buffers_?[0] = " << buffer.m_ppfBuffers[0] << '\n';
			//std::cout << "swapped buffers_?[1] = " << buffer.m_ppfBuffers[1] << '\n';
			//update m_iCurrentHead
			buffer.m_iCurrentHead += m_iReadSize;
			//async read in second buffer if necessary
			if (buffer.m_iCurrentHead + m_iReadSize  < m_iSize) {
				buffer.m_future = std::async(std::launch::async, std::mem_fn(&CFloat32DataSource::readData),
											   buffer.m_pSource, buffer.m_iBaseOffset + buffer.m_iCurrentHead + m_iReadSize, m_iReadSize, buffer.m_ppfBuffers[1]);
				assert(buffer.m_future.valid());
			}
			//return value
			return buffer.m_ppfBuffers[0] + iIndex - buffer.m_iCurrentHead;
		} else {
			//wait for potential previous read to end
			if (buffer.m_future.valid()) buffer.m_future.wait();
			//read the correct slice in the first buffer
			buffer.m_pSource->readData(buffer.m_iBaseOffset + iIndex, m_iReadSize, buffer.m_ppfBuffers[0]);
			//update m_iCurrentHead
			buffer.m_iCurrentHead = iIndex;
			//async read in second buffer if necessary
			if (buffer.m_iCurrentHead + m_iReadSize  < m_iSize) {
				buffer.m_future = std::async(std::launch::async, std::mem_fn(&CFloat32DataSource::readData),
					buffer.m_pSource, buffer.m_iBaseOffset + buffer.m_iCurrentHead + m_iReadSize, m_iReadSize, buffer.m_ppfBuffers[1]);
				assert(buffer.m_future.valid());
			}
			//return value
			return buffer.m_ppfBuffers[0] + iIndex - buffer.m_iCurrentHead;
		}

	}
	float32 CFloat32DataMultiBufferedFile::readValue(std::size_t _iBufferIndex, std::size_t _iValueIndex) {
		std::size_t iSliceindex = _iValueIndex - (_iValueIndex % m_iReadSize);
		float32* buf = readSlice(_iBufferIndex, iSliceindex);
		return buf[_iValueIndex - iSliceindex];
	}
	void CFloat32DataMultiBufferedFile::writeSlice(std::size_t _iBufferIndex, std::size_t _iSliceIndex, const float32* _pfData) {
		// TODO:
		assert(false);
	}
	void CFloat32DataMultiBufferedFile::writeValue(std::size_t _iBufferIndex, std::size_t _iValueIndex, float32 _fValue) {
		// TODO:
		assert(false);
	}
} // namespace astra