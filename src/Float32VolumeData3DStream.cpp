/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "astra/Float32VolumeData3DStream.h"

#include <cstring>

using namespace std;

namespace astra
{

	//----------------------------------------------------------------------------------------
	// Default constructor
	CFloat32VolumeData3DStream::CFloat32VolumeData3DStream() :
		CFloat32Data3DStream()
	{
		m_pGeometry = NULL;
		m_bInitialized = false;
	}

	//----------------------------------------------------------------------------------------
	// Create an instance of the CFloat32VolumeData2D class with initialization of the data.
	CFloat32VolumeData3DStream::CFloat32VolumeData3DStream(CVolumeGeometry3D* _pGeometry, CFloat32DataBufferedFile* _pDataSource)
	{
		m_bInitialized = false;
		m_bInitialized = initialize(_pGeometry, _pDataSource);
	}


	//----------------------------------------------------------------------------------------
	// Destructor
	CFloat32VolumeData3DStream::~CFloat32VolumeData3DStream()
	{
		if (m_pGeometry){
			delete m_pGeometry;
		}
		m_pGeometry = 0;

	}

	//----------------------------------------------------------------------------------------
	// Initialization
	bool CFloat32VolumeData3DStream::initialize(CVolumeGeometry3D* _pGeometry, CFloat32DataBufferedFile* _pDataSource)
	{
		m_pGeometry = _pGeometry->clone();
		m_bInitialized = _initialize(_pDataSource);
		return m_bInitialized;
	}


	//----------------------------------------------------------------------------------------
	// Fetch a slice
	CFloat32VolumeData2D * CFloat32VolumeData3DStream::fetchSliceZ(int _iSliceIndex) const
	{
		// fetch slice of the geometry
		int iRowCount = m_pGeometry->getGridRowCount();
		int iColumnCount = m_pGeometry->getGridColCount();
		CVolumeGeometry2D volGeom(iColumnCount, iRowCount);

		// create new volume data
		CFloat32VolumeData2D* res = new CFloat32VolumeData2D(&volGeom);

		// get slice data
		float32* pSlice = getSlice(_iSliceIndex);
		// copy data
		float * pfTargetData = res->getData();
		for (int iRowIndex = 0; iRowIndex < iRowCount; iRowIndex++)
		{
			for (int iColumnIndex = 0; iColumnIndex < iColumnCount; iColumnIndex++)
			{
				int iArrayIndex = iRowIndex * iColumnCount + iColumnIndex;
				float fStoredValue = pSlice[iArrayIndex];
				pfTargetData[iArrayIndex] = fStoredValue;
			}
		}
		// return
		return res;
	}

	const float32* CFloat32VolumeData3DStream::getDataZ(int _iSliceIndex) const {
		return getSlice(_iSliceIndex);
	}

	//----------------------------------------------------------------------------------------
	// Return a slice
	void CFloat32VolumeData3DStream::returnSliceZ(int _iSliceIndex, CFloat32VolumeData2D * _pSlice)
	{
		int iRowCount = _pSlice->getGeometry()->getGridRowCount();
		int iColumnCount = _pSlice->getGeometry()->getGridColCount();

		assert(iRowCount == m_pGeometry->getGridRowCount());
		assert(iColumnCount == m_pGeometry->getGridColCount());
		writeSlice(_iSliceIndex, _pSlice->getData());
	}

	CFloat32VolumeData2D * CFloat32VolumeData3DStream::fetchSliceX(int _iColumnIndex) const
	{
		// TODO:
		assert(false);
		return NULL;
	}

	CFloat32VolumeData2D * CFloat32VolumeData3DStream::fetchSliceY(int _iRowIndex) const
	{
		// TODO:
		assert(false);
		return NULL;
	}

	void CFloat32VolumeData3DStream::returnSliceX(int _iColumnIndex, CFloat32VolumeData2D * _pSliceData)
	{
		// TODO:
		assert(false);
	}

	void CFloat32VolumeData3DStream::returnSliceY(int _iRowIndex, CFloat32VolumeData2D * _pSliceData)
	{
		// TODO:
		assert(false);
	}

	//----------------------------------------------------------------------------------------
	// Returns a specific value
	float32 CFloat32VolumeData3DStream::getVoxelValue(int _iIndex)
	{
		return getDataPoint(_iIndex);
	}

	//----------------------------------------------------------------------------------------
	// Sets a specific value
	void CFloat32VolumeData3DStream::setVoxelValue(int _iIndex, float32 _fValue)
	{
		// TODO:
		assert(false);
	}
	//----------------------------------------------------------------------------------------

	CFloat32Data3D& CFloat32VolumeData3DStream::clampMin(float32& _fMin) {
		// TODO:
		assert(false);
		return *this;
	}

	/**
	* Clamp data to maximum value
	*
	* @param _fMax maximum value
	* @return l-value
	*/
	CFloat32Data3D& CFloat32VolumeData3DStream::clampMax(float32& _fMax) {
		// TODO:
		assert(false);
		return *this;
	}

	CFloat32VolumeData3DStream& CFloat32VolumeData3DStream::operator=(const CFloat32VolumeData3DStream& _dataIn)
	{
		// TODO:
		assert(false);
		return *this;
	}

} // end namespace astra
