/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "astra/Float32DVFData3D.h"
#include <iostream>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <limits>

#ifdef _MSC_VER
#include <malloc.h>
#else
#include <cstdlib>
#endif

namespace astra {
	//----------------------------------------------------------------------------------------
	// Constructor
	CFloat32DVFData3D::CFloat32DVFData3D() : m_iWidth(0), m_iHeight(0), m_iDepth(0),
		m_fMinZ(std::numeric_limits<float32>::quiet_NaN()), m_fMaxZ(std::numeric_limits<float32>::quiet_NaN()) {
		m_bInitialized = false;
	}

	//----------------------------------------------------------------------------------------
	// Destructor.
	CFloat32DVFData3D::~CFloat32DVFData3D() {}


	int CFloat32DVFData3D::getConsecutiveDepth() const {
		return m_iDepth;
	}


	//----------------------------------------------------------------------------------------
	// Other methods
	void CFloat32DVFData3D::setMinZ(float32 _v) {
		m_fMinZ = _v;
	}
	void CFloat32DVFData3D::setMaxZ(float32 _v) {
		m_fMaxZ = _v;
	}
	float32 CFloat32DVFData3D::getMinZ() const {
		return m_fMinZ;
	}
	float32 CFloat32DVFData3D::getMaxZ() const {
		return m_fMaxZ;
	}
	bool CFloat32DVFData3D::hasMinZ() const {
		return !std::isnan(m_fMinZ);
	}
	bool CFloat32DVFData3D::hasMaxZ() const {
		return !std::isnan(m_fMinZ);
	}
} // end namespace astra
