/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "astra/CudaDeformAlgorithm3D.h"

#include "astra/Logging.h"
#include "astra/AstraObjectManager.h"

#include "../cuda/3d/astra3d.h"
#include "../cuda/3d/deform3d.h"
#include "../cuda/3d/deform3dstream.h"
#include "../cuda/3d/mem3d.h"
#include "../cuda/2d/util.h"

#include <cuda.h>

using namespace std;
using namespace astraCUDA3d;

namespace astra {

//----------------------------------------------------------------------------------------
// settings
//----------------------------------------------------------------------------------------
	void CCudaDeformAlgorithm3D::setGPUIndex(int _iGPUIndex) {
		m_iGPUIndex = _iGPUIndex;
	}
	int CCudaDeformAlgorithm3D::getGPUIndex() const {
		return m_iGPUIndex;
	}

	void CCudaDeformAlgorithm3D::setMaxMemory(std::size_t _iMaxMemory) {
		m_iMaxMemory = _iMaxMemory;
	}
	std::size_t CCudaDeformAlgorithm3D::getMaxMemory() const {
		return m_iMaxMemory;
	}

	void CCudaDeformAlgorithm3D::setInterpolationMode(EInterpolationMode _eMode) {
		m_eMode = _eMode;
	}

	EInterpolationMode CCudaDeformAlgorithm3D::getInterpolationMode() const {
		return m_eMode;
	}

	void CCudaDeformAlgorithm3D::_finishInit() {
		if (m_iGPUIndex < 0)
			m_iGPUIndex = 0;

		if (m_iMaxMemory == 0) {
			m_iMaxMemory = availableGPUMemory();
		}
	}

	bool CCudaBasicDeformAlgorithm3D::initialize(const Config& _cfg) {
		ASTRA_ASSERT(_cfg.self);
		ConfigStackCheck<CAlgorithm> CC("DeformationAlgorithm3D", this, _cfg);

		// Source Data
		XMLNode node = _cfg.self.getSingleNode("SourceDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No SourceDataId tag specified.");

		int id;
		id = node.getContentInt();
		CFloat32VolumeData3DMemory* pSource = dynamic_cast<CFloat32VolumeData3DMemory*>(CData3DManager::getSingleton().get(id));
		CC.markNodeParsed("SourceDataId");

		// Output Data
		node = _cfg.self.getSingleNode("OutputDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No OutputDataId tag specified.");
		id = node.getContentInt();
		CFloat32VolumeData3DMemory* pOutput = dynamic_cast<CFloat32VolumeData3DMemory*>(CData3DManager::getSingleton().get(id));
		CC.markNodeParsed("OutputDataId");

		// DVF Data
		node = _cfg.self.getSingleNode("DVFDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No DVFDataId tag specified.");
		id = node.getContentInt();
		CFloat32DVFData3DMemory* pDVF = dynamic_cast<CFloat32DVFData3DMemory*>(CDVFData3DManager::getSingleton().get(id));
		CC.markNodeParsed("DVFDataId");

		return initialize(pSource, pDVF, pOutput);
	}
	bool CCudaBasicDeformAlgorithm3D::initialize(CFloat32VolumeData3DMemory* _pSource,
												 CFloat32DVFData3DMemory* _pDVF,
												 CFloat32VolumeData3DMemory* _pOutput) {
		m_pSource = _pSource;
		m_pDVF = _pDVF;
		m_pOutput = _pOutput;
		m_width = m_pSource->getWidth();
		m_height = m_pSource->getHeight();
		m_depth = m_pSource->getDepth();

		return true;
	}

	void CCudaBasicDeformAlgorithm3Dfwd::run(int) {
		// finalize initialization
		_finishInit();
		// create algorithm object
		DeformAlgorithm3DFwd algo;
		SDimensions3D dims;
		dims.iVolX = m_width;
		dims.iVolY = m_height;
		dims.iVolZ = m_depth;
		algo.setDimensions(dims);
		algo.setGPUIndex(getGPUIndex());
		// allocate buffers
		algo.init();
		cudaPitchedPtr D_source, D_dvfx, D_dvfy, D_dvfz, D_target;
		algo.getBuffers(D_source, D_dvfx, D_dvfy, D_dvfz, D_target);
		// initialize buffers
		ASTRA_ASSERT(copyVolumeToDevice(m_pSource->getData(), D_source, dims));
		ASTRA_ASSERT(copyVolumeToDevice(m_pDVF->getDatax(), D_dvfx, dims));
		ASTRA_ASSERT(copyVolumeToDevice(m_pDVF->getDatay(), D_dvfy, dims));
		ASTRA_ASSERT(copyVolumeToDevice(m_pDVF->getDataz(), D_dvfz, dims));
		ASTRA_ASSERT(zeroVolumeData(D_target, dims));
		// run the algorithm
		algo.setInterpolationMode(m_eMode);
		algo.run();
		// get the result back
		copyVolumeFromDevice(m_pOutput->getData(), D_target, dims);
		// release buffers
		algo.reset();
	}

	void CCudaBasicDeformAlgorithm3Dbckwd::run(int) {
		// finalize initialization
		_finishInit();
		// create algorithm object
		DeformAlgorithm3DBckwd algo;
		SDimensions3D dims;
		dims.iVolX = m_width;
		dims.iVolY = m_height;
		dims.iVolZ = m_depth;
		algo.setDimensions(dims);
		algo.setGPUIndex(getGPUIndex());
		// allocate buffers
		algo.init();
		cudaPitchedPtr D_source, D_dvfx, D_dvfy, D_dvfz, D_target;
		algo.getBuffers(D_source, D_dvfx, D_dvfy, D_dvfz, D_target);
		// initialize buffers
		ASTRA_ASSERT(copyVolumeToDevice(m_pSource->getData(), D_source, dims));
		ASTRA_ASSERT(copyVolumeToDevice(m_pDVF->getDatax(), D_dvfx, dims));
		ASTRA_ASSERT(copyVolumeToDevice(m_pDVF->getDatay(), D_dvfy, dims));
		ASTRA_ASSERT(copyVolumeToDevice(m_pDVF->getDataz(), D_dvfz, dims));
		ASTRA_ASSERT(zeroVolumeData(D_target, dims));
		// run the algorithm
		algo.setInterpolationMode(m_eMode);
		algo.run();
		// get the result back
		copyVolumeFromDevice(m_pOutput->getData(), D_target, dims);
		// release buffers
		algo.reset();
	}

	//----------------------------------------------------------------------------------------
	// settings
	//----------------------------------------------------------------------------------------
	CCudaStreamDeformAlgorithm3Dfwd::CCudaStreamDeformAlgorithm3Dfwd() : m_iStreamSize(64)
	{}

	bool CCudaStreamDeformAlgorithm3Dfwd::initialize(const Config& _cfg) {
		ASTRA_ASSERT(_cfg.self);
		ConfigStackCheck<CAlgorithm> CC("DeformationAlgorithm3D", this, _cfg);

		// Source Data
		XMLNode node = _cfg.self.getSingleNode("SourceDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No SourceDataId tag specified.");

		int id;
		id = node.getContentInt();
		CFloat32VolumeData3D* pSource = dynamic_cast<CFloat32VolumeData3D*>(CData3DManager::getSingleton().get(id));
		CC.markNodeParsed("SourceDataId");

		// Output Data
		node = _cfg.self.getSingleNode("OutputDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No OutputDataId tag specified.");
		id = node.getContentInt();
		CFloat32VolumeData3DMemory* pOutput = dynamic_cast<CFloat32VolumeData3DMemory*>(CData3DManager::getSingleton().get(id));
		CC.markNodeParsed("OutputDataId");

		// DVF Data
		node = _cfg.self.getSingleNode("DVFDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No DVFDataId tag specified.");
		id = node.getContentInt();
		CFloat32DVFData3D* pDVF = dynamic_cast<CFloat32DVFData3D*>(CDVFData3DManager::getSingleton().get(id));
		CC.markNodeParsed("DVFDataId");

		return initialize(pSource, pDVF, pOutput);
	}
	bool CCudaStreamDeformAlgorithm3Dfwd::initialize(CFloat32VolumeData3D* _pSource,
		CFloat32DVFData3D* _pDVF,
		CFloat32VolumeData3DMemory* _pOutput) {

		m_pSource = _pSource;
		m_pDVF = _pDVF;
		m_pOutput = _pOutput;

		m_width = m_pSource->getWidth();
		m_height = m_pSource->getHeight();
		m_depth = m_pSource->getDepth();
		return true;
	}

	void CCudaStreamDeformAlgorithm3Dfwd::run(int) {
		// finalize initialization
		_finishInit();
		// create algorithm object
		astraCUDA3d::StreamDeformAlgorithm3DFwd algorithm(m_iStreamSize);
		algorithm.initialize(m_pSource, m_pDVF, m_pOutput);
		algorithm.setInterpolationMode(m_eMode);
		// run the algorithm
		algorithm.run();
	}


	bool CCudaStreamDeformAlgorithm3Dbckwd::initialize(const Config& _cfg) {
		ASTRA_ASSERT(_cfg.self);
		ConfigStackCheck<CAlgorithm> CC("DeformationAlgorithm3D", this, _cfg);

		// Source Data
		XMLNode node = _cfg.self.getSingleNode("SourceDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No SourceDataId tag specified.");

		int id;
		id = node.getContentInt();
		CFloat32VolumeData3DMemory* pSource = dynamic_cast<CFloat32VolumeData3DMemory*>(CData3DManager::getSingleton().get(id));
		CC.markNodeParsed("SourceDataId");

		// Output Data
		node = _cfg.self.getSingleNode("OutputDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No OutputDataId tag specified.");
		id = node.getContentInt();
		CFloat32VolumeData3D* pOutput = dynamic_cast<CFloat32VolumeData3D*>(CData3DManager::getSingleton().get(id));
		CC.markNodeParsed("OutputDataId");

		// DVF Data
		node = _cfg.self.getSingleNode("DVFDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No DVFDataId tag specified.");
		id = node.getContentInt();
		CFloat32DVFData3D* pDVF = dynamic_cast<CFloat32DVFData3D*>(CDVFData3DManager::getSingleton().get(id));
		CC.markNodeParsed("DVFDataId");

		return initialize(pSource, pDVF, pOutput);
	}

	bool CCudaStreamDeformAlgorithm3Dbckwd::initialize(CFloat32VolumeData3DMemory* _pSource,
													 CFloat32DVFData3D* _pDVF,
													 CFloat32VolumeData3D* _pOutput) {
		m_pSource = _pSource;
		m_pDVF = _pDVF;
		m_pOutput = _pOutput;

		m_width = m_pSource->getWidth();
		m_height = m_pSource->getHeight();
		m_depth = m_pSource->getDepth();
		return true;
	}

	void CCudaStreamDeformAlgorithm3Dbckwd::run(int) {
		// finalize initialization
		_finishInit();
		// create algorithm object
		astraCUDA3d::StreamDeformAlgorithm3DBckwd algorithm(m_iStreamSize);
		algorithm.initialize(m_pSource, m_pDVF, m_pOutput);
		algorithm.setInterpolationMode(m_eMode);
		// run the algorithm
		algorithm.run();
	}

} // namespace astra
