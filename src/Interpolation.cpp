/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "astra/Interpolation.h"

#include "astra/AstraObjectManager.h"

using namespace std;

namespace astra {
	const int SInterpolate<NEAREST_NEIGHBOR>::kernelsize = 1;
	float32 SInterpolate<NEAREST_NEIGHBOR>::interp(float32 x, float32* arg) {
		return (x > 0.0f ? arg[0] : arg[1]);
	}
	float32 SInterpolate<NEAREST_NEIGHBOR>::calcpart(float32 x, float32 v) {
		return (x < 0.5f ? v : 0.0f);
	}

	const int SInterpolate<LINEAR>::kernelsize = 1;
	float32 SInterpolate<LINEAR>::interp(float32 x, float32* arg) {
		return lerp(arg[0], arg[1], x + 0.5f);
	}
	float32 SInterpolate<LINEAR>::calcpart(float32 x, float32 v) {
		return (x < 1.0f ? v * (1.0f - x) : 0.0f);
	}

	const int SInterpolate<CUBICBSPLINE>::kernelsize = 2;
	float32 SInterpolate<CUBICBSPLINE>::interp(float32 x, float32* arg) {
		ASTRA_ASSERT(false && "not implemented yet");
		return 0.0f;
	}
	float32 SInterpolate<CUBICBSPLINE>::calcpart(float32 x, float32 v) {
		return (x < 1.0f ? v * (4.0f + 3.0f * x * x * (x - 2.0f)) :
			(x < 2.0f ? v * (8.0f - 12.0f * x + (-x + 6.0f) * x * x) : 0.0f));
	}
}