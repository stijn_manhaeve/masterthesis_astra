/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "astra/Float32DVFData3DStream.h"


namespace astra {
	CFloat32DVFData3DStream::CFloat32DVFData3DStream(CFloat32DataMultiBufferedFile* _pSource) {
		m_bInitialized = _initialize(_pSource);
	}

	bool CFloat32DVFData3DStream::_initialize(CFloat32DataMultiBufferedFile* _pSource) {
		ASTRA_ASSERT(_pSource);
		m_pSource = _pSource;
		int iDim = m_pSource->readDimensionCount();
		ASTRA_ASSERT(iDim == 4);

		m_iWidth = m_pSource->readDimensionSize(0);
		m_iHeight = m_pSource->readDimensionSize(1);
		m_iDepth = m_pSource->readDimensionSize(2);
		m_iSize = m_iWidth * m_iHeight * m_iDepth;
		return true;
	}

	int CFloat32DVFData3DStream::getConsecutiveDepth() const {
		return m_pSource->getBufferSize();
	}

	float32* CFloat32DVFData3DStream::getDispX(size_t _iz) {
		return m_pSource->readSlice(0, _iz);
	}
	const float32* CFloat32DVFData3DStream::getDispXConst(size_t _iz) const {
		return m_pSource->readSlice(0, _iz);
	}

	/** Get a pointer to the y displacement data starting from slice _iz.
	*/
	float32* CFloat32DVFData3DStream::getDispY(size_t _iz) {
		return m_pSource->readSlice(1, _iz);
	}
	const float32* CFloat32DVFData3DStream::getDispYConst(size_t _iz) const {
		return m_pSource->readSlice(1, _iz);
	}

	/** Get a pointer to the z displacement data starting from slice _iz.
	*/
	float32* CFloat32DVFData3DStream::getDispZ(size_t _iz) {
		return m_pSource->readSlice(2, _iz);
	}
	const float32* CFloat32DVFData3DStream::getDispZConst(size_t _iz) const {
		return m_pSource->readSlice(2, _iz);
	}
} // end namespace astra