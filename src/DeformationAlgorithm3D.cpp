/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "astra/DeformationAlgorithm3D.h"

#include "astra/AstraObjectManager.h"

using namespace std;

namespace astra {
	//---------------------------------------------------------------------------------------
	// Constructor
	CDeformationAlgorithm3D::CDeformationAlgorithm3D() : m_pSource(NULL),
		m_pDVF(NULL),
		m_pOutput(NULL) {}
	CDeformationAlgorithm3D::~CDeformationAlgorithm3D() {}
	//---------------------------------------------------------------------------------------
	// Destructor

	//---------------------------------------------------------------------------------------
	// Initialization - config
	bool CDeformationAlgorithm3D::initialize(const Config& _cfg) {
		ASTRA_ASSERT(_cfg.self);
		ConfigStackCheck<CAlgorithm> CC("DeformationAlgorithm3D", this, _cfg);

		// Source Data
		XMLNode node = _cfg.self.getSingleNode("SourceDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No SourceDataId tag specified.");

		int id;
		id = node.getContentInt();
		m_pSource = dynamic_cast<CFloat32VolumeData3D*>(CData3DManager::getSingleton().get(id));
		CC.markNodeParsed("SourceDataId");

		// Output Data
		node = _cfg.self.getSingleNode("OutputDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No OutputDataId tag specified.");
		id = node.getContentInt();
		m_pOutput = dynamic_cast<CFloat32VolumeData3D*>(CData3DManager::getSingleton().get(id));
		CC.markNodeParsed("OutputDataId");

		// DVF Data
		node = _cfg.self.getSingleNode("DVFDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No DVFDataId tag specified.");
		id = node.getContentInt();
		m_pDVF = CDVFData3DManager::getSingleton().get(id);
		CC.markNodeParsed("DVFDataId");

		return _check();
	}
	//---------------------------------------------------------------------------------------
	// Initialization - C++
	bool CDeformationAlgorithm3D::initialize(CFloat32VolumeData3D* _pSource,
											 CFloat32DVFData3D* _pDVF,
											 CFloat32VolumeData3D* _pOutput) {
		m_pSource = _pSource;
		m_pDVF = _pDVF;
		m_pOutput = _pOutput;

		m_eMode = DEFAULT;

		m_bIsInitialized = _check();
		return m_bIsInitialized;
	}

	//---------------------------------------------------------------------------------------
	// Initialization - checking
	bool CDeformationAlgorithm3D::_check() const {
		// check pointers
		ASTRA_CONFIG_CHECK(m_pSource, "Deformation3D", "Invalid Source Data Object.");
		ASTRA_CONFIG_CHECK(m_pOutput, "Deformation3D", "Invalid Output Data Object.");
		ASTRA_CONFIG_CHECK(m_pDVF, "Deformation3D", "Invalid DVF Data Object.");

		// check initializations
		ASTRA_CONFIG_CHECK(m_pSource->isInitialized(), "Deformation3D", "Invalid Source Data Object.");
		ASTRA_CONFIG_CHECK(m_pOutput->isInitialized(), "Deformation3D", "Invalid Output Data Object.");
		ASTRA_CONFIG_CHECK(m_pDVF->isInitialized(), "Deformation3D", "Invalid DVF Data Object.");

		// make sure the two images have the same size
		ASTRA_CONFIG_CHECK(m_pSource->getWidth() == m_pOutput->getWidth(), "Deformation3D", "Images don't have the same width.");
		ASTRA_CONFIG_CHECK(m_pSource->getHeight() == m_pOutput->getHeight(), "Deformation3D", "Images don't have the same height.");
		ASTRA_CONFIG_CHECK(m_pSource->getDepth() == m_pOutput->getDepth(), "Deformation3D", "Images don't have the same depth.");
		// success
		return true;
	}
	//---------------------------------------------------------------------------------------
	// Information - All
	map<string, boost::any> CDeformationAlgorithm3D::getInformation() {
		map<string, boost::any> res;
		res["SourceDataId"] = getInformation("SourceDataId");
		res["OutputDataId"] = getInformation("OutputDataId");
		res["DVFDataID"] = getInformation("DVFDataID");
		return mergeMap<string, boost::any>(CAlgorithm::getInformation(), res);
	};

	//---------------------------------------------------------------------------------------
	// Information - Specific
	boost::any CDeformationAlgorithm3D::getInformation(std::string _sIdentifier) {
		if (_sIdentifier == "SourceDataId")	{
			int iIndex = CData3DManager::getSingleton().getIndex(m_pSource);
			if (iIndex != 0) return iIndex;
			return std::string("not in manager");
		}
		if (_sIdentifier == "OutputDataId") {
			int iIndex = CData3DManager::getSingleton().getIndex(m_pOutput);
			if (iIndex != 0) return iIndex;
			return std::string("not in manager");
		}
		if (_sIdentifier == "DVFDataID") {
			int iIndex = CDVFData3DManager::getSingleton().getIndex(m_pDVF);
			if (iIndex != 0) return iIndex;
			return std::string("not in manager");
		}
		return CAlgorithm::getInformation(_sIdentifier);
	};
	//----------------------------------------------------------------------------------------
	// the algorithm
	void putAddItem(CFloat32VolumeData3D* _pData, int _ix, int _iy, int _iz, float32 _fValue) {
		if (_ix < 0) return;
		if (_ix >= _pData->getWidth()) return;
		if (_iy < 0) return;
		if (_iy >= _pData->getHeight()) return;
		if (_iz < 0) return;
		if (_iz >= _pData->getDepth()) return;
		int index = _ix + (_pData->getWidth() * (_iy + (_pData->getHeight() * _iz)));
		float32 fOldValue = _pData->getVoxelValue(index);
		//_ix == 66 && _iy == 155 && _iz == 160
		//fOldValue + _fValue > 1.0
		//if (_ix == 66 && _iy == 155 && _iz == 160) {
//		if (fOldValue + _fValue > 1.00001) {
//			std::cout << "Writing pixel value > 1.0\n";
//			std::cout << " old value: " << fOldValue << "\n";
//			std::cout << " added value: " << _fValue << "\n";
//			std::cout << " new value: " << (fOldValue + _fValue) << "\n";
//			std::cout << " location: " << _ix << ", " << _iy << ", " << _iz << "\n";
//			std::cout.flush();
//			//assert(false);
//		}
		_pData->setVoxelValue(index, fOldValue + _fValue);
	}

	void putItemNear(CFloat32VolumeData3D* _pData, int _ix, int _iy, int _iz, float32 _fdx, float32 _fdy, float32 _fdz, float32 _fValue) {
		//if (_fValue == 0.0f) return;
		//calculate the new position of the pixel
		float32 fresx = _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fresy = _fdy * float(_pData->getHeight()) / 2.0f;
		float32 fresz = _fdz * float(_pData->getDepth()) / 2.0f;

		float32 factualx = float(_ix) + fresx;
		float32 factualy = float(_iy) + fresy;
		float32 factualz = float(_iz) + fresz;
		//round and calculate the fractions of the four pixels
		//the value must be spread over
		int froundx = fastround2int<float32, int>(factualx);
		int froundy = fastround2int<float32, int>(factualy);
		int froundz = fastround2int<float32, int>(factualz);

		//add these values to the output image
		putAddItem(_pData, froundx, froundy, froundz, _fValue);
	}

	void putItemLin(CFloat32VolumeData3D* _pData, int _ix, int _iy, int _iz, float32 _fdx, float32 _fdy, float32 _fdz, float32 _fValue) {
		//if (_fValue == 0.0f) return;
		//calculate the new position of the pixel
		float32 fresx = _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fresy = _fdy * float(_pData->getHeight()) / 2.0f;
		float32 fresz = _fdz * float(_pData->getDepth()) / 2.0f;

		float32 factualx = float(_ix) + fresx;
		float32 factualy = float(_iy) + fresy;
		float32 factualz = float(_iz) + fresz;
		//round and calculate the fractions of the eight pixels
		//the value must be spread over
		int iroundx = fastround2int<float32, int>(factualx);
		int iroundy = fastround2int<float32, int>(factualy);
		int iroundz = fastround2int<float32, int>(factualz);

		int ileftx, itopy, ifrontz;
		float32 fleftfraction, ftopfraction, ffrontfraction;

		if (iroundx <= factualx) {
			ileftx = iroundx;
			fleftfraction = 1.0f - (factualx - iroundx);
		}
		else {
			ileftx = iroundx - 1;
			fleftfraction = (iroundx - factualx);
		}
		if (iroundy <= factualy) {
			itopy = iroundy;
			ftopfraction = 1.0f - (factualy - iroundy);
		}
		else {
			itopy = iroundy - 1;
			ftopfraction = (iroundy - factualy);
		}
		if (iroundz <= factualz) {
			ifrontz = iroundz;
			ffrontfraction = 1.0f - (factualz - iroundz);
		}
		else {
			ifrontz = iroundz - 1;
			ffrontfraction = (iroundz - factualz);
		}
		//calculate the four values added to the four pixels
		float32 a = (fleftfraction          * ftopfraction * ffrontfraction) * _fValue;
		float32 b = ((1.0f - fleftfraction) * ftopfraction * ffrontfraction) * _fValue;
		float32 c = (fleftfraction          * (1.0f - ftopfraction) * ffrontfraction) * _fValue;
		float32 d = ((1.0f - fleftfraction) * (1.0f - ftopfraction) * ffrontfraction) * _fValue;
		float32 e = (fleftfraction          * ftopfraction * (1.0f - ffrontfraction)) * _fValue;
		float32 f = ((1.0f - fleftfraction) * ftopfraction * (1.0f - ffrontfraction)) * _fValue;
		float32 g = (fleftfraction          * (1.0f - ftopfraction) * (1.0f - ffrontfraction)) * _fValue;
		float32 h = ((1.0f - fleftfraction) * (1.0f - ftopfraction) * (1.0f - ffrontfraction)) * _fValue;

//		float tt = fabs((a + b + c + d + e + f + g + h) - _fValue);
//		if (tt > 1e-5) {
//			std::cout << "tt = " << tt << std::endl;
//			ASTRA_ASSERT((a + b + c + d + e + f + g + h) == _fValue && "Voxel value calculations are off.");
//		}
		//add these values to the output image
		putAddItem(_pData, ileftx, itopy, ifrontz, a);
		putAddItem(_pData, ileftx + 1, itopy, ifrontz, b);
		putAddItem(_pData, ileftx, itopy + 1, ifrontz, c);
		putAddItem(_pData, ileftx + 1, itopy + 1, ifrontz, d);
		putAddItem(_pData, ileftx, itopy, ifrontz + 1, e);
		putAddItem(_pData, ileftx + 1, itopy, ifrontz + 1, f);
		putAddItem(_pData, ileftx, itopy + 1, ifrontz + 1, g);
		putAddItem(_pData, ileftx + 1, itopy + 1, ifrontz + 1, h);
	}

	void CDeformationAlgorithm3D::run(int) {
		// check initialized
		ASTRA_ASSERT(m_bIsInitialized);

		m_bShouldAbort = false;

		//main loop
		switch (m_eMode) {
			case NEAREST_NEIGHBOR:
				runNearest();
				break;
			case LINEAR:
				runLinear();
				break;
			case CUBICBSPLINE:
				runCubic();
				break;
			default:
				ASTRA_ASSERT(false && "Unknown interpolation mode.");
				break;
		}
	}

	void CDeformationAlgorithm3DFwd::runNearest() {
		int index = 0;
		for (int iz = 0; iz < m_pSource->getDepth(); ++iz) {

			const float32* pfDeformx = m_pDVF->getDispXConst(iz);
			const float32* pfDeformy = m_pDVF->getDispYConst(iz);
			const float32* pfDeformz = m_pDVF->getDispZConst(iz);
			CFloat32VolumeData2D* pSlice = m_pSource->fetchSliceZ(iz);

			for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
				if (m_bShouldAbort) {
					return;
				}
				for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
					putItemNear(m_pOutput, ix, iy, iz, pfDeformx[ix], pfDeformy[ix], pfDeformz[ix], pSlice->getData2D()[iy][ix]);
				}
			}

			delete pSlice;
			// Don't need to return the slice as we don't modify it
			// m_pSource->returnSliceZ(iz, pSliceZ);
		}
	}
	void CDeformationAlgorithm3DFwd::runLinear() {
		int index = 0;
		for (int iz = 0; iz < m_pSource->getDepth(); ++iz) {

			const float32* pfDeformx = m_pDVF->getDispXConst(iz);
			const float32* pfDeformy = m_pDVF->getDispYConst(iz);
			const float32* pfDeformz = m_pDVF->getDispZConst(iz);
			CFloat32VolumeData2D* pSlice = m_pSource->fetchSliceZ(iz);

			for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
				if (m_bShouldAbort) {
					return;
				}
				for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
					putItemLin(m_pOutput, ix, iy, iz, pfDeformx[ix], pfDeformy[ix], pfDeformz[ix], pSlice->getData2D()[iy][ix]);
				}
			}

			delete pSlice;
			// Don't need to return the slice as we don't modify it
			// m_pSource->returnSliceZ(iz, pSliceZ);
		}
	}

	void CDeformationAlgorithm3DFwd::runCubic() {
		ASTRA_ASSERT(false && "Not implemented yet.");
	}
	//----------------------------------------------------------------------------------------

	float readItem(CFloat32VolumeData3D* _pData, int _ix, int _iy, int _iz) {
		if ((_ix < 0) || (_ix >= _pData->getWidth())) return 0.0f;
		if ((_iy < 0) || (_iy >= _pData->getHeight())) return 0.0f;
		if ((_iz < 0) || (_iz >= _pData->getDepth())) return 0.0f;
		int index = _ix + (_pData->getWidth() * (_iy + (_pData->getHeight() * _iz)));
		return _pData->getVoxelValue(index);
	}
	float readItemNearest(CFloat32VolumeData3D* _pData, float32 _fx, float32 _fy, float32 _fz) {
		int ix = fastround2int<float32, int>(_fx);
		int iy = fastround2int<float32, int>(_fy);
		int iz = fastround2int<float32, int>(_fz);

		return readItem(_pData, ix, iy, iz);
	}

	float readItemLinear(CFloat32VolumeData3D* _pData, float32 _fx, float32 _fy, float32 _fz) {
		int lx = fastfloor2int<float32, int>(_fx);
		int ly = fastfloor2int<float32, int>(_fy);
		int lz = fastfloor2int<float32, int>(_fz);
		int lxp1 = lx + 1;
		int lyp1 = ly + 1;
		int lzp1 = lz + 1;
		float32 tx = _fx - lx;
		float32 ty = _fy - ly;
		float32 tz = _fz - lz;
		if (lx < 0) lx = lxp1 = 0;
		else if (lx >= _pData->getWidth() - 1)  lx = lxp1 = float32(_pData->getWidth() - 1);
		if (ly < 0) ly = lyp1 = 0;
		else if (ly >= _pData->getHeight() - 1) ly = lyp1 = float32(_pData->getHeight() - 1);
		if (lz < 0) lz = lzp1 = 0;
		else if (lz >= _pData->getDepth() - 1) lz = lzp1 = float32(_pData->getHeight() - 1);
		float32 a = readItem(_pData, lx, ly, lz);
		float32 b = readItem(_pData, lxp1, ly, lz);
		float32 c = readItem(_pData, lx, lyp1, lz);
		float32 d = readItem(_pData, lxp1, lyp1, lz);
		float32 e = readItem(_pData, lx, ly, lzp1);
		float32 f = readItem(_pData, lxp1, ly, lzp1);
		float32 g = readItem(_pData, lx, lyp1, lzp1);
		float32 h = readItem(_pData, lxp1, lyp1, lzp1);
		return lerp(a, b, c, d, e, f, g, h, tx, ty, tz);
	}

	float readItemCubic(CFloat32VolumeData3D* _pData, float32 _fx, float32 _fy, float32 _fz) {
		//see http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.89.7835
		ASTRA_ASSERT(false && "Not implemented yet.");
		return 0.0f;
	}

	float32 processItemNear(CFloat32VolumeData3D* _pData, int _ix, int _iy, int _iz, float32 _fdx, float32 _fdy, float32 _fdz) {
		//calculate the new position of the pixel
		float32 fx = float(_ix) + _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fy = float(_iy) + _fdy * float(_pData->getHeight()) / 2.0f;
		float32 fz = float(_iz) + _fdz * float(_pData->getDepth()) / 2.0f;

		return readItemNearest(_pData, fx, fy, fz);
	}

	float32 processItemLin(CFloat32VolumeData3D* _pData, int _ix, int _iy, int _iz, float32 _fdx, float32 _fdy, float32 _fdz) {
		//calculate the new position of the pixel
		float32 fresx = _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fresy = _fdy * float(_pData->getHeight()) / 2.0f;
		float32 fresz = _fdz * float(_pData->getDepth()) / 2.0f;

		float32 fx = float(_ix) + fresx;
		float32 fy = float(_iy) + fresy;
		float32 fz = float(_iz) + fresz;

		return readItemLinear(_pData, fx, fy, fz);
	}

	float32 processItemCubic(CFloat32VolumeData3D* _pData, int _ix, int _iy, int _iz, float32 _fdx, float32 _fdy, float32 _fdz) {
		//calculate the new position of the pixel
		float32 fresx = _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fresy = _fdy * float(_pData->getHeight()) / 2.0f;
		float32 fresz = _fdz * float(_pData->getDepth()) / 2.0f;

		float32 fx = float(_ix) + fresx;
		float32 fy = float(_iy) + fresy;
		float32 fz = float(_iz) + fresz;

		return readItemCubic(_pData, fx, fy, fz);
	}

	void CDeformationAlgorithm3DBckwd::runNearest() {
		int index = 0;
		for (int iz = 0; iz < m_pSource->getDepth(); ++iz) {
			const float32* pfDeformx = m_pDVF->getDispXConst(iz);
			const float32* pfDeformy = m_pDVF->getDispYConst(iz);
			const float32* pfDeformz = m_pDVF->getDispZConst(iz);
			CFloat32VolumeData2D* pSlice = m_pOutput->fetchSliceZ(iz);

			for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
				if (m_bShouldAbort) {
					return;
				}
				for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
					pSlice->getData2D()[iy][ix] = processItemNear(m_pSource, ix, iy, iz, pfDeformx[ix], pfDeformy[ix], pfDeformz[ix]);
				}
			}
			// Return the slice as we modified it
			m_pOutput->returnSliceZ(iz, pSlice);
			delete pSlice;
		}
	}
	void CDeformationAlgorithm3DBckwd::runLinear() {
		int index = 0;
		for (int iz = 0; iz < m_pSource->getDepth(); ++iz) {
			const float32* pfDeformx = m_pDVF->getDispXConst(iz);
			const float32* pfDeformy = m_pDVF->getDispYConst(iz);
			const float32* pfDeformz = m_pDVF->getDispZConst(iz);
			CFloat32VolumeData2D* pSlice = m_pOutput->fetchSliceZ(iz);

			for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
				if (m_bShouldAbort) {
					return;
				}
				for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
					pSlice->getData2D()[iy][ix] = processItemLin(m_pSource, ix, iy, iz, pfDeformx[ix], pfDeformy[ix], pfDeformz[ix]);
				}
			}
			// Return the slice as we modified it
			m_pOutput->returnSliceZ(iz, pSlice);
			delete pSlice;
		}
	}
	void CDeformationAlgorithm3DBckwd::runCubic() {
		int index = 0;
		for (int iz = 0; iz < m_pSource->getDepth(); ++iz) {

			const float32* pfDeformx = m_pDVF->getDispXConst(iz);
			const float32* pfDeformy = m_pDVF->getDispYConst(iz);
			const float32* pfDeformz = m_pDVF->getDispZConst(iz);
			CFloat32VolumeData2D* pSlice = m_pOutput->fetchSliceZ(iz);

			for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
				if (m_bShouldAbort) {
					return;
				}
				for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
					pSlice->getData2D()[iy][ix] = processItemCubic(m_pSource, ix, iy, iz, pfDeformx[ix], pfDeformy[ix], pfDeformz[ix]);
				}
			}
			// Return the slice as we modified it
			m_pOutput->returnSliceZ(iz, pSlice);
			delete pSlice;
		}
	}


#ifdef _OPENMP

	void COpenMPDeformationAlgorithm3DBckwd::runNearest() {
		int index = 0;
		for (int iz = 0; iz < m_pSource->getDepth(); ++iz) {
			const float32* pfDeformx = m_pDVF->getDispXConst(iz);
			const float32* pfDeformy = m_pDVF->getDispYConst(iz);
			const float32* pfDeformz = m_pDVF->getDispZConst(iz);
			CFloat32VolumeData2D* pSlice = m_pOutput->fetchSliceZ(iz);
#pragma omp parallel for
			for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
				if (!m_bShouldAbort) {
					for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
						pSlice->getData2D()[iy][ix] = processItemNear(m_pSource, ix, iy, iz, pfDeformx[ix], pfDeformy[ix], pfDeformz[ix]);
					}
				}
			}
			// Return the slice as we modified it
			m_pOutput->returnSliceZ(iz, pSlice);
			delete pSlice;
		}
	}
	void COpenMPDeformationAlgorithm3DBckwd::runLinear() {
		int index = 0;
		for (int iz = 0; iz < m_pSource->getDepth(); ++iz) {
			const float32* pfDeformx = m_pDVF->getDispXConst(iz);
			const float32* pfDeformy = m_pDVF->getDispYConst(iz);
			const float32* pfDeformz = m_pDVF->getDispZConst(iz);
			CFloat32VolumeData2D* pSlice = m_pOutput->fetchSliceZ(iz);

#pragma omp parallel for
			for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
				if (!m_bShouldAbort) {
					for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
						pSlice->getData2D()[iy][ix] = processItemLin(m_pSource, ix, iy, iz, pfDeformx[ix], pfDeformy[ix], pfDeformz[ix]);
					}
				}
			}
			// Return the slice as we modified it
			m_pOutput->returnSliceZ(iz, pSlice);
			delete pSlice;
		}
	}
	void COpenMPDeformationAlgorithm3DBckwd::runCubic() {
		int index = 0;
		for (int iz = 0; iz < m_pSource->getDepth(); ++iz) {

			const float32* pfDeformx = m_pDVF->getDispXConst(iz);
			const float32* pfDeformy = m_pDVF->getDispYConst(iz);
			const float32* pfDeformz = m_pDVF->getDispZConst(iz);
			CFloat32VolumeData2D* pSlice = m_pOutput->fetchSliceZ(iz);

#pragma omp parallel for
			for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
				if (!m_bShouldAbort) {
					for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
						pSlice->getData2D()[iy][ix] = processItemCubic(m_pSource, ix, iy, iz, pfDeformx[ix], pfDeformy[ix], pfDeformz[ix]);
					}
				}
			}
			// Return the slice as we modified it
			m_pOutput->returnSliceZ(iz, pSlice);
			delete pSlice;
		}
	}
#endif
} // namespace astra