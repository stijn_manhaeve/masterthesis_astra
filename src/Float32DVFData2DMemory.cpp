/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "astra/Float32DVFData2DMemory.h"
#include <iostream>
#include <cstring>
#include <sstream>
#include <algorithm>

#ifdef _MSC_VER
#include <malloc.h>
#else
#include <cstdlib>
#endif

namespace astra {
	//----------------------------------------------------------------------------------------
	// Constructor
	CFloat32DVFData2DMemory::CFloat32DVFData2DMemory() {
		_clear();
		m_bInitialized = false;
	}
	//----------------------------------------------------------------------------------------
	// Constructor
	CFloat32DVFData2DMemory::CFloat32DVFData2DMemory(int _iWidth, int _iHeight) {
		m_bInitialized = false;
		_initialize(_iWidth, _iHeight);
	}
	//----------------------------------------------------------------------------------------
	// Constructor
	CFloat32DVFData2DMemory::CFloat32DVFData2DMemory(int _iWidth, int _iHeight, const float32 *_pfDatax, const float32 *_pfDatay) {
		m_bInitialized = false;
		std::cout << "Initializing DVF memory...";
		_initialize(_iWidth, _iHeight, _pfDatax, _pfDatay);
		std::cout << " Done!\n";
		std::cout.flush();
	}

	//----------------------------------------------------------------------------------------
	// Destructor. Free allocated memory
	CFloat32DVFData2DMemory::~CFloat32DVFData2DMemory() {
		if (m_bInitialized)
		{
			_unInit();
		}
	}

	//----------------------------------------------------------------------------------------
	// Memory Allocation 
	//----------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------
	// Allocate memory for m_pfData and m_ppfData2D arrays.
	void CFloat32DVFData2DMemory::_allocateData() {
		// basic checks
		ASTRA_ASSERT(!m_bInitialized);

		ASTRA_ASSERT(m_iSize > 0);
		ASTRA_ASSERT(m_iSize == (size_t)m_iWidth * m_iHeight);
		ASTRA_ASSERT(m_pfDatax == NULL);
		ASTRA_ASSERT(m_pfDatay == NULL);
		ASTRA_ASSERT(m_ppfData2Dx == NULL);
		ASTRA_ASSERT(m_ppfData2Dy == NULL);

		// allocate contiguous block
#ifdef _MSC_VER
		m_pfDatax = (float32*)_aligned_malloc(m_iSize * sizeof(float32), 16);
		m_pfDatay = (float32*)_aligned_malloc(m_iSize * sizeof(float32), 16);
#else
		int ret = posix_memalign((void**)&m_pfDatax, 16, m_iSize * sizeof(float32));
		ASTRA_ASSERT(ret == 0);
		ret = posix_memalign((void**)&m_pfDatay, 16, m_iSize * sizeof(float32));
		ASTRA_ASSERT(ret == 0);
#endif

		// create array of pointers to each row of the data block
		m_ppfData2Dx = new float32*[m_iHeight];
		m_ppfData2Dy = new float32*[m_iHeight];
		for (int iy = 0; iy < m_iHeight; iy++)
		{
			m_ppfData2Dx[iy] = &(m_pfDatax[iy * m_iWidth]);
			m_ppfData2Dy[iy] = &(m_pfDatay[iy * m_iWidth]);
		}
	}

	//----------------------------------------------------------------------------------------
	// Free memory for m_pfData and m_ppfData2D arrays.
	void CFloat32DVFData2DMemory::_freeData() {
		// basic checks
		ASTRA_ASSERT(m_pfDatax != NULL);
		ASTRA_ASSERT(m_pfDatay != NULL);
		ASTRA_ASSERT(m_ppfData2Dx != NULL);
		ASTRA_ASSERT(m_ppfData2Dy != NULL);
		// free memory for index table
		delete[] m_ppfData2Dx;
		delete[] m_ppfData2Dy;

		// free memory for data block
#ifdef _MSC_VER
		_aligned_free(m_pfDatax);
		_aligned_free(m_pfDatay);
#else
		free(m_pfDatax);
		free(m_pfDatay);
#endif
	}


	//----------------------------------------------------------------------------------------
	// Clear all member variables, setting all numeric variables to 0 and all pointers to NULL. 
	void CFloat32DVFData2DMemory::_clear() {
		m_iWidth = 0;
		m_iHeight = 0;
		m_iSize = 0;

		m_pfDatax = NULL;
		m_pfDatay = NULL;
		m_ppfData2Dx = NULL;
		m_ppfData2Dy = NULL;
	}


	//----------------------------------------------------------------------------------------
	// Un-initialize the object, bringing it back in the unitialized state.
	void CFloat32DVFData2DMemory::_unInit() {
		ASTRA_ASSERT(m_bInitialized);

		_freeData();
		_clear();
		m_bInitialized = false;
	}


	//----------------------------------------------------------------------------------------
	// Initializes an instance of the CFloat32Data2D class, allocating (but not initializing) the data block.
	bool CFloat32DVFData2DMemory::_initialize(int _iWidth, int _iHeight) {
		// basic checks
		ASTRA_ASSERT(_iWidth > 0);
		ASTRA_ASSERT(_iHeight > 0);

		if (m_bInitialized)
		{
			_unInit();
		}

		// calculate size
		m_iWidth = _iWidth;
		m_iHeight = _iHeight;
		m_iSize = (size_t)m_iWidth * m_iHeight;

		// allocate memory for the data, but do not fill it
		m_pfDatax = 0;
		m_pfDatay = 0;
		m_ppfData2Dx = 0;
		m_ppfData2Dy = 0;
		_allocateData();

		// initialization complete
		m_bInitialized = true;
		return true;

	}

	//----------------------------------------------------------------------------------------
	// Initializes an instance of the CFloat32Data2D class with initialization of the data block. 
	bool CFloat32DVFData2DMemory::_initialize(int _iWidth, int _iHeight, const float32 *_pfDatax, const float32 *_pfDatay) {
		// basic checks
		ASTRA_ASSERT(_iWidth > 0);
		ASTRA_ASSERT(_iHeight > 0);
		ASTRA_ASSERT(_pfDatax != NULL);
		ASTRA_ASSERT(_pfDatay != NULL);

		if (m_bInitialized)
		{
			_unInit();
		}

		// calculate size
		m_iWidth = _iWidth;
		m_iHeight = _iHeight;
		m_iSize = (size_t)m_iWidth * m_iHeight;

		// allocate memory for the data 
		m_pfDatax = 0;
		m_pfDatay = 0;
		m_ppfData2Dx = 0;
		m_ppfData2Dy = 0;
		_allocateData();

		// fill the data block with a copy of the input data
		memcpy(m_pfDatax, _pfDatax, sizeof(float32) * m_iSize);
		memcpy(m_pfDatay, _pfDatay, sizeof(float32) * m_iSize);

		// initialization complete
		m_bInitialized = true;
		return true;
	}

	//----------------------------------------------------------------------------------------
	// Initializes an instance of the CFloat32Data2D class with a scalar initialization of the data block. 
	bool CFloat32DVFData2DMemory::_initialize(int _iWidth, int _iHeight, float32 _fScalar) {
		return _initialize(_iWidth, _iHeight, _fScalar, _fScalar);
	}

	//----------------------------------------------------------------------------------------
	// Initializes an instance of the CFloat32Data2D class with a scalar initialization of the data block. 
	bool CFloat32DVFData2DMemory::_initialize(int _iWidth, int _iHeight, float32 _fScalarx, float32 _fScalary) {
		// basic checks
		ASTRA_ASSERT(_iWidth > 0);
		ASTRA_ASSERT(_iHeight > 0);

		if (m_bInitialized)	{
			_unInit();
		}

		// calculate size
		m_iWidth = _iWidth;
		m_iHeight = _iHeight;
		m_iSize = (size_t)m_iWidth * m_iHeight;

		// allocate memory for the data 
		m_pfDatax = 0;
		m_pfDatay = 0;
		m_ppfData2Dx = 0;
		m_ppfData2Dy = 0;
		_allocateData();

		// fill the data block with a copy of the input data
		std::fill(m_pfDatax, m_pfDatax + m_iSize, _fScalarx);
		std::fill(m_pfDatay, m_pfDatay + m_iSize, _fScalary);

		// initialization complete
		m_bInitialized = true;
		return true;
	}

	void CFloat32DVFData2DMemory::copyData(const float32* _pfDatax, const float32* _pfDatay) {
		// basic checks
		ASTRA_ASSERT(m_bInitialized);
		ASTRA_ASSERT(_pfDatax != NULL);
		ASTRA_ASSERT(_pfDatay != NULL);
		ASTRA_ASSERT(m_pfDatax != NULL);
		ASTRA_ASSERT(m_pfDatay != NULL);
		ASTRA_ASSERT(m_iSize > 0);

		// copy data
		memcpy(m_pfDatax, _pfDatax, sizeof(float)*m_iSize);
		memcpy(m_pfDatay, _pfDatay, sizeof(float)*m_iSize);
	}

	void CFloat32DVFData2DMemory::setData(float32 _fScalar) {
		setData(_fScalar, _fScalar);
	}

	void CFloat32DVFData2DMemory::setData(float32 _fScalarx, float32 _fScalary) {
		// basic checks
		ASTRA_ASSERT(m_bInitialized);
		ASTRA_ASSERT(m_pfDatax != NULL);
		ASTRA_ASSERT(m_pfDatay != NULL);
		ASTRA_ASSERT(m_iSize > 0);

		// copy data
		std::fill(m_pfDatax, m_pfDatax + m_iSize, _fScalarx);
		std::fill(m_pfDatay, m_pfDatay + m_iSize, _fScalary);
	}

	void CFloat32DVFData2DMemory::clearData() {
		// basic checks
		ASTRA_ASSERT(m_bInitialized);
		ASTRA_ASSERT(m_pfDatax != NULL);
		ASTRA_ASSERT(m_pfDatay != NULL);
		ASTRA_ASSERT(m_iSize > 0);

		// set data
		memset(m_pfDatax, 0, sizeof(float) * m_iSize);
		memset(m_pfDatay, 0, sizeof(float) * m_iSize);
	}
} // end namespace astra
