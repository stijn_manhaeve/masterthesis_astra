/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "astra/Float32DVFData3DMemory.h"
#include <iostream>
#include <cstring>
#include <sstream>
#include <algorithm>

#ifdef _MSC_VER
#include <malloc.h>
#else
#include <cstdlib>
#endif

namespace astra {
	//----------------------------------------------------------------------------------------
	// Constructor
	CFloat32DVFData3DMemory::CFloat32DVFData3DMemory() {
		_clear();
		m_bInitialized = false;
	}
	//----------------------------------------------------------------------------------------
	// Constructor
	CFloat32DVFData3DMemory::CFloat32DVFData3DMemory(int _iWidth, int _iHeight, int _iDepth) {
		m_bInitialized = false;
		_initialize(_iWidth, _iHeight, _iDepth);
	}
	//----------------------------------------------------------------------------------------
	// Constructor
	CFloat32DVFData3DMemory::CFloat32DVFData3DMemory(int _iWidth, int _iHeight, int _iDepth, const float32 *_pfDatax, const float32 *_pfDatay, const float32 *_pfDataz) {
		m_bInitialized = false;
		_initialize(_iWidth, _iHeight, _iDepth, _pfDatax, _pfDatay, _pfDataz);
	}

	//----------------------------------------------------------------------------------------
	// Destructor. Free allocated memory
	CFloat32DVFData3DMemory::~CFloat32DVFData3DMemory() {
		if (m_bInitialized)
		{
			_unInit();
		}
	}

	//----------------------------------------------------------------------------------------
	// Memory Allocation 
	//----------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------
	// Allocate memory for m_pfData and m_ppfData2D arrays.
	void CFloat32DVFData3DMemory::_allocateData() {
		// basic checks
		ASTRA_ASSERT(!m_bInitialized);

		ASTRA_ASSERT(m_iSize > 0);
		ASTRA_ASSERT(m_iSize == (size_t)m_iWidth * m_iHeight * m_iDepth);
		ASTRA_ASSERT(m_pfDatax == NULL);
		ASTRA_ASSERT(m_pfDatay == NULL);
		ASTRA_ASSERT(m_pfDataz == NULL);
		ASTRA_ASSERT(m_ppfData2Dx == NULL);
		ASTRA_ASSERT(m_ppfData2Dy == NULL);
		ASTRA_ASSERT(m_ppfData2Dz == NULL);

		// allocate contiguous block
#ifdef _MSC_VER
		m_pfDatax = (float32*)_aligned_malloc(m_iSize * sizeof(float32), 16);
		m_pfDatay = (float32*)_aligned_malloc(m_iSize * sizeof(float32), 16);
		m_pfDataz = (float32*)_aligned_malloc(m_iSize * sizeof(float32), 16);
#else
		int ret = posix_memalign((void**)&m_pfDatax, 16, m_iSize * sizeof(float32));
		ASTRA_ASSERT(ret == 0);
		ret = posix_memalign((void**)&m_pfDatay, 16, m_iSize * sizeof(float32));
		ASTRA_ASSERT(ret == 0);
		ret = posix_memalign((void**)&m_pfDataz, 16, m_iSize * sizeof(float32));
		ASTRA_ASSERT(ret == 0);
#endif

		// create array of pointers to each row of the data block
		m_ppfData2Dx = new float32*[m_iDepth];
		m_ppfData2Dy = new float32*[m_iDepth];
		m_ppfData2Dz = new float32*[m_iDepth];
		for (int iz = 0; iz < m_iDepth; iz++)
		{
			m_ppfData2Dx[iz] = &(m_pfDatax[iz * m_iWidth * m_iHeight]);
			m_ppfData2Dy[iz] = &(m_pfDatay[iz * m_iWidth * m_iHeight]);
			m_ppfData2Dz[iz] = &(m_pfDataz[iz * m_iWidth * m_iHeight]);
		}
	}

	//----------------------------------------------------------------------------------------
	// Free memory for m_pfData and m_ppfData2D arrays.
	void CFloat32DVFData3DMemory::_freeData() {
		// basic checks
		ASTRA_ASSERT(m_pfDatax != NULL);
		ASTRA_ASSERT(m_pfDatay != NULL);
		ASTRA_ASSERT(m_pfDataz != NULL);
		ASTRA_ASSERT(m_ppfData2Dx != NULL);
		ASTRA_ASSERT(m_ppfData2Dy != NULL);
		ASTRA_ASSERT(m_ppfData2Dz != NULL);
		// free memory for index table
		delete[] m_ppfData2Dx;
		delete[] m_ppfData2Dy;
		delete[] m_ppfData2Dz;

		// free memory for data block
#ifdef _MSC_VER
		_aligned_free(m_pfDatax);
		_aligned_free(m_pfDatay);
		_aligned_free(m_pfDataz);
#else
		free(m_pfDatax);
		free(m_pfDatay);
		free(m_pfDataz);
#endif
	}


	//----------------------------------------------------------------------------------------
	// Clear all member variables, setting all numeric variables to 0 and all pointers to NULL. 
	void CFloat32DVFData3DMemory::_clear() {
		m_iWidth = 0;
		m_iHeight = 0;
		m_iDepth = 0;
		m_iSize = 0;

		m_pfDatax = NULL;
		m_pfDatay = NULL;
		m_pfDataz = NULL;
		m_ppfData2Dx = NULL;
		m_ppfData2Dy = NULL;
		m_ppfData2Dz = NULL;
	}


	//----------------------------------------------------------------------------------------
	// Un-initialize the object, bringing it back in the unitialized state.
	void CFloat32DVFData3DMemory::_unInit() {
		ASTRA_ASSERT(m_bInitialized);

		_freeData();
		_clear();
		m_bInitialized = false;
	}


	//----------------------------------------------------------------------------------------
	// Initializes an instance of the CFloat32Data2D class, allocating (but not initializing) the data block.
	bool CFloat32DVFData3DMemory::_initialize(int _iWidth, int _iHeight, int _iDepth) {
		// basic checks
		ASTRA_ASSERT(_iWidth > 0);
		ASTRA_ASSERT(_iHeight > 0);
		ASTRA_ASSERT(_iDepth > 0);

		if (m_bInitialized)
		{
			_unInit();
		}

		// calculate size
		m_iWidth = _iWidth;
		m_iHeight = _iHeight;
		m_iDepth = _iDepth;
		m_iSize = (size_t)m_iWidth * m_iHeight * m_iDepth;

		// allocate memory for the data, but do not fill it
		m_pfDatax = 0;
		m_pfDatay = 0;
		m_pfDataz = 0;
		m_ppfData2Dx = 0;
		m_ppfData2Dy = 0;
		m_ppfData2Dz = 0;
		_allocateData();

		// initialization complete
		m_bInitialized = true;
		return true;

	}

	//----------------------------------------------------------------------------------------
	// Initializes an instance of the CFloat32Data2D class with initialization of the data block. 
	bool CFloat32DVFData3DMemory::_initialize(int _iWidth, int _iHeight, int _iDepth, const float32 *_pfDatax, const float32 *_pfDatay, const float32 *_pfDataz) {
		// basic checks
		ASTRA_ASSERT(_iWidth > 0);
		ASTRA_ASSERT(_iHeight > 0);
		ASTRA_ASSERT(_iDepth > 0);
		ASTRA_ASSERT(_pfDatax != NULL);
		ASTRA_ASSERT(_pfDatay != NULL);
		ASTRA_ASSERT(_pfDataz != NULL);

		if (m_bInitialized)
		{
			_unInit();
		}

		// calculate size
		m_iWidth = _iWidth;
		m_iHeight = _iHeight;
		m_iDepth = _iDepth;
		m_iSize = (size_t)m_iWidth * m_iHeight * m_iDepth;

		// allocate memory for the data 
		m_pfDatax = 0;
		m_pfDatay = 0;
		m_pfDataz = 0;
		m_ppfData2Dx = 0;
		m_ppfData2Dy = 0;
		m_ppfData2Dz = 0;
		_allocateData();

		// fill the data block with a copy of the input data
		memcpy(m_pfDatax, _pfDatax, sizeof(float32) * m_iSize);
		memcpy(m_pfDatay, _pfDatay, sizeof(float32) * m_iSize);
		memcpy(m_pfDataz, _pfDataz, sizeof(float32) * m_iSize);

		// initialization complete
		m_bInitialized = true;
		return true;
	}

	//----------------------------------------------------------------------------------------
	// Initializes an instance of the CFloat32Data2D class with a scalar initialization of the data block. 
	bool CFloat32DVFData3DMemory::_initialize(int _iWidth, int _iHeight, int _iDepth, float32 _fScalar) {
		return _initialize(_iWidth, _iHeight, _iDepth, _fScalar, _fScalar, _fScalar);
	}

	//----------------------------------------------------------------------------------------
	// Initializes an instance of the CFloat32Data2D class with a scalar initialization of the data block. 
	bool CFloat32DVFData3DMemory::_initialize(int _iWidth, int _iHeight, int _iDepth, float32 _fScalarx, float32 _fScalary, float32 _fScalarz) {
		// basic checks
		ASTRA_ASSERT(_iWidth > 0);
		ASTRA_ASSERT(_iHeight > 0);
		ASTRA_ASSERT(_iDepth > 0);

		if (m_bInitialized)	{
			_unInit();
		}

		// calculate size
		m_iWidth = _iWidth;
		m_iHeight = _iHeight;
		m_iDepth = _iDepth;
		m_iSize = (size_t)m_iWidth * m_iHeight * m_iDepth;

		// allocate memory for the data 
		m_pfDatax = 0;
		m_pfDatay = 0;
		m_pfDataz = 0;
		m_ppfData2Dx = 0;
		m_ppfData2Dy = 0;
		m_ppfData2Dz = 0;
		_allocateData();

		// fill the data block with a copy of the input data
		std::fill(m_pfDatax, m_pfDatax + m_iSize, _fScalarx);
		std::fill(m_pfDatay, m_pfDatay + m_iSize, _fScalary);
		std::fill(m_pfDataz, m_pfDataz + m_iSize, _fScalarz);

		// initialization complete
		m_bInitialized = true;
		return true;
	}

	void CFloat32DVFData3DMemory::copyData(const float32* _pfDatax, const float32* _pfDatay, const float32* _pfDataz) {
		// basic checks
		ASTRA_ASSERT(m_bInitialized);
		ASTRA_ASSERT(_pfDatax != NULL);
		ASTRA_ASSERT(_pfDatay != NULL);
		ASTRA_ASSERT(_pfDataz != NULL);
		ASTRA_ASSERT(m_pfDatax != NULL);
		ASTRA_ASSERT(m_pfDatay != NULL);
		ASTRA_ASSERT(m_pfDataz != NULL);
		ASTRA_ASSERT(m_iSize > 0);

		// copy data
		memcpy(m_pfDatax, _pfDatax, sizeof(float)*m_iSize);
		memcpy(m_pfDatay, _pfDatay, sizeof(float)*m_iSize);
		memcpy(m_pfDataz, _pfDataz, sizeof(float)*m_iSize);
	}

	void CFloat32DVFData3DMemory::setData(float32 _fScalar) {
		setData(_fScalar, _fScalar, _fScalar);
	}

	void CFloat32DVFData3DMemory::setData(float32 _fScalarx, float32 _fScalary, float32 _fScalarz) {
		// basic checks
		ASTRA_ASSERT(m_bInitialized);
		ASTRA_ASSERT(m_pfDatax != NULL);
		ASTRA_ASSERT(m_pfDatay != NULL);
		ASTRA_ASSERT(m_pfDataz != NULL);
		ASTRA_ASSERT(m_iSize > 0);

		// copy data
		std::fill(m_pfDatax, m_pfDatax + m_iSize, _fScalarx);
		std::fill(m_pfDatay, m_pfDatay + m_iSize, _fScalary);
		std::fill(m_pfDataz, m_pfDataz + m_iSize, _fScalarz);
	}

	void CFloat32DVFData3DMemory::clearData() {
		// basic checks
		ASTRA_ASSERT(m_bInitialized);
		ASTRA_ASSERT(m_pfDatax != NULL);
		ASTRA_ASSERT(m_pfDatay != NULL);
		ASTRA_ASSERT(m_pfDataz != NULL);
		ASTRA_ASSERT(m_iSize > 0);

		// set data
		memset(m_pfDatax, 0, sizeof(float) * m_iSize);
		memset(m_pfDatay, 0, sizeof(float) * m_iSize);
		memset(m_pfDataz, 0, sizeof(float) * m_iSize);
	}
} // end namespace astra
