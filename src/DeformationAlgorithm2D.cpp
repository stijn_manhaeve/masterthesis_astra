/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "astra/DeformationAlgorithm2D.h"

#include "astra/AstraObjectManager.h"

using namespace std;

namespace astra {
//---------------------------------------------------------------------------------------
// Constructor
	CDeformationAlgorithm2D::CDeformationAlgorithm2D() : m_pSource(NULL),
														m_pDVF(NULL),
														m_pOutput(NULL),
														m_eMode(DEFAULT){}
	CDeformationAlgorithm2D::~CDeformationAlgorithm2D() {}
//---------------------------------------------------------------------------------------
// Destructor

//---------------------------------------------------------------------------------------
// Initialization - config
	bool CDeformationAlgorithm2D::initialize(const Config& _cfg) {
		ASTRA_ASSERT(_cfg.self);
		ConfigStackCheck<CAlgorithm> CC("DeformationAlgorithm2D", this, _cfg);

		// Source Data
		XMLNode node = _cfg.self.getSingleNode("SourceDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation2D", "No SourceDataId tag specified.");

		int id;
		id = node.getContentInt();
		m_pSource = dynamic_cast<CFloat32VolumeData2D*>(CData2DManager::getSingleton().get(id));;
		CC.markNodeParsed("SourceDataId");

		// Output Data
		node = _cfg.self.getSingleNode("OutputDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation2D", "No OutputDataId tag specified.");
		id = node.getContentInt();
		m_pSource = dynamic_cast<CFloat32VolumeData2D*>(CData2DManager::getSingleton().get(id));;
		CC.markNodeParsed("OutputDataId");

		// DVF Data
		node = _cfg.self.getSingleNode("DVFDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation2D", "No DVFDataId tag specified.");
		id = node.getContentInt();
		m_pDVF = CDVFData2DManager::getSingleton().get(id);
		CC.markNodeParsed("DVFDataId");

		return _check();
	}
//---------------------------------------------------------------------------------------
// Initialization - C++
	bool CDeformationAlgorithm2D::initialize(CFloat32VolumeData2D* _pSource,
											 CFloat32DVFData2D* _pDVF,
											 CFloat32VolumeData2D* _pOutput) {
		m_pSource = _pSource;
		m_pDVF = _pDVF;
		m_pOutput = _pOutput;

		return _check();
	}

//---------------------------------------------------------------------------------------
// Initialization - checking
	bool CDeformationAlgorithm2D::_check() const {
		// check pointers
		ASTRA_CONFIG_CHECK(m_pSource, "Deformation2D", "Invalid Source Data Object.");
		ASTRA_CONFIG_CHECK(m_pOutput, "Deformation2D", "Invalid Output Data Object.");
		ASTRA_CONFIG_CHECK(m_pDVF, "Deformation2D", "Invalid DVF Data Object.");

		// check initializations
		ASTRA_CONFIG_CHECK(m_pSource->isInitialized(), "Deformation2D", "Invalid Source Data Object.");
		ASTRA_CONFIG_CHECK(m_pOutput->isInitialized(), "Deformation2D", "Invalid Output Data Object.");
		ASTRA_CONFIG_CHECK(m_pDVF->isInitialized(), "Deformation2D", "Invalid DVF Data Object.");

		// make sure the two images have the same size
		ASTRA_CONFIG_CHECK(m_pSource->getWidth() == m_pOutput->getWidth(), "Deformation2D", "Images don't have the same width.");
		ASTRA_CONFIG_CHECK(m_pSource->getHeight() == m_pOutput->getHeight(), "Deformation2D", "Images don't have the same height.");
		// success
		return true;
	}
//---------------------------------------------------------------------------------------
// Information - All
	map<string, boost::any> CDeformationAlgorithm2D::getInformation() {
		map<string, boost::any> res;
		res["SourceDataId"] = getInformation("SourceDataId");
		res["OutputDataId"] = getInformation("OutputDataId");
		res["DVFDataID"] = getInformation("DVFDataID");
		return mergeMap<string, boost::any>(CAlgorithm::getInformation(), res);
	};

//---------------------------------------------------------------------------------------
// Information - Specific
	boost::any CDeformationAlgorithm2D::getInformation(std::string _sIdentifier) {
		if (_sIdentifier == "SourceDataId")	{
			int iIndex = CData2DManager::getSingleton().getIndex(m_pSource);
			if (iIndex != 0) return iIndex;
			return std::string("not in manager");
		}
		if (_sIdentifier == "OutputDataId") {
			int iIndex = CData2DManager::getSingleton().getIndex(m_pOutput);
			if (iIndex != 0) return iIndex;
			return std::string("not in manager");
		}
		if (_sIdentifier == "DVFDataID") {
			int iIndex = CDVFData2DManager::getSingleton().getIndex(m_pDVF);
			if (iIndex != 0) return iIndex;
			return std::string("not in manager");
		}
		return CAlgorithm::getInformation(_sIdentifier);
	};
//----------------------------------------------------------------------------------------
// the algorithm
	void putAddItem(CFloat32VolumeData2D* _pData, int _ix, int _iy, float32 _fValue) {
		if (_ix < 0) return;
		if (_ix >= _pData->getWidth()) return;
		if (_iy < 0) return;
		if (_iy >= _pData->getHeight()) return;
		_pData->getData2D()[_iy][_ix] += _fValue;
	}

	void putItem(CFloat32VolumeData2D* _pData, int _ix, int _iy, float32 _fdx, float32 _fdy, float32 _fValue) {
		//calculate the new position of the pixel
		float32 fresx = _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fresy = _fdy * float(_pData->getHeight()) / 2.0f;

		float32 factualx = float(_ix) + fresx;
		float32 factualy = float(_iy) + fresy;
		//round and calculate the fractions of the four pixels
		//the value must be spread over
		float32 froundx = std::round(factualx);
		float32 froundy = std::round(factualy);

		int ileftx, itopy;
		float32 fleftfraction, ftopfraction;

		if (froundx <= factualx) {
			ileftx = int(froundx);
			fleftfraction = 1.0f - (factualx - froundx);
		} else {
			ileftx = int(froundx) - 1;
			fleftfraction = (froundx - factualx);
		}
		if (froundy <= factualy) {
			itopy = int(froundy);
			ftopfraction = 1.0f - (factualy - froundy);
		} else {
			itopy = int(froundy) - 1;
			ftopfraction = (froundy - factualy);
		}
		//calculate the four values added to the four pixels
		float32 a = (fleftfraction          * ftopfraction         ) * _fValue;
		float32 b = ((1.0f - fleftfraction) * ftopfraction         ) * _fValue;
		float32 c = (fleftfraction          * (1.0f - ftopfraction)) * _fValue;
		float32 d = ((1.0f - fleftfraction) * (1.0f - ftopfraction)) * _fValue;

		float tt = fabs((a + b + c + d) - _fValue);
		if (tt > 1e-5) {
			std::cout << "tt = " << tt << std::endl;
			ASTRA_ASSERT((a + b + c + d) == _fValue && "Pixel value calculations are off.");
		}
		//add these values to the output image
		putAddItem(_pData, ileftx, itopy, a);
		putAddItem(_pData, ileftx + 1, itopy, b);
		putAddItem(_pData, ileftx, itopy + 1, c);
		putAddItem(_pData, ileftx + 1, itopy + 1, d);
	}

	void CDeformationAlgorithm2D::run(int) {
		// check initialized
		ASTRA_ASSERT(m_bIsInitialized);

		m_bShouldAbort = false;


		//main loop
		switch (m_eMode) {
		case NEAREST_NEIGHBOR:
			runNearest();
			break;
		case LINEAR:
			runLinear();
			break;
		case CUBICBSPLINE:
			runCubic();
			break;
		default:
			ASTRA_ASSERT(false && "Unknown interpolation mode.");
			break;
		}
		// prepare output
		m_pOutput->clearData();
		float32** pSourceData = m_pSource->getData2D();

		//main loop
		for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
			if (m_bShouldAbort) {
				return;
			}
			const float32* pfDeformx = m_pDVF->getDispXConst(iy);
			const float32* pfDeformy = m_pDVF->getDispYConst(iy);
			const float32* pfData = pSourceData[iy];
			std::cout.flush();
			for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
				putItem(m_pOutput, ix, iy, pfDeformx[ix], pfDeformy[ix], pfData[ix]);
			}
		}
	}
//----------------------------------------------------------------------------------------


	void putItemNear(CFloat32VolumeData2D* _pData, int _ix, int _iy, float32 _fdx, float32 _fdy, float32 _fValue) {
		//if (_fValue == 0.0f) return;
		//calculate the new position of the pixel
		float32 fresx = _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fresy = _fdy * float(_pData->getHeight()) / 2.0f;

		float32 factualx = float(_ix) + fresx;
		float32 factualy = float(_iy) + fresy;
		//round and calculate the fractions of the four pixels
		//the value must be spread over
		int froundx = fastround2int<float32, int>(factualx);
		int froundy = fastround2int<float32, int>(factualy);

		//add these values to the output image
		putAddItem(_pData, froundx, froundy, _fValue);
	}

	void putItemLin(CFloat32VolumeData2D* _pData, int _ix, int _iy, float32 _fdx, float32 _fdy, float32 _fValue) {
		//if (_fValue == 0.0f) return;
		//calculate the new position of the pixel
		float32 fresx = _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fresy = _fdy * float(_pData->getHeight()) / 2.0f;

		float32 factualx = float(_ix) + fresx;
		float32 factualy = float(_iy) + fresy;
		//round and calculate the fractions of the eight pixels
		//the value must be spread over
		int iroundx = fastround2int<float32, int>(factualx);
		int iroundy = fastround2int<float32, int>(factualy);

		int ileftx, itopy;
		float32 fleftfraction, ftopfraction;

		if (iroundx <= factualx) {
			ileftx = iroundx;
			fleftfraction = 1.0f - (factualx - iroundx);
		}
		else {
			ileftx = iroundx - 1;
			fleftfraction = (iroundx - factualx);
		}
		if (iroundy <= factualy) {
			itopy = iroundy;
			ftopfraction = 1.0f - (factualy - iroundy);
		}
		else {
			itopy = iroundy - 1;
			ftopfraction = (iroundy - factualy);
		}
		//calculate the four values added to the four pixels
		float32 a = (fleftfraction          * ftopfraction) * _fValue;
		float32 b = ((1.0f - fleftfraction) * ftopfraction) * _fValue;
		float32 c = (fleftfraction          * (1.0f - ftopfraction)) * _fValue;
		float32 d = ((1.0f - fleftfraction) * (1.0f - ftopfraction)) * _fValue;

		//		float tt = fabs((a + b + c + d + e + f + g + h) - _fValue);
		//		if (tt > 1e-5) {
		//			std::cout << "tt = " << tt << std::endl;
		//			ASTRA_ASSERT((a + b + c + d + e + f + g + h) == _fValue && "Voxel value calculations are off.");
		//		}
		//add these values to the output image
		putAddItem(_pData, ileftx, itopy, a);
		putAddItem(_pData, ileftx + 1, itopy, b);
		putAddItem(_pData, ileftx, itopy + 1, c);
		putAddItem(_pData, ileftx + 1, itopy + 1, d);
	}

	void CDeformationAlgorithm2DFwd::runNearest() {
		for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
			if (m_bShouldAbort) {
				return;
			}
			const float32* pfDeformx = m_pDVF->getDispXConst(iy);
			const float32* pfDeformy = m_pDVF->getDispYConst(iy);

			for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
				putItemNear(m_pOutput, ix, iy, pfDeformx[ix], pfDeformy[ix], m_pSource->getData2DConst()[ix][iy]);
			}
		}
	}
	void CDeformationAlgorithm2DFwd::runLinear() {
		int index = 0;

		for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
			if (m_bShouldAbort) {
				return;
			}
			const float32* pfDeformx = m_pDVF->getDispXConst(iy);
			const float32* pfDeformy = m_pDVF->getDispYConst(iy);

			for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
				putItemLin(m_pOutput, ix, iy, pfDeformx[ix], pfDeformy[ix], m_pSource->getData2DConst()[ix][iy]);
			}
		}
	}

	void CDeformationAlgorithm2DFwd::runCubic() {
		ASTRA_ASSERT(false && "Not implemented yet.");
	}
	//----------------------------------------------------------------------------------------

	float readItem(CFloat32VolumeData2D* _pData, int _ix, int _iy) {
		if ((_ix < 0) || (_ix >= _pData->getWidth())) return 0.0f;
		if ((_iy < 0) || (_iy >= _pData->getHeight())) return 0.0f;
		int index = _ix + (_pData->getWidth() * _iy);
		return _pData->getData(index);
	}
	float readItemNearest(CFloat32VolumeData2D* _pData, float32 _fx, float32 _fy) {
		int ix = fastround2int<float32, int>(_fx);
		int iy = fastround2int<float32, int>(_fy);

		return readItem(_pData, ix, iy);
	}

	float readItemLinear(CFloat32VolumeData2D* _pData, float32 _fx, float32 _fy) {
		int lx = fastfloor2int<float32, int>(_fx);
		int ly = fastfloor2int<float32, int>(_fy);
		int lxp1 = lx + 1;
		int lyp1 = ly + 1;
		float32 tx = _fx - lx;
		float32 ty = _fy - ly;
		if (lx < 0) lx = lxp1 = 0;
		else if (lx >= _pData->getWidth() - 1)  lx = lxp1 = float32(_pData->getWidth() - 1);
		if (ly < 0) ly = lyp1 = 0;
		else if (ly >= _pData->getHeight() - 1) ly = lyp1 = float32(_pData->getHeight() - 1);
		float32 a = readItem(_pData, lx, ly);
		float32 b = readItem(_pData, lxp1, ly);
		float32 c = readItem(_pData, lx, lyp1);
		float32 d = readItem(_pData, lxp1, lyp1);
		return lerp(a, b, c, d, tx, ty);
	}

	float readItemCubic(CFloat32VolumeData2D* _pData, float32 _fx, float32 _fy) {
		//see http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.89.7835
		ASTRA_ASSERT(false && "Not implemented yet.");
		return 0.0f;
	}

	float32 processItemNear(CFloat32VolumeData2D* _pData, int _ix, int _iy, float32 _fdx, float32 _fdy) {
		//calculate the new position of the pixel
		float32 fx = float(_ix) + _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fy = float(_iy) + _fdy * float(_pData->getHeight()) / 2.0f;

		return readItemNearest(_pData, fx, fy);
	}

	float32 processItemLin(CFloat32VolumeData2D* _pData, int _ix, int _iy, float32 _fdx, float32 _fdy) {
		//calculate the new position of the pixel
		float32 fresx = _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fresy = _fdy * float(_pData->getHeight()) / 2.0f;

		float32 fx = float(_ix) + fresx;
		float32 fy = float(_iy) + fresy;

		return readItemLinear(_pData, fx, fy);
	}

	float32 processItemCubic(CFloat32VolumeData2D* _pData, int _ix, int _iy, float32 _fdx, float32 _fdy) {
		//calculate the new position of the pixel
		float32 fresx = _fdx * float(_pData->getWidth()) / 2.0f;
		float32 fresy = _fdy * float(_pData->getHeight()) / 2.0f;

		float32 fx = float(_ix) + fresx;
		float32 fy = float(_iy) + fresy;

		return readItemCubic(_pData, fx, fy);
	}

	void CDeformationAlgorithm2DBckwd::runNearest() {
		for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
			if (m_bShouldAbort) {
				return;
			}
			const float32* pfDeformx = m_pDVF->getDispXConst(iy);
			const float32* pfDeformy = m_pDVF->getDispYConst(iy);

			for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
				int index = ix + (m_pSource->getWidth() * iy);
				m_pOutput->getData(index) = processItemNear(m_pSource, ix, iy, pfDeformx[ix], pfDeformy[ix]);
			}
		}
	}
	void CDeformationAlgorithm2DBckwd::runLinear() {
		for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
			if (m_bShouldAbort) {
				return;
			}
			const float32* pfDeformx = m_pDVF->getDispXConst(iy);
			const float32* pfDeformy = m_pDVF->getDispYConst(iy);

			for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
				int index = ix + (m_pSource->getWidth() * iy);
				m_pOutput->getData(index) = processItemLin(m_pSource, ix, iy, pfDeformx[ix], pfDeformy[ix]);
			}
		}
	}
	void CDeformationAlgorithm2DBckwd::runCubic() {
		for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
			if (m_bShouldAbort) {
				return;
			}
			const float32* pfDeformx = m_pDVF->getDispXConst(iy);
			const float32* pfDeformy = m_pDVF->getDispYConst(iy);

			for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
				int index = ix + (m_pSource->getWidth() * iy);
				m_pOutput->getData(index) = processItemCubic(m_pSource, ix, iy, pfDeformx[ix], pfDeformy[ix]);
			}
		}
	}


#ifdef _OPENMP
	void COpenMPDeformationAlgorithm2DBckwd::runNearest() {
#pragma omp parallel for
		for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
			if (!m_bShouldAbort) {
				const float32* pfDeformx = m_pDVF->getDispXConst(iy);
				const float32* pfDeformy = m_pDVF->getDispYConst(iy);

				for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
					int index = ix + (m_pSource->getWidth() * iy);
					m_pOutput->getData(index) = processItemNear(m_pSource, ix, iy, pfDeformx[ix], pfDeformy[ix]);
				}
			}
		}
	}
	void COpenMPDeformationAlgorithm2DBckwd::runLinear() {
#pragma omp parallel for
		for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
			if (!m_bShouldAbort) {
				const float32* pfDeformx = m_pDVF->getDispXConst(iy);
				const float32* pfDeformy = m_pDVF->getDispYConst(iy);

				for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
					int index = ix + (m_pSource->getWidth() * iy);
					m_pOutput->getData(index) = processItemLin(m_pSource, ix, iy, pfDeformx[ix], pfDeformy[ix]);
				}
			}
		}
	}
	void COpenMPDeformationAlgorithm2DBckwd::runCubic() {
#pragma omp parallel for
		for (int iy = 0; iy < m_pSource->getHeight(); ++iy) {
			if (!m_bShouldAbort) {
				const float32* pfDeformx = m_pDVF->getDispXConst(iy);
				const float32* pfDeformy = m_pDVF->getDispYConst(iy);

				for (int ix = 0; ix < m_pSource->getWidth(); ++ix) {
					int index = ix + (m_pSource->getWidth() * iy);
					m_pOutput->getData(index) = processItemCubic(m_pSource, ix, iy, pfDeformx[ix], pfDeformy[ix]);
				}
			}
		}
	}
#endif

} // namespace astra