/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/
#include "astra\Float32Data3DStream.h"

#include <algorithm> //std::swap

namespace astra {
	//----------------------------------------------------------------------------------------
	// Constructor
	CFloat32Data3DStream::CFloat32Data3DStream() {
		m_bInitialized = false;
	}

	//----------------------------------------------------------------------------------------
	// Destructor. Free allocated memory
	CFloat32Data3DStream::~CFloat32Data3DStream() {
	}

	bool CFloat32Data3DStream::_initialize(CFloat32DataBufferedFile* _pDataSource) {
		ASTRA_ASSERT(_pDataSource);
		m_pSource = _pDataSource;
		int iDim = m_pSource->readDimensionCount();
		ASTRA_ASSERT(iDim == 3);

		m_iWidth = m_pSource->readDimensionSize(0);
		m_iHeight = m_pSource->readDimensionSize(1);
		m_iDepth = m_pSource->readDimensionSize(2);
		m_iSize = m_iWidth * m_iHeight * m_iDepth;
		return true;
	}

	//----------------------------------------------------------------------------------------
	int CFloat32Data3DStream::getConsecutiveDepth() const {
		return m_pSource->getBufferSize();
	}

	float32* CFloat32Data3DStream::getSlice(int _iSliceIndex) const {
		return m_pSource->readSlice(_iSliceIndex);
	}

	void CFloat32Data3DStream::writeSlice(int _iSliceIndex, const float32* _pData) {
		m_pSource->writeSlice(_iSliceIndex, _pData);
	}

	float32 CFloat32Data3DStream::getDataPoint(int _iIndex) const {
		return m_pSource->readValue(_iIndex);
	}

	void CFloat32Data3DStream::setDataPoint(int _iIndex, float32 _fValue) {
		m_pSource->writeValue(_iIndex, _fValue);
	}

} // end namespace
