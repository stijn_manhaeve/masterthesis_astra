#include "Misc.h"
#include <iomanip>

namespace misc {

	std::ostream& operator<<(std::ostream& out, const prnthelp< BYTES >& bt) {
		if (bt._t < std::uint64_t(4u * 1024u)) {
			out << bt._t << 'B';
		} else if (bt._t < std::uint64_t(4u * 1024u) * 1024u) {
			out << bt._t/(1024u) << "kB";
		} else if (bt._t < std::uint64_t(4u * 1024u) * 1024u * 1024u) {
			out << bt._t/(1024u*1024u) << "MB";
		} else {
			out << bt._t / (std::uint64_t(1024u) * 1024u * 1024u) << "GB";
		}
		return out;
	}

	std::ostream& operator<<(std::ostream& out, const prnthelp< TIME >& bt) {
		unsigned int h = std::chrono::duration_cast<std::chrono::hours>(bt._t).count();
		unsigned int m = std::chrono::duration_cast<std::chrono::minutes>(bt._t).count() - h * 60u;
		std::uint64_t s = std::chrono::duration_cast<std::chrono::seconds>(bt._t).count() - (h * 60u + m) * 60u;
		std::uint64_t ms = std::chrono::duration_cast<std::chrono::milliseconds>(bt._t).count() - ((h * 60u + m) * 60u + s) * 1000u;
		out << h << ':'
			<< (m < 10u ? "0" : "") << m << ':'
			<< (s < 10u ? "0" : "") << s << '.'
			<< (ms < 10u ? "00" : (ms < 100u ? "0" : "")) << ms;
		return out;
	}

	std::ostream& operator<<(std::ostream& out, const prnthelp< TIMESECONDS >& bt) {
		std::uint64_t s = std::chrono::duration_cast<std::chrono::seconds>(bt._t).count();
		std::uint64_t ms = std::chrono::duration_cast<std::chrono::milliseconds>(bt._t).count() - s * 1000u;
		char old = out.fill('0');
		out << s << '.' << std::setw(3) << ms;
		out.fill(old);
		return out;
	}

	std::ostream& operator<<(std::ostream& out, const prnthelp< HEX >& bt) {
		static char* digits = "0123456789ABCDEF";
		const unsigned char* chp = reinterpret_cast<const unsigned char*>(&bt._t);
		for (std::uint64_t i = 0; i < sizeof(float); ++i) {
			unsigned char ch = chp[i];
			out << digits[int(ch >> 4u)] << digits[int(ch & unsigned char(15))];
		}
		return out;
	}

	std::ostream& operator<<(std::ostream& out, const prnthelp< BYTESPERTIME >& bt) {
		std::uint64_t s = std::chrono::duration_cast<std::chrono::seconds>(bt._t).count();
		if (s > 1000u) {
			std::uint64_t bps = bt._b / s;
			out << prnthelp<BYTES>(bps) << "/s";
		} else {
			std::uint64_t ms = std::chrono::duration_cast<std::chrono::milliseconds>(bt._t).count() - s * 1000u;
			double bps = double(bt._b) / (double(s) + double(ms) / 1000.0);
			out << prnthelp<BYTES>(std::uint64_t(bps)) << "/s";
		}
		return out;
	}

} /* namespace prototype */