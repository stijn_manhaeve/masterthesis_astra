#ifndef MISC_H
#define MISC_H

#include <iostream>
#include <cstdint>
#include <chrono>

namespace misc {
	enum prnthelpType {BYTES, TIME, TIMESECONDS, HEX, BYTESPERTIME};
	template<prnthelpType T>
	struct prnthelp {};

	template<>
	struct prnthelp < BYTES > {
		prnthelp(std::uint64_t t) : _t(t) {}
		std::uint64_t _t;
	};

	std::ostream& operator<<(std::ostream& out, const prnthelp< BYTES >& bt);

	typedef std::chrono::duration<std::chrono::system_clock::rep, std::chrono::system_clock::period> timediff;

	template<>
	struct prnthelp < TIME > {
		prnthelp(timediff t) : _t(t) {}
		timediff _t;
	};

	std::ostream& operator<<(std::ostream& out, const prnthelp< TIME >& bt);

	template<>
	struct prnthelp < TIMESECONDS > {
		prnthelp(timediff t) : _t(t) {}
		timediff _t;
	};

	std::ostream& operator<<(std::ostream& out, const prnthelp< TIMESECONDS >& bt);

	template<>
	struct prnthelp < HEX > {
		prnthelp(float t) : _t(t) {}
		float _t;
	};

	std::ostream& operator<<(std::ostream& out, const prnthelp< HEX >& bt);

	template<>
	struct prnthelp < BYTESPERTIME > {
		prnthelp(std::uint64_t b, timediff t) : _b(b), _t(t) {}
		std::uint64_t _b;
		timediff _t;
	};

	std::ostream& operator<<(std::ostream& out, const prnthelp< BYTESPERTIME >& bt);

#define BEGINCHRONO {\
		auto chrono__start = std::chrono::steady_clock::now();

#define ENDCHRONO(st) \
		auto chrono__end = std::chrono::steady_clock::now();\
		std::cout << st "   -> duration: " << prnthelp<TIME>(chrono__end - chrono__start) << "\n";\
	}

#define ENDCHRONOSAVE(target) \
		auto chrono__end = std::chrono::steady_clock::now();\
		target = chrono__end - chrono__start;\
	}

#define ENDCHRONOBYTES(st, bts) \
		auto chrono__end = std::chrono::steady_clock::now();\
		std::cout << st "   -> duration: " << prnthelp<TIME>(chrono__end - chrono__start) << "\n";\
		std::cout << st "   -> average speed: " << prnthelp<BYTESPERTIME>(bts, chrono__end - chrono__start) << "\n";\
	}

} /* namespace prototype */

#endif /* ifndef MISC_H */