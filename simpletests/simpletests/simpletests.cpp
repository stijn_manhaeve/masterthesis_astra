// simpletests.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdint>
#include <stdexcept>

#include <omp.h>
#include <cuda.h>

#include "astra/DeformationAlgorithm2D.h"
#include "astra/Float32DVFData2DMemory.h"
#include "astra/DeformationAlgorithm3D.h"
#include "astra/Float32DVFData3DMemory.h"
#include "astra/Float32DVFData3DStream.h"
#include "astra/Float32VolumeData3DMemory.h"
#include "astra/Float32VolumeData3DStream.h"
#include "astra/CudaDeformAlgorithm3D.h"
#include "Misc.h"
#include "Paths.h"
#include "CoutRedirect.h"

#include "DeformProfile.h"
#include "MiscProfile.h"
#include "Tests.h"


using namespace astra;
using namespace std;
using namespace misc;


bool mainTest() {
	// configuration
	std::string datafolder = DATA_ROOT;
	std::string inImgPath = datafolder + "phantoma_1.bin";
	std::string inDVFPath = datafolder + "phantoma_dvf.bin";
	std::string inImg2Path = datafolder + "phantoma_2.bin";
	std::string outImgPath = datafolder + "test_out";

	profile::paths pthsfwd;
	profile::paths pthsbigfwd;
	profile::paths pthsbckw;
	profile::paths pthsbigbckw;
	profile::paths pthsrandom;

	pthsfwd.inimage = inImgPath;
	pthsfwd.indvf = inDVFPath;
	pthsfwd.outimage = outImgPath;

	pthsbckw.inimage = inImg2Path;
	pthsbckw.indvf = inDVFPath;
	pthsbckw.outimage = outImgPath;

	pthsrandom.inimage = datafolder + "random_1.bin";
	pthsrandom.indvf = datafolder + "random_dvf.bin";
	pthsrandom.outimage = outImgPath;

	pthsbigfwd.inimage = datafolder + "slices1_c.bin";
	pthsbigfwd.indvf = datafolder + "dvf_c.bin";
	pthsbigfwd.outimage = outImgPath;

	pthsbigbckw.inimage = datafolder + "slices2_c.bin";
	pthsbigbckw.indvf = datafolder + "dvf_c.bin";
	pthsbigbckw.outimage = outImgPath;

	std::ofstream file(DATA_ROOT "profile_output_tests.txt");
	profile::profileResults::printHeader(file);
	CoutRedirect redirect(file);

	// test the forward method
	profile::TestRunner tests;
	profile::testpaths tpaths;
	tpaths.m_correctNearest = datafolder + "corrNearfwd.bin";
	tpaths.m_correctLinear = datafolder + "corrLinfwd.bin";
	tests.initialize(tpaths);
	tests.breakOnFirstError(false);
	std::size_t iBufferCPU = 64;
	std::size_t iBufferGPU = 100;
	tests.addTest(new profile::FwdNoStreamCPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR));
	tests.addTest(new profile::FwdStreamCPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU));
	tests.addTest(new profile::FwdNoStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR));
	tests.addTest(new profile::FwdStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferCPU));
	tests.addTest(new profile::FwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR));
	tests.addTest(new profile::FwdNoStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
	tests.addTest(new profile::FwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR));
	tests.addTest(new profile::FwdNoStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferGPU));
	
	tests.addTest(new profile::FwdNoStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
	tests.addTest(new profile::FwdStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, false));
	
	tests.addTest(new profile::FwdStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU));
	tests.addTest(new profile::FwdNoStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferGPU));
	tests.addTest(new profile::FwdStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferCPU, iBufferGPU));

	// run
	bool success = tests.runTests();

	// test the backward method
	tests = profile::TestRunner();
	tpaths.m_correctNearest = datafolder + "corrNearbckw.bin";
	tpaths.m_correctLinear = datafolder + "corrLinbckw.bin";
	tests.initialize(tpaths);
	tests.breakOnFirstError(false);
	iBufferCPU = 10;
	iBufferGPU = 100;
	pthsfwd.outimage += "_b";
	tests.addTest(new profile::BckwdNoStreamCPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR));
	tests.addTest(new profile::BckwdStreamCPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU));
	tests.addTest(new profile::BckwdNoStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR));
	tests.addTest(new profile::BckwdStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferCPU));
#ifdef _OPENMP
	tests.addTest(new profile::BckwdOpenMPNoStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR));
	tests.addTest(new profile::BckwdOpenMPStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferCPU));
#endif
	tests.addTest(new profile::BckwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR));
	tests.addTest(new profile::BckwdNoStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferGPU));
	tests.addTest(new profile::BckwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR));
	tests.addTest(new profile::BckwdStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, false));

	tests.addTest(new profile::BckwdNoStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
	tests.addTest(new profile::BckwdStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU));
	tests.addTest(new profile::BckwdNoStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferGPU));
	tests.addTest(new profile::BckwdStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferCPU, iBufferGPU));
	
	// run
	bool success2 = tests.runTests();
	if (success) {
		std::cerr << "All forward tests successful.\n";
	}
	if (success2) {
		std::cerr << "All backward tests successful.\n";
	}
	return success & success2;
}

void singletest() {
	std::ofstream file(DATA_ROOT "profile_output_fastround.txt");
	profile::profileResults::printHeader(file);

	int iterations = 1;
	std::size_t iBufferCPU = 10;
	std::size_t iBufferGPU = 100;
	bool doAverage = false;
	bool doWarmup = false;

	profile::paths pthsfwd;
	profile::paths pthsbigfwd;
	profile::paths pthsbckw;
	profile::paths pthsbigbckw;
	profile::paths pthsrandom;

	std::string datafolder = DATA_ROOT;
	std::string size = "300";
	std::string dvf = "r_fixed_10";
	pthsfwd.inimage = datafolder + "phantom_" + size + "_1.bin";
	pthsfwd.indvf = datafolder + "phantom_" + size + "_dvf.bin";
	pthsfwd.outimage = datafolder + "phantom_" + size + "_outf.bin";

	pthsbckw.inimage = datafolder + "phantom_" + size + "_2.bin";
	pthsbckw.indvf = datafolder + "phantom_" + size + "_dvf.bin";
	pthsbckw.outimage = datafolder + "phantom_" + size + "_outb.bin";

	pthsrandom.inimage = datafolder + "phantom_" + size + "_2.bin";
	pthsrandom.indvf = datafolder + "phantom_" + size + "_dvf" + dvf + ".bin";
	pthsrandom.outimage = datafolder + "phantom_" + size + "_outb.bin";

	// setup
	vector<profile::ProfileSetup*> profiles;
	//profiles.push_back(new profile::BckwdNoStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));

//	profiles.push_back(new profile::BckwdNoStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
	//profiles.push_back(new profile::BckwdStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, false));
	profiles.push_back(new profile::FwdNoStreamCPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR));
//	profiles.push_back(new profile::FwdStreamCPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU));
//	profiles.push_back(new profile::FwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR));
//	profiles.push_back(new profile::FwdNoStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
	profiles.push_back(new profile::BckwdNoStreamCPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR));
	profiles.push_back(new profile::BckwdNoStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR));
//	profiles.push_back(new profile::FwdStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferCPU));
	//profiles.push_back(new profile::FwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR));
	//profiles.push_back(new profile::FwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR));
	//profiles.push_back(new profile::BckwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::NEAREST_NEIGHBOR));
	//profiles.push_back(new profile::BckwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR));
//	profiles.push_back(new profile::FwdNoStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR, iBufferGPU));
//
//	profiles.push_back(new profile::BckwdNoStreamCPUProfile(pthsbckw, astraCUDA3d::NEAREST_NEIGHBOR));
//	profiles.push_back(new profile::BckwdStreamCPUProfile(pthsbckw, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU));
//
//#ifdef _OPENMP
//	profiles.push_back(new profile::BckwdOpenMPNoStreamCPUProfile(pthsbckw, astraCUDA3d::NEAREST_NEIGHBOR));
//	profiles.push_back(new profile::BckwdOpenMPStreamCPUProfile(pthsbckw, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU));
//#endif
//	profiles.push_back(new profile::BckwdNoStreamSingleCopyGPUProfile(pthsbckw, astraCUDA3d::NEAREST_NEIGHBOR));
//	///*TODO*/ profiles.push_back(new profile::BckwdNoStreamMultipleCopyGPUProfile(pthsbckw, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
//	profiles.push_back(new profile::BckwdNoStreamCPUProfile(pthsbckw, astraCUDA3d::LINEAR));
//	profiles.push_back(new profile::BckwdStreamCPUProfile(pthsbckw, astraCUDA3d::LINEAR, iBufferCPU));
//
//#ifdef _OPENMP
//	profiles.push_back(new profile::BckwdOpenMPNoStreamCPUProfile(pthsbckw, astraCUDA3d::LINEAR));
//	profiles.push_back(new profile::BckwdOpenMPStreamCPUProfile(pthsbckw, astraCUDA3d::LINEAR, iBufferCPU));
//#endif
//	profiles.push_back(new profile::BckwdNoStreamSingleCopyGPUProfile(pthsbckw, astraCUDA3d::LINEAR));
//	///*TODO*/ profiles.push_back(new profile::BckwdNoStreamMultipleCopyGPUProfile(pthsbckw, astraCUDA3d::LINEAR, iBufferGPU));
//
//
	//profiles.push_back(new profile::FwdNoStreamGlobalGhostGPUProfile(pthsrandom, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
//	profiles.push_back(new profile::FwdStreamGlobalGhostGPUProfile(pthsrandom, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU));
//	profiles.push_back(new profile::FwdNoStreamGlobalGhostGPUProfile(pthsrandom, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
//	/*to check */profiles.push_back(new profile::FwdStreamGlobalGhostGPUProfile(pthsrandom, astraCUDA3d::LINEAR, iBufferCPU, iBufferGPU));
//
//	profiles.push_back(new profile::BckwdNoStreamGlobalGhostGPUProfile(pthsbckw, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
//	profiles.push_back(new profile::BckwdStreamGlobalGhostGPUProfile(pthsbckw, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU));
//	profiles.push_back(new profile::BckwdNoStreamGlobalGhostGPUProfile(pthsbckw, astraCUDA3d::LINEAR, iBufferGPU));
//	profiles.push_back(new profile::BckwdNoStreamGlobalGhostGPUProfile(pthsbckw, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
	// run
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
	// delete
	for (profile::ProfileSetup* pr : profiles) {
		delete pr;
	}
	profiles.clear();
}

void profilerun(profile::paths pthsfwd, profile::paths pthsbckw, std::ofstream& file, int iterations = 15, std::size_t iBufferCPU = 10, std::size_t iBufferGPU = 100) {
	bool doAverage = true;
	bool doWarmup = true;

	// setup
	vector<profile::ProfileSetup*> profiles;
	profiles.push_back(new profile::FwdNoStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR));
	profiles.push_back(new profile::FwdStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR, 40u));

	profiles.push_back(new profile::FwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR));
	profiles.push_back(new profile::FwdNoStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR, 10u));
	profiles.push_back(new profile::FwdNoStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::LINEAR, 100u));

	profiles.push_back(new profile::FwdStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR, 40u, 10u));
	profiles.push_back(new profile::FwdStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::LINEAR, 40u, 100u));


	profiles.push_back(new profile::BckwdNoStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR));
	profiles.push_back(new profile::BckwdStreamCPUProfile(pthsfwd, astraCUDA3d::LINEAR, 40u));
#ifdef _OPENMP
	profiles.push_back(new profile::BckwdOpenMPNoStreamCPUProfile(pthsbckw, astraCUDA3d::LINEAR));
	profiles.push_back(new profile::BckwdOpenMPStreamCPUProfile(pthsbckw, astraCUDA3d::LINEAR, 40u));
#endif

	profiles.push_back(new profile::BckwdNoStreamSingleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR));
	profiles.push_back(new profile::BckwdNoStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR, 10u));
	profiles.push_back(new profile::BckwdNoStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::LINEAR, 100u));

	profiles.push_back(new profile::BckwdStreamMultipleCopyGPUProfile(pthsfwd, astraCUDA3d::LINEAR, 40u, 10u));
	profiles.push_back(new profile::BckwdStreamGlobalGhostGPUProfile(pthsfwd, astraCUDA3d::LINEAR, 40u, 100u));

	// run
	file << "# CPU slices:  40, GPU slices multiple copy: 10, GPU slices ghost slice: 100\n";
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
	// delete
	for (profile::ProfileSetup* pr : profiles) {
		delete pr;
	}
	profiles.clear();
}

void profilerunbig(profile::paths pthsbigfwd, profile::paths pthsbigbckw, std::ofstream& file, int iterations = 15, std::size_t iBufferCPU = 10, std::size_t iBufferGPU = 100, bool isBytes = false) {
	bool doAverage = false;
	bool doWarmup = true;
	iBufferCPU = 10;
	iBufferGPU = 10;

	// setup
	vector<profile::ProfileSetup*> profiles;
	//profiles.push_back(new profile::FwdStreamCPUProfile(pthsbigfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU));
	//profiles.push_back(new profile::FwdStreamMultipleCopyGPUProfile(pthsbigfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, isBytes));
	//profiles.push_back(new profile::FwdStreamGlobalGhostGPUProfile(pthsbigfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, isBytes));
	//profiles.push_back(new profile::BckwdStreamCPUProfile(pthsbigfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU));
	//profiles.push_back(new profile::BckwdStreamMultipleCopyGPUProfile(pthsbigfwd, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, isBytes));
	profiles.push_back(new profile::BckwdStreamGlobalGhostGPUProfile(pthsbigbckw, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, isBytes));

	//profiles.push_back(new profile::FwdStreamCPUProfile(pthsbigfwd, astraCUDA3d::LINEAR, iBufferCPU));
	//profiles.push_back(new profile::FwdStreamMultipleCopyGPUProfile(pthsbigfwd, astraCUDA3d::LINEAR, iBufferCPU, iBufferGPU, isBytes));
	//profiles.push_back(new profile::FwdStreamGlobalGhostGPUProfile(pthsbigfwd, astraCUDA3d::LINEAR, iBufferCPU, iBufferGPU, isBytes));
	//profiles.push_back(new profile::BckwdStreamCPUProfile(pthsbigfwd, astraCUDA3d::LINEAR, iBufferCPU));
	//profiles.push_back(new profile::BckwdStreamMultipleCopyGPUProfile(pthsbigfwd, astraCUDA3d::LINEAR, iBufferCPU, iBufferGPU, isBytes));
	profiles.push_back(new profile::BckwdStreamGlobalGhostGPUProfile(pthsbigbckw, astraCUDA3d::LINEAR, iBufferCPU, iBufferGPU, isBytes));

	// run
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
	// delete
	for (profile::ProfileSetup* pr : profiles) {
		delete pr;
	}
	profiles.clear();
}

bool doBigRun = false;
int regulariterations = 20;
int bigiterations = 10;
std::size_t iBufferCPU = 10;
std::size_t iBufferGPU = 100;

void bigprofile() {
	// configuration
	std::string datafolder = DATA_ROOT;
	std::string inImgPath = datafolder + "phantoma_1.bin";
	std::string inDVFPath = datafolder + "phantoma_dvf.bin";
	std::string inImg2Path = datafolder + "phantoma_2.bin";
	std::string outImgPath = datafolder + "phantoma_out.bin";

	profile::paths pthsbigfwd;
	profile::paths pthsbigbckw;

	pthsbigfwd.inimage = datafolder + "slices1_c.bin";
	pthsbigfwd.indvf = datafolder + "dvf_c.bin";
	pthsbigfwd.outimage = datafolder + "out_c.bin";

	pthsbigbckw.inimage = datafolder + "slices2_c.bin";
	pthsbigbckw.indvf = datafolder + "dvf_c.bin";
	pthsbigbckw.outimage = datafolder + "out_c.bin";

	std::ofstream file(DATA_ROOT "profile_big.txt");
	profile::profileResults::printHeader(file);
	int iterations = 10;
	profilerunbig(pthsbigfwd, pthsbigbckw, file, iterations, iBufferCPU, iBufferGPU);
}

void streamrun(profile::paths pths, std::ofstream& file, int iterations, std::size_t iBufferCPU) {
	bool doAverage = true;
	bool doWarmup = true;

	// setup
	vector<profile::ProfileSetup*> profiles;

	profiles.push_back(new profile::FwdStreamCPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU));
	profiles.push_back(new profile::BckwdStreamCPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU));

	// run
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
}

void streamrungpu(profile::paths pths, std::ofstream& file, int iterations, std::size_t iBufferGPU) {
	bool doAverage = true;
	bool doWarmup = true;

	// setup
	vector<profile::ProfileSetup*> profiles;

	profiles.push_back(new profile::FwdNoStreamMultipleCopyGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU, false));
	profiles.push_back(new profile::BckwdNoStreamMultipleCopyGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU, false));
	// run
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
}

void streamruncopy(profile::paths pths, std::ofstream& file, int iterations, std::size_t iBufferCPU, std::size_t iBufferGPU, bool isBytes) {
	bool doAverage = true;
	bool doWarmup = true;

	// setup
	vector<profile::ProfileSetup*> profiles;

	profiles.push_back(new profile::FwdStreamMultipleCopyGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, isBytes));
	profiles.push_back(new profile::BckwdStreamMultipleCopyGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, isBytes));


	// run
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
}

void streamrunghost(profile::paths pths, std::ofstream& file, int iterations, std::size_t iBufferCPU, std::size_t iBufferGPU, bool isBytes) {
	bool doAverage = true;
	bool doWarmup = true;

	// setup
	vector<profile::ProfileSetup*> profiles;

	profiles.push_back(new profile::FwdStreamGlobalGhostGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, isBytes));
	profiles.push_back(new profile::BckwdStreamGlobalGhostGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU, iBufferGPU, isBytes));

	// run
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
}

void streamprofile() {
	// configuration
	std::string datafolder = DATA_ROOT;
	std::string inImgPath = datafolder + "phantoma_1.bin";
	std::string inDVFPath = datafolder + "phantoma_dvf.bin";
	std::string inImg2Path = datafolder + "phantoma_2.bin";
	std::string outImgPath = datafolder + "phantoma_out.bin";

	profile::paths pthsfwd;
	profile::paths pthsbigfwd;
	profile::paths pthsbckw;
	profile::paths pthsbigbckw;
	profile::paths pthsrandom;


	std::cout << "Writing data to " DATA_ROOT "profile_stream.txt\n";
	std::ofstream file(DATA_ROOT "profile_stream.txt");
	profile::profileResults::printHeader(file);

	std::vector<std::string> ranges = { "fixed_10" };
	std::vector<int> sizes = { 300 }; // { 100, 150, 200, 250, 300 }; //{50, 100};
	std::vector<std::size_t> cpuslicenum = { 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240 }; // { 4, 8, 10, 14, 18, 22 };
	std::vector<std::size_t> gpuslicenum = { 10, 20, 30, 40 }; // { 100, 150, 200, 250, 300 };
	std::vector<std::size_t> gpuslicenum2 = { 100, 150, 200, 250, 300 }; // { 100, 150, 200, 250, 300 };
	std::size_t factor = 512 * 512 * 4;
	std::string r = ranges[0];

	for (int sc : cpuslicenum){
		for (int i : sizes) {
			std::string istr = to_string(i);
			pthsrandom.inimage = datafolder + "phantom_" + istr + "_2.bin";
			pthsrandom.indvf = datafolder + "phantom_" + istr + "_dvfr_" + r + ".bin";
			pthsrandom.outimage = datafolder + "phantom_" + istr + "_outb.bin";

			file << "# cpu size(slices)" << sc << '\n';
			std::cout << "# cpu size (slices)" << sc << '\n';
			streamrun(pthsrandom, file, regulariterations, sc);
			for (std::size_t sg : gpuslicenum) {
				file << "# cpu size (slices)" << sc << " gpu size (slices) " << sg << '\n';
				file.flush();
				std::cout << "# cpu size " << sc << " gpu size (slices) " << sg << '\n';
				streamruncopy(pthsrandom, file, regulariterations, sc, sg, false);
			}
			for (std::size_t sg : gpuslicenum2) {
				file << "# cpu size (slices)" << sc << " gpu size (slices) " << sg << '\n';
				file.flush();
				std::cout << "# cpu size " << sc << " gpu size (slices) " << sg << '\n';
				streamrunghost(pthsrandom, file, regulariterations, sc, sg, false);
			}
		}
	}
	for (int sg : gpuslicenum){
		file << "# gpu size(slices)" << sg << '\n';
		std::cout << "# gpu size (slices)" << sg << '\n';
		for (int i : sizes) {
			std::string istr = to_string(i);
			pthsrandom.inimage = datafolder + "phantom_" + istr + "_2.bin";
			pthsrandom.indvf = datafolder + "phantom_" + istr + "_dvfr_" + r + ".bin";
			pthsrandom.outimage = datafolder + "phantom_" + istr + "_outb.bin";

			file << "# cpu size " << sg << '\n';
			std::cout << "# cpu size " << sg << '\n';
			streamrungpu(pthsrandom, file, regulariterations, sg);
		}
	}
}


void ghostrun(profile::paths pths, std::ofstream& file, int iterations, std::size_t iBufferGPU, bool _inBytes) {
	bool doAverage = true;
	bool doWarmup = true;

	// setup
	vector<profile::ProfileSetup*> profiles;

	profiles.push_back(new profile::FwdNoStreamGlobalGhostGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU, _inBytes));
	//profiles.push_back(new profile::FwdNoStreamGlobalGhostGPUProfile(pths, astraCUDA3d::LINEAR, iBufferGPU, _inBytes));
	
	//profiles.push_back(new profile::BckwdNoStreamGlobalGhostGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU, _inBytes));
	//profiles.push_back(new profile::BckwdNoStreamGlobalGhostGPUProfile(pths, astraCUDA3d::LINEAR, iBufferGPU, _inBytes));
	// run
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
}

void ghostprofile() {
	// configuration
	std::string datafolder = DATA_ROOT;
	std::string inImgPath = datafolder + "phantoma_1.bin";
	std::string inDVFPath = datafolder + "phantoma_dvf.bin";
	std::string inImg2Path = datafolder + "phantoma_2.bin";
	std::string outImgPath = datafolder + "phantoma_out.bin";

	profile::paths pthsfwd;
	profile::paths pthsbigfwd;
	profile::paths pthsbckw;
	profile::paths pthsbigbckw;
	profile::paths pthsrandom;


	std::cout << "Writing data to " DATA_ROOT "profile_ghost_reduce.txt\n";
	std::ofstream file(DATA_ROOT "profile_ghost_reduce.txt");
	profile::profileResults::printHeader(file);

	std::vector<std::string> ranges = {"fixed_10"};
	std::vector<int> sizes = {250, 300, 350, 400}; // { 100, 150, 200, 250, 300 }; //{50, 100};
	std::vector<std::size_t> slicenum = { 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145 }; // { 100, 150, 200, 250, 300 };
	std::vector<std::size_t> bytesnum = { 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32}; // { 4, 8, 10, 14, 18, 22 };
	std::size_t factor = 512 * 512 * 4;

	for (std::string r : ranges){
		file << "# deltaz range " << r << '\n';
		std::cout << "# deltaz range " << r << '\n';
		for (int i : sizes) {
			std::string istr = to_string(i);
			pthsrandom.inimage = datafolder + "phantom_" + istr + "_2.bin";
			pthsrandom.indvf = datafolder + "phantom_" + istr + "_dvfr_" + r + ".bin";
			pthsrandom.outimage = datafolder + "phantom_" + istr + "_outb.bin";
			for (std::size_t s : slicenum) {
				file << "# size " << istr << "buffer size (slices) " << s << '\n';
				file.flush();
				std::cout << "# size " << istr << "buffer size (slices) " << s << '\n';
				ghostrun(pthsrandom, file, regulariterations, s, false);
			}
		}
		for (int i : sizes) {
			std::string istr = to_string(i);
			pthsrandom.inimage = datafolder + "phantom_" + istr + "_2.bin";
			pthsrandom.indvf = datafolder + "phantom_" + istr + "_dvfr_" + r + ".bin";
			pthsrandom.outimage = datafolder + "phantom_" + istr + "_outb.bin";
			for (std::size_t s : bytesnum) {
				s *= factor;
				file << "# size " << istr << "buffer size (bytes) " << s << '\n';
				file.flush();
				std::cout << "# size " << istr << "buffer size (bytes) " << s << '\n';
				ghostrun(pthsrandom, file, regulariterations, s, true);
			}
		}
	}
}

void dvfrangerun(profile::paths pths, std::ofstream& file, int iterations, std::size_t iBufferGPU, bool _inBytes) {
	bool doAverage = true;
	bool doWarmup = true;

	// setup
	vector<profile::ProfileSetup*> profiles;
	profiles.push_back(new profile::FwdNoStreamCPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR));
	profiles.push_back(new profile::BckwdNoStreamCPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR));

	profiles.push_back(new profile::FwdNoStreamSingleCopyGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR));
	profiles.push_back(new profile::BckwdNoStreamSingleCopyGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR));
	profiles.push_back(new profile::FwdNoStreamMultipleCopyGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));
	profiles.push_back(new profile::BckwdNoStreamMultipleCopyGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU));

	profiles.push_back(new profile::FwdNoStreamGlobalGhostGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU, _inBytes));
	profiles.push_back(new profile::BckwdNoStreamGlobalGhostGPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferGPU, _inBytes));
	// run
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
}

void dfvrangeprofile() {
	// configuration
	std::string datafolder = DATA_ROOT;
	std::string inImgPath = datafolder + "phantoma_1.bin";
	std::string inDVFPath = datafolder + "phantoma_dvf.bin";
	std::string inImg2Path = datafolder + "phantoma_2.bin";
	std::string outImgPath = datafolder + "phantoma_out.bin";

	profile::paths pthsfwd;
	profile::paths pthsbigfwd;
	profile::paths pthsbckw;
	profile::paths pthsbigbckw;
	profile::paths pthsrandom;


	std::cout << "Writing data to " DATA_ROOT "profile_dvfrange.txt\n";
	std::ofstream file(DATA_ROOT "profile_dvfrange.txt");
	profile::profileResults::printHeader(file);

	std::vector<std::string> ranges = { "fixed_0", "fixed_1", "fixed_2", "fixed_5", "fixed_10", "fixed_15", "fixed_20" };
	std::vector<std::string> ranges2 = { "relative_0", "relative_1.000000e-02", "relative_2.000000e-02", "relative_3.000000e-02", "relative_4.000000e-02", "relative_6.000000e-02", "relative_8.000000e-02", "relative_1.000000e-01" };
	std::vector<int> sizes = { 300 }; // { 100, 150, 200, 250, 300 }; //{50, 100};
	std::vector<std::size_t> slicenum = { 100  }; // { 100, 150, 200, 250, 300 };
	std::vector<std::size_t> bytesnum = {  }; // { 4, 8, 10, 14, 18, 22 };
	std::size_t factor = 512 * 512 * 4;
	std::size_t s = slicenum[0];

	for (int i : sizes){
		for (std::string r : ranges) {
			std::string istr = to_string(i);
			pthsrandom.inimage = datafolder + "phantom_" + istr + "_2.bin";
			pthsrandom.indvf = datafolder + "phantom_" + istr + "_dvfr_" + r + ".bin";
			pthsrandom.outimage = datafolder + "phantom_" + istr + "_outb.bin";

			file << "# size " << istr << " random " << r << '\n';
			file.flush();
			std::cout << "# size " << istr << " random " << r << '\n';
			dvfrangerun(pthsrandom, file, regulariterations, s, false);
		}
		for (std::string r : ranges2) {
			std::string istr = to_string(i);
			pthsrandom.inimage = datafolder + "phantom_" + istr + "_2.bin";
			pthsrandom.indvf = datafolder + "phantom_" + istr + "_dvfr_" + r + ".bin";
			pthsrandom.outimage = datafolder + "phantom_" + istr + "_outb.bin";

			file << "# size " << istr << " random " << r << '\n';
			file.flush();
			std::cout << "# size " << istr << " random " << r << '\n';
			dvfrangerun(pthsrandom, file, regulariterations, s, false);
		}
	}
}


#ifdef _OPENMP
void omprun(profile::paths pths, std::ofstream& file, int iterations) {
	bool doAverage = true;
	bool doWarmup = true;

	// setup
	vector<profile::ProfileSetup*> profiles;

	profiles.push_back(new profile::BckwdOpenMPNoStreamCPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR));
	profiles.push_back(new profile::BckwdOpenMPStreamCPUProfile(pths, astraCUDA3d::NEAREST_NEIGHBOR, iBufferCPU));

	profiles.push_back(new profile::BckwdOpenMPNoStreamCPUProfile(pths, astraCUDA3d::LINEAR));
	profiles.push_back(new profile::BckwdOpenMPStreamCPUProfile(pths, astraCUDA3d::LINEAR, iBufferCPU));
	// run
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
}
void ompprofile() {
	// configuration
	std::string datafolder = DATA_ROOT;
	std::string inImgPath = datafolder + "phantoma_1.bin";
	std::string inDVFPath = datafolder + "phantoma_dvf.bin";
	std::string inImg2Path = datafolder + "phantoma_2.bin";
	std::string outImgPath = datafolder + "phantoma_out.bin";

	profile::paths pthsrandom;

	std::cout << "Writing data to " DATA_ROOT "profile_omp.txt\n";
	std::ofstream file(DATA_ROOT "profile_omp.txt");
	profile::profileResults::printHeader(file);

	std::vector<std::string> ranges = { "fixed_10" };
	std::vector<int> sizes = { 300 }; // { 100, 150, 200, 250, 300 }; //{50, 100};
	std::vector<std::size_t> slicenum = { 100 }; // { 100, 150, 200, 250, 300 };
	std::vector<std::size_t> bytesnum = {}; // { 4, 8, 10, 14, 18, 22 };
	std::size_t factor = 512 * 512 * 4;
	std::size_t s = slicenum[0];
	std::string r = ranges[0];

	int threadcount = omp_get_max_threads();
	for (int i : sizes){
		for (int j = 1; j <= threadcount; ++j) {
			omp_set_num_threads(j);
			std::string istr = to_string(i);
			pthsrandom.inimage = datafolder + "phantom_" + istr + "_2.bin";
			pthsrandom.indvf = datafolder + "phantom_" + istr + "_dvfr_" + r + ".bin";
			pthsrandom.outimage = datafolder + "phantom_" + istr + "_outb.bin";

			file << "# size " << istr << " threads " << j << '\n';
			file.flush();
			std::cout << "# size " << istr << " threads " << j << '\n';
			omprun(pthsrandom, file, regulariterations);
		}
	}
	omp_set_num_threads(threadcount);
}
#endif

void mainProfile() {
	// configuration
	std::string datafolder = DATA_ROOT;
	std::string inImgPath = datafolder + "phantoma_1.bin";
	std::string inDVFPath = datafolder + "phantoma_dvf.bin";
	std::string inImg2Path = datafolder + "phantoma_2.bin";
	std::string outImgPath = datafolder + "phantoma_out.bin";

	profile::paths pthsfwd;
	profile::paths pthsbigfwd;
	profile::paths pthsbckw;
	profile::paths pthsbigbckw;
	profile::paths pthsrandom;


	std::cout << "Writing data to " DATA_ROOT "profile_output_main_linear.txt\n";
	std::ofstream file(DATA_ROOT "profile_output_main.txt");
	profile::profileResults::printHeader(file);

	int iterations = regulariterations;

	std::vector<int> sizes = { 300 }; // { 250, 300, 350, 400 }; //{50, 100};
	for (int i : sizes) {
		std::string istr = to_string(i);
		file << "# size " << istr << '\n';
		std::cout << "# size " << istr << '\n';
		pthsfwd.inimage = datafolder + "phantom_" + istr + "_1.bin";
		pthsfwd.indvf = datafolder + "phantom_" + istr + "_dvf.bin";
		pthsfwd.outimage = datafolder + "phantom_" + istr + "_outf.bin";

		pthsbckw.inimage = datafolder + "phantom_" + istr + "_2.bin";
		pthsbckw.indvf = datafolder + "phantom_" + istr + "_dvf.bin";
		pthsbckw.outimage = datafolder + "phantom_" + istr + "_outb.bin";

		std::string randname = "fixed_10";
		pthsrandom.inimage = datafolder + "phantom_" + istr + "_2.bin";
		pthsrandom.indvf = datafolder + "phantom_" + istr + "_dvfr_" + randname + ".bin";
		pthsrandom.outimage = datafolder + "phantom_" + istr + "_outb.bin";
		file << "\n\n";
		file << "# nonrandomized, size " << istr << '\n';
		std::cout << "# nonrandomized, size " << istr << '\n';
		profilerun(pthsfwd, pthsbckw, file, iterations, iBufferCPU, iBufferGPU);
		//file << "\n\n";
		//file << "# randomized, size " << istr << '\n';
		//std::cout << "# randomized, size " << istr << '\n';
		//profilerun(pthsrandom, pthsrandom, file, iterations, iBufferCPU, iBufferGPU);
	}

	if (!doBigRun)
		return;
	file << "# actual, big data\n";
	iterations = bigiterations;
	pthsbigfwd.inimage = datafolder + "slices1_c.bin";
	pthsbigfwd.indvf = datafolder + "dvf_c.bin";
	pthsbigfwd.outimage = datafolder + "out_c.bin";

	pthsbigbckw.inimage = datafolder + "slices2_c.bin";
	pthsbigbckw.indvf = datafolder + "dvf_c.bin";
	pthsbigbckw.outimage = datafolder + "out_c.bin";
	profilerunbig(pthsbigfwd, pthsbigbckw, file, iterations, iBufferCPU, iBufferGPU);
}

void profilerunMisc() {
	unsigned int times = 100000000;
	unsigned int iterations = 20;
	bool doAverage = false;
	bool doWarmup = true;

	std::cout << "Writing data to " DATA_ROOT "profile_output_misc_fma.txt\n";
	std::ofstream file(DATA_ROOT "profile_output_misc_fma.txt");
	profile::profileResults::printHeader(file);

	// setup
	vector<profile::ProfileSetup*> profiles;
	//profiles.push_back(new profile::FastRoundSetupGT(times));
	//profiles.push_back(new profile::FastRoundSetupGE(times));
	//profiles.push_back(new profile::FastRoundSetupLT(times));
	//profiles.push_back(new profile::FastRoundSetupLE(times));
	//profiles.push_back(new profile::FastRoundSetupSignbit(times));
	//profiles.push_back(new profile::FastRoundSetupStdRound(times));
	//profiles.push_back(new profile::FastRoundSetupStdRint(times));
	profiles.push_back(new profile::NoFMASetup(times));
	profiles.push_back(new profile::FMASetup(times));
	// run
	for (profile::ProfileSetup* pr : profiles) {
		pr->printAverage(doAverage);
		pr->warmupRound(doWarmup);
		pr->run(iterations);
		file << *pr << '\n';
	}
	// delete
	for (profile::ProfileSetup* pr : profiles) {
		delete pr;
	}
	profiles.clear();
}

void printGPUInfo() {
#ifdef ASTRA_CUDA
	// see https://devtalk.nvidia.com/default/topic/389173/cuda-programming-and-performance/cumemgetinfo-/post/2760536/#2760536
	std::size_t free, total;
	int gpuCount, i;
	CUresult res;
	CUdevice dev;
	CUcontext ctx;

	cuInit(0);

	cuDeviceGetCount(&gpuCount);
	std::cout << "Found " << gpuCount << " GPU device(s)\n";

	for (i = 0; i<gpuCount; i++) {
		cuDeviceGet(&dev, i);
		cuCtxCreate(&ctx, 0, dev);
		res = cuMemGetInfo(&free, &total);
		std::cout << "device: " << i << '\n';
		if (res != CUDA_SUCCESS) {
			std::cout << "  cuMemGetInfo failed!\n";
			std::cout << "  > status: " << res << '\n';
		} else {
			std::cout << "  > free memory: " << misc::prnthelp<BYTES>(free) << '\n';
			std::cout << "  > total memory: " << misc::prnthelp<BYTES>(total) << '\n';
		}
		cuCtxDestroy(ctx);
	}
# else
	std::cout << "CPU only mode\n";
#endif
}

int main(int argc, char** argv)
{
	doBigRun = false;
	regulariterations = 20;
	bigiterations = 10;
	iBufferCPU = 100;
	iBufferGPU = 50;
	try {
		//std::ofstream file(DATA_ROOT "out.txt");
		//CoutRedirect out(file);
		//printGPUInfo();
		bool doProfile = true;
		bool doMainProfile = true;
		bool doGhostProfile = false;
		bool doDVFrangeProfile = false;
		bool doOmpProfile = false;
		bool doProfileMisc = false;
		bool doStreamProfile = true;
		bool doBigProfile = false;
		if (doProfileMisc) {
			profilerunMisc();
		} else if (doProfile) {
			std::cout << "Starting tests...\n";
			if (mainTest()) {
				BEGINCHRONO
				std::cout << "Tests ok\nStarting profiling...\n";
				if (doMainProfile) mainProfile();
#ifdef _OPENMP
				if (doOmpProfile) ompprofile();
#endif
				if (doDVFrangeProfile) dfvrangeprofile();
				if (doGhostProfile) ghostprofile();
				if (doStreamProfile) streamprofile();
				if (doBigProfile) bigprofile();
				ENDCHRONO("");
			}
		} else {
			singletest();
		}
	} catch (std::exception& e) {
		std::cerr << "Caught exception: " << e.what() << "\n";
	}
	std::cout << "All done!\n";
	std::cout.flush();
	return 0;
}
