#ifndef MISCPROFILE_H
#define MISCPROFILE_H

#include "Profile.h"
#include "astra\CudaDeformAlgorithm3D.h"

namespace profile {

	class MathTestSetup : public ProfileSetup {
	public:
		inline void numcalls(std::size_t _numcalls) { m_uiNumCalls = _numcalls; }
		inline std::size_t numcalls() const { return m_uiNumCalls; }
	protected:
		MathTestSetup(std::string _description, std::size_t _numcalls = 0);
		std::size_t m_uiNumCalls;
	};

	class FastRoundSetupGT : public MathTestSetup {
	public:
		FastRoundSetupGT(std::size_t _numcalls = 0);
		virtual void runIteration(profileResults& _res);
	};

	class FastRoundSetupGE : public MathTestSetup {
	public:
		FastRoundSetupGE(std::size_t _numcalls = 0);
		virtual void runIteration(profileResults& _res);
	};

	class FastRoundSetupLT : public MathTestSetup {
	public:
		FastRoundSetupLT(std::size_t _numcalls = 0);
		virtual void runIteration(profileResults& _res);
	};

	class FastRoundSetupLE : public MathTestSetup {
	public:
		FastRoundSetupLE(std::size_t _numcalls = 0);
		virtual void runIteration(profileResults& _res);
	};

	class FastRoundSetupSignbit : public MathTestSetup {
	public:
		FastRoundSetupSignbit(std::size_t _numcalls = 0);
		virtual void runIteration(profileResults& _res);
	};

	class FastRoundSetupStdRound : public MathTestSetup {
	public:
		FastRoundSetupStdRound(std::size_t _numcalls = 0);
		virtual void runIteration(profileResults& _res);
	};

	class FastRoundSetupStdRint : public MathTestSetup {
	public:
		FastRoundSetupStdRint(std::size_t _numcalls = 0);
		virtual void runIteration(profileResults& _res);
	};

	class FMASetup : public MathTestSetup {
	public:
		FMASetup(std::size_t _numcalls = 0);
		virtual void runIteration(profileResults& _res);
	};

	class NoFMASetup : public MathTestSetup {
	public:
		NoFMASetup(std::size_t _numcalls = 0);
		virtual void runIteration(profileResults& _res);
	};
}

#endif