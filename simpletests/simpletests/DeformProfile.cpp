#include "DeformProfile.h"
#include "astra/DeformationAlgorithm2D.h"
#include "astra/Float32DVFData2DMemory.h"
#include "astra/DeformationAlgorithm3D.h"
#include "astra/Float32DVFData3DMemory.h"
#include "astra/Float32DVFData3DStream.h"
#include "astra/Float32VolumeData3DMemory.h"
#include "astra/Float32VolumeData3DStream.h"
#include "astra/CudaDeformAlgorithm3D.h"
#include "Misc.h"
#include "Paths.h"

using namespace astra;
using namespace std;
using namespace misc;

namespace profile {

	struct volgeom3D {
		CFloat32VolumeData3DMemory* m_volume;
		CVolumeGeometry3D* m_geom;
		volgeom3D() : m_volume(NULL), m_geom(NULL) {}
		~volgeom3D() {
			delete m_volume;
			delete m_geom;
		}
	};

	struct volsourcegeom3D {
		CFloat32DataBufferedFile* m_file;
		CFloat32DataSource* m_source;
		CFloat32VolumeData3DStream* m_volume;
		CVolumeGeometry3D* m_geom;
		volsourcegeom3D() : m_file(NULL), m_source(NULL), m_volume(NULL), m_geom(NULL) {}
		~volsourcegeom3D() {
			delete m_volume;
			delete m_file;
			delete m_geom;
			delete m_source;
		}
	};

	struct dvf3D {
		CFloat32DVFData3DMemory* m_dvf;
		dvf3D() : m_dvf(NULL) {}
		~dvf3D() {
			delete m_dvf;
		}
	};

	struct dvfsource3D {
		CFloat32DataMultiBufferedFile* m_file;
		CFloat32DataSource* m_source;
		CFloat32DVFData3DStream* m_dvf;
		dvfsource3D() : m_file(NULL), m_dvf(NULL), m_source(NULL) {}
		~dvfsource3D() {
			delete m_dvf;
			delete m_file;
			delete m_source;
		}
	};

	void readImage3D(std::string filename, volsourcegeom3D& result, std::size_t _buffersize) {
		result.m_source = new CFloat32RawDataFile();
		static_cast<CFloat32RawDataFile*>(result.m_source)->open(filename, CFloat32DataSource::READ);
		result.m_geom = new CVolumeGeometry3D(result.m_source->readDimensionSize(0), result.m_source->readDimensionSize(1), result.m_source->readDimensionSize(2));
		result.m_file = new CFloat32DataBufferedFile();
		result.m_file->setBufferSize(_buffersize);
		result.m_file->initialize(result.m_source);
		result.m_volume = new CFloat32VolumeData3DStream(result.m_geom, result.m_file);
	}

	void readImage3D(std::string filename, volsourcegeom3D& result, CVolumeGeometry3D* geom, std::size_t _buffersize) {
		result.m_source = new CFloat32RawDataFile();
		result.m_source->setDimensionCount(3u);
		result.m_source->setDimensionSize(0u, geom->getGridColCount());
		result.m_source->setDimensionSize(1u, geom->getGridRowCount());
		result.m_source->setDimensionSize(2u, geom->getGridSliceCount());
		static_cast<CFloat32RawDataFile*>(result.m_source)->open(filename, CFloat32DataSource::WRITE);
		result.m_geom = geom;
		result.m_file = new CFloat32DataBufferedFile();
		result.m_file->setBufferSize(_buffersize);
		result.m_file->initialize(result.m_source);
		result.m_volume = new CFloat32VolumeData3DStream(result.m_geom, result.m_file);
	}

	void readImage3D(std::string filename, volgeom3D& result) {
		std::ifstream m_file;
		uint32_t ndims, sx, sy, sz;
		m_file.open(filename, std::ios::in | std::ios::binary);
		if (!m_file.read(reinterpret_cast<char*>(&ndims), sizeof(ndims))) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Failed to read the amount of dimensions when reading file '"
				<< filename
				<< "'.";
			throw std::runtime_error(msg.str());
		}
		if (ndims != 3u) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Expected 3 dimensions when reading file '"
				<< filename
				<< "', got " << ndims << ".";
			throw std::runtime_error(msg.str());
		}
		if (!m_file.read(reinterpret_cast<char*>(&(sx)), sizeof(sx))) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Failed to read the 1st dimension when reading file '"
				<< filename
				<< "'.";
			throw std::runtime_error(msg.str());
		}
		if (!m_file.read(reinterpret_cast<char*>(&(sy)), sizeof(sy))) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Failed to read the 2nd dimension when reading file '"
				<< filename
				<< "'.";
			throw std::runtime_error(msg.str());
		}
		if (!m_file.read(reinterpret_cast<char*>(&(sz)), sizeof(sz))) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Failed to read the 3rd dimension when reading file '"
				<< filename
				<< "'.";
			throw std::runtime_error(msg.str());
		}

		float32* data = new float32[sx * sy * sz];
		m_file.read(reinterpret_cast<char*>(data), sizeof(float32) * sx * sy * sz);

		result.m_geom = new CVolumeGeometry3D(sx, sy, sz);
		result.m_volume = new CFloat32VolumeData3DMemory(result.m_geom, data);
		delete[] data;

	}

	void writeImage3D(CFloat32VolumeData3DMemory* _pData, std::string _sfilename) {
		std::ofstream file;
		uint32_t uitmp;
		file.open(_sfilename, std::ios::out | std::ios::binary);
		// write the header
		uitmp = 3;
		file.write(reinterpret_cast<char*>(&uitmp), sizeof(std::uint32_t));
		uitmp = std::uint32_t(_pData->getWidth());
		file.write(reinterpret_cast<char*>(&uitmp), sizeof(std::uint32_t));
		uitmp = std::uint32_t(_pData->getHeight());
		file.write(reinterpret_cast<char*>(&uitmp), sizeof(std::uint32_t));
		uitmp = std::uint32_t(_pData->getDepth());
		file.write(reinterpret_cast<char*>(&uitmp), sizeof(std::uint32_t));

		file.write(reinterpret_cast<char*>(_pData->getData()), sizeof(float32) * _pData->getSize());
		file.flush();
		file.close();
	}

	void readDVF3D(std::string filename, dvfsource3D& result, std::size_t _buffersize) {
		result.m_source = new CFloat32RawDataFile();
		static_cast<CFloat32RawDataFile*>(result.m_source)->open(filename, CFloat32DataSource::READ);
		result.m_file = new CFloat32DataMultiBufferedFile();
		result.m_file->setBufferSize(_buffersize);
		result.m_file->initialize(result.m_source);
		result.m_dvf = new CFloat32DVFData3DStream(result.m_file);
	}

	void readDVF3D(std::string filename, dvf3D& result) {
		std::ifstream m_file;
		uint32_t ndims, sx, sy, sz, ss;
		m_file.open(filename, std::ios::in | std::ios::binary);
		if (!m_file.read(reinterpret_cast<char*>(&ndims), sizeof(ndims))) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Failed to read the amount of dimensions when reading file '"
				<< filename
				<< "'.";
			throw std::runtime_error(msg.str());
		}
		if (ndims != 4u) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Expected 4 dimensions when reading file '"
				<< filename
				<< "', got " << ndims << ".";
			throw std::runtime_error(msg.str());
		}
		if (!m_file.read(reinterpret_cast<char*>(&(sx)), sizeof(sx))) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Failed to read the 1st dimension when reading file '"
				<< filename
				<< "'.";
			throw std::runtime_error(msg.str());
		}
		if (!m_file.read(reinterpret_cast<char*>(&(sy)), sizeof(sy))) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Failed to read the 2nd dimension when reading file '"
				<< filename
				<< "'.";
			throw std::runtime_error(msg.str());
		}
		if (!m_file.read(reinterpret_cast<char*>(&(sz)), sizeof(sz))) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Failed to read the 3nd dimension when reading file '"
				<< filename
				<< "'.";
			throw std::runtime_error(msg.str());
		}
		if (!m_file.read(reinterpret_cast<char*>(&(ss)), sizeof(ss))) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Failed to read the 4th dimension when reading file '"
				<< filename
				<< "'.";
			throw std::runtime_error(msg.str());
		}
		if (ss != 3u) {
			std::stringstream msg;
			msg << __FILE__ << ": " << __LINE__
				<< ": Expected The 4th dimension to have size 3 when reading file '"
				<< filename
				<< "', got " << ss << ".";
			throw std::runtime_error(msg.str());
		}

		float32* data = new float32[sx * sy *sz * 3];
		m_file.read(reinterpret_cast<char*>(data), sizeof(float32) * sx * sy *sz * 3);

		result.m_dvf = new CFloat32DVFData3DMemory(sx, sy, sz, data, data + sx * sy * sz, data + 2 * sx * sy * sz);
		delete[] data;
	}

	using astra::EInterpolationMode;
	std::string mode2str(EInterpolationMode _mode) {
		switch (_mode) {
			case EInterpolationMode::LINEAR:
				return "_linear";
			case EInterpolationMode::NEAREST_NEIGHBOR:
				return "_nearest";
			default:
				return "_unknown";
		}
	}
	GPUStreamProfile::GPUStreamProfile(std::size_t _buffersize, bool _asBytes) : m_iSlicesGPU(_buffersize), m_iBytesGPU(_asBytes? _buffersize : 0u) {
	}
	CPUStreamProfile::CPUStreamProfile(std::size_t _buffersize) : m_iSlicesCPU(_buffersize) {
	}
	std::size_t GPUStreamProfile::calcBufferSize(std::size_t _in, std::size_t width, std::size_t height) const {
		const std::size_t pitch = 512;
		return _in * height * pitch * ((width + pitch - 1u) / pitch);
	}

	DeformProfileSetup::DeformProfileSetup(std::string _description, paths& _paths, EInterpolationMode _mode) :
		ProfileSetup(_description), m_paths(_paths), m_eMode(_mode) {
	}

	FwdNoStreamCPUProfile::FwdNoStreamCPUProfile(paths& _paths, EInterpolationMode _mode) :
		DeformProfileSetup("CPU_no_stream_fwd" + mode2str(_mode), _paths, _mode) {

	}
	void FwdNoStreamCPUProfile::runIteration(profileResults& _res) {
		volgeom3D volgeomSource;
		dvf3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource); 
			readDVF3D(m_paths.indvf, dvfSource);
		ENDCHRONOSAVE(_res.m_readDisk)

		//prepare output image
		volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		CDeformationAlgorithm3DFwd algorithm;
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(EInterpolationMode(m_eMode));
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_pureCompute)
		//write the data
		BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
	}

	FwdStreamCPUProfile::FwdStreamCPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _buffersize) :
		DeformProfileSetup("CPU_stream_fwd" + mode2str(_mode), _paths, _mode),
		CPUStreamProfile(_buffersize){

	}
	void FwdStreamCPUProfile::runIteration(profileResults& _res) {
		volsourcegeom3D volgeomSource;
		dvfsource3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource, m_iSlicesCPU);
			readDVF3D(m_paths.indvf, dvfSource, m_iSlicesCPU);
		ENDCHRONOSAVE(_res.m_readDisk)

		//prepare output image
		volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		CDeformationAlgorithm3DFwd algorithm;
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(EInterpolationMode(m_eMode));
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
		//write the data
		BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
	}

	BckwdNoStreamCPUProfile::BckwdNoStreamCPUProfile(paths& _paths, EInterpolationMode _mode) :
		DeformProfileSetup("CPU_no_stream_bckwd" + mode2str(_mode), _paths, _mode) {

	}
	void BckwdNoStreamCPUProfile::runIteration(profileResults& _res) {
		volgeom3D volgeomSource;
		dvf3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
			readDVF3D(m_paths.indvf, dvfSource);
		ENDCHRONOSAVE(_res.m_readDisk)

		//prepare output image
		volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		CDeformationAlgorithm3DBckwd algorithm;
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(EInterpolationMode(m_eMode));
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_pureCompute)
		//write the data
		BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
	}

	BckwdStreamCPUProfile::BckwdStreamCPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _buffersize) :
		DeformProfileSetup("CPU_stream_bckwd" + mode2str(_mode), _paths, _mode),
		CPUStreamProfile(_buffersize){

	}
	void BckwdStreamCPUProfile::runIteration(profileResults& _res) {
		volgeom3D volgeomSource;
		dvfsource3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
			readDVF3D(m_paths.indvf, dvfSource, m_iSlicesCPU);
		ENDCHRONOSAVE(_res.m_readDisk)

		//prepare output image
		volsourcegeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		readImage3D(m_paths.outimage, volgeomTarget, new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez), m_iSlicesCPU);

		//start the algorithm
		CDeformationAlgorithm3DBckwd algorithm;
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(EInterpolationMode(m_eMode));
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
	}


#ifdef _OPENMP
	BckwdOpenMPNoStreamCPUProfile::BckwdOpenMPNoStreamCPUProfile(paths& _paths, EInterpolationMode _mode) :
		DeformProfileSetup("CPU_no_stream_openmp_bckwd" + mode2str(_mode), _paths, _mode) {

	}
	void BckwdOpenMPNoStreamCPUProfile::runIteration(profileResults& _res) {
		volgeom3D volgeomSource;
		dvf3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
		readDVF3D(m_paths.indvf, dvfSource);
		ENDCHRONOSAVE(_res.m_readDisk)

			//prepare output image
			volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		COpenMPDeformationAlgorithm3DBckwd algorithm;
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(EInterpolationMode(m_eMode));
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_pureCompute)
			//write the data
			BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
	}

	BckwdOpenMPStreamCPUProfile::BckwdOpenMPStreamCPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _buffersize) :
		DeformProfileSetup("CPU_stream_openmp_bckwd" + mode2str(_mode), _paths, _mode),
		CPUStreamProfile(_buffersize) {

	}
	void BckwdOpenMPStreamCPUProfile::runIteration(profileResults& _res) {
		volgeom3D volgeomSource;
		dvfsource3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
		readDVF3D(m_paths.indvf, dvfSource, m_iSlicesCPU);
		ENDCHRONOSAVE(_res.m_readDisk)

			//prepare output image
			volsourcegeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		readImage3D(m_paths.outimage, volgeomTarget, new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez), m_iSlicesCPU);

		//start the algorithm
		COpenMPDeformationAlgorithm3DBckwd algorithm;
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(EInterpolationMode(m_eMode));
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
	}
#endif

	FwdNoStreamSingleCopyGPUProfile::FwdNoStreamSingleCopyGPUProfile(paths& _paths, EInterpolationMode _mode) :
		DeformProfileSetup("GPU_no_stream_single_copy_fwd" + mode2str(_mode), _paths, _mode) {

	}
	void FwdNoStreamSingleCopyGPUProfile::runIteration(profileResults& _res) {
#ifdef ASTRA_CUDA
		volgeom3D volgeomSource;
		dvf3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
			readDVF3D(m_paths.indvf, dvfSource);
		ENDCHRONOSAVE(_res.m_readDisk)

		//prepare output image
		volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		CCudaBasicDeformAlgorithm3Dfwd algorithm;
		algorithm.setGPUIndex(0);
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(m_eMode);
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
		//write the data
		BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
#else
		_res.m_iterations = 0;
#endif
	}

	FwdNoStreamMultipleCopyGPUProfile::FwdNoStreamMultipleCopyGPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _buffersize, bool _asBytes) :
		DeformProfileSetup("GPU_no_stream_multiple_copy_fwd" + mode2str(_mode), _paths, _mode),
		GPUStreamProfile(_buffersize, _asBytes){

	}
	void FwdNoStreamMultipleCopyGPUProfile::runIteration(profileResults& _res) {
#ifdef ASTRA_CUDA
		volgeom3D volgeomSource;
		dvf3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
			readDVF3D(m_paths.indvf, dvfSource);
		ENDCHRONOSAVE(_res.m_readDisk)

			//prepare output image
			volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		CCudaStreamDeformAlgorithm3Dfwd algorithm;
		algorithm.setGPUIndex(0);
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(m_eMode);
		algorithm.setStreamSize(m_iSlicesGPU);
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
			//write the data
			BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
#else
		_res.m_iterations = 0;
#endif
	}

	BckwdNoStreamSingleCopyGPUProfile::BckwdNoStreamSingleCopyGPUProfile(paths& _paths, EInterpolationMode _mode) :
		DeformProfileSetup("GPU_no_stream_single_copy_bckwd" + mode2str(_mode), _paths, _mode) {

	}
	void BckwdNoStreamSingleCopyGPUProfile::runIteration(profileResults& _res) {
#ifdef ASTRA_CUDA
		volgeom3D volgeomSource;
		dvf3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
		readDVF3D(m_paths.indvf, dvfSource);
		ENDCHRONOSAVE(_res.m_readDisk)

			//prepare output image
			volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		CCudaBasicDeformAlgorithm3Dbckwd algorithm;
		algorithm.setGPUIndex(0);
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(m_eMode);
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
			//write the data
			BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
#else
		_res.m_iterations = 0;
#endif
	}

	BckwdNoStreamMultipleCopyGPUProfile::BckwdNoStreamMultipleCopyGPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _buffersize, bool _asBytes) :
		DeformProfileSetup("GPU_no_stream_multiple_copy_bckwd" + mode2str(_mode), _paths, _mode),
		GPUStreamProfile(_buffersize, _asBytes){

	}
	void BckwdNoStreamMultipleCopyGPUProfile::runIteration(profileResults& _res) {
#ifdef ASTRA_CUDA
		volgeom3D volgeomSource;
		dvf3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
		readDVF3D(m_paths.indvf, dvfSource);
		ENDCHRONOSAVE(_res.m_readDisk)

			//prepare output image
			volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		CCudaStreamDeformAlgorithm3Dbckwd algorithm;
		algorithm.setGPUIndex(0);
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(m_eMode);
		algorithm.setStreamSize(m_iSlicesGPU);
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
			//write the data
			BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
#else
		_res.m_iterations = 0;
#endif
	}

	FwdNoStreamGlobalGhostGPUProfile::FwdNoStreamGlobalGhostGPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _buffersize, bool _asBytes) :
		DeformProfileSetup("GPU_no_stream_global_ghost_fwd" + mode2str(_mode), _paths, _mode),
		GPUStreamProfile(_buffersize, _asBytes){

	}
	void FwdNoStreamGlobalGhostGPUProfile::runIteration(profileResults& _res) {
#ifdef ASTRA_CUDA
		volgeom3D volgeomSource;
		dvf3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
		readDVF3D(m_paths.indvf, dvfSource);
		ENDCHRONOSAVE(_res.m_readDisk)

			//prepare output image
			volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		astraCUDA3d::GlobalGhostSliceDeformAlgorithm3DFwd algorithm;
		algorithm.setGPUIndex(0);
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(m_eMode);
		algorithm.setMaxMemory(m_iBytesGPU ? m_iBytesGPU : calcBufferSize(m_iSlicesGPU, _res.m_sizex, _res.m_sizey));
#ifdef _DEBUG
		std::cout << "  > max used memory: " << misc::prnthelp<BYTES>(algorithm.getMaxMemory()) << '\n';
#endif
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
			//write the data
			BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
#else
		_res.m_iterations = 0;
#endif
	}

	FwdStreamMultipleCopyGPUProfile::FwdStreamMultipleCopyGPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _cpubuffersize, std::size_t _gpubuffersize, bool _asBytes):
		DeformProfileSetup("GPU_stream_multiple_copy_fwd" + mode2str(_mode), _paths, _mode),
		CPUStreamProfile(_cpubuffersize),
		GPUStreamProfile(_gpubuffersize, _asBytes) {

	}

	void FwdStreamMultipleCopyGPUProfile::runIteration(profileResults& _res) {
#ifdef ASTRA_CUDA
		volsourcegeom3D volgeomSource;
		dvfsource3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource, m_iSlicesCPU);
			readDVF3D(m_paths.indvf, dvfSource, m_iSlicesCPU);
		ENDCHRONOSAVE(_res.m_readDisk)

		//prepare output image
		volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		CCudaStreamDeformAlgorithm3Dfwd algorithm;
		algorithm.setGPUIndex(0);
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(m_eMode);
		algorithm.setStreamSize(m_iSlicesGPU);
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
			//write the data
			BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
#else
		_res.m_iterations = 0;
#endif
	}

	BckwdStreamMultipleCopyGPUProfile::BckwdStreamMultipleCopyGPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _cpubuffersize, std::size_t _gpubuffersize, bool _asBytes) :
		DeformProfileSetup("GPU_stream_multiple_copy_bckwd" + mode2str(_mode), _paths, _mode),
		CPUStreamProfile(_cpubuffersize),
		GPUStreamProfile(_gpubuffersize, _asBytes) {

	}

	void BckwdStreamMultipleCopyGPUProfile::runIteration(profileResults& _res) {
#ifdef ASTRA_CUDA
		volgeom3D volgeomSource;
		dvfsource3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
			readDVF3D(m_paths.indvf, dvfSource, m_iSlicesCPU);
		ENDCHRONOSAVE(_res.m_readDisk)

			//prepare output image
			volsourcegeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		readImage3D(m_paths.outimage, volgeomTarget, new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez), m_iSlicesCPU);


		//start the algorithm
		CCudaStreamDeformAlgorithm3Dbckwd algorithm;
		algorithm.setGPUIndex(0);
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(m_eMode);
		algorithm.setStreamSize(m_iSlicesGPU);
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
#else
		_res.m_iterations = 0;
#endif
	}

	FwdStreamGlobalGhostGPUProfile::FwdStreamGlobalGhostGPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _cpubuffersize, std::size_t _gpubuffersize, bool _asBytes) :
		DeformProfileSetup("GPU_stream_global_ghost_fwd" + mode2str(_mode), _paths, _mode),
		CPUStreamProfile(_cpubuffersize),
		GPUStreamProfile(_gpubuffersize, _asBytes){

	}
	void FwdStreamGlobalGhostGPUProfile::runIteration(profileResults& _res) {
#ifdef ASTRA_CUDA
		volsourcegeom3D volgeomSource;
		dvfsource3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource, m_iSlicesCPU);
			readDVF3D(m_paths.indvf, dvfSource, m_iSlicesCPU);
		ENDCHRONOSAVE(_res.m_readDisk)

			//prepare output image
			volsourcegeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		readImage3D(m_paths.outimage, volgeomTarget, new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez), m_iSlicesCPU);


		//start the algorithm
		astraCUDA3d::GlobalGhostSliceDeformAlgorithm3DFwd algorithm;
		algorithm.setGPUIndex(0);
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(m_eMode);
		algorithm.setMaxMemory(m_iBytesGPU ? m_iBytesGPU : calcBufferSize(m_iSlicesGPU, _res.m_sizex, _res.m_sizey));
#ifdef _DEBUG
		std::cout << "  > max used memory: " << misc::prnthelp<BYTES>(algorithm.getMaxMemory()) << '\n';
#endif
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
#else
		_res.m_iterations = 0;
#endif
	}

	BckwdNoStreamGlobalGhostGPUProfile::BckwdNoStreamGlobalGhostGPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _buffersize, bool _asBytes) :
		DeformProfileSetup("GPU_no_stream_global_ghost_bckwd" + mode2str(_mode), _paths, _mode),
		GPUStreamProfile(_buffersize, _asBytes){

	}
	void BckwdNoStreamGlobalGhostGPUProfile::runIteration(profileResults& _res) {
#ifdef ASTRA_CUDA
		volgeom3D volgeomSource;
		dvf3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource);
		readDVF3D(m_paths.indvf, dvfSource);
		ENDCHRONOSAVE(_res.m_readDisk)

			//prepare output image
			volgeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		volgeomTarget.m_geom = new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez);
		volgeomTarget.m_volume = new CFloat32VolumeData3DMemory(volgeomTarget.m_geom, 0.0f);

		//start the algorithm
		astraCUDA3d::GlobalGhostSliceDeformAlgorithm3DBckwd algorithm;
		algorithm.setGPUIndex(0);
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(m_eMode);
		algorithm.setMaxMemory(m_iBytesGPU ? m_iBytesGPU : calcBufferSize(m_iSlicesGPU, _res.m_sizex, _res.m_sizey));
#ifdef _DEBUG
		std::cout << "  > max used memory: " << misc::prnthelp<BYTES>(algorithm.getMaxMemory()) << '\n';
#endif
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
			//write the data
			BEGINCHRONO
			writeImage3D(volgeomTarget.m_volume, m_paths.outimage);
		ENDCHRONOSAVE(_res.m_writeDisk)
#else
		_res.m_iterations = 0;
#endif
	}

	BckwdStreamGlobalGhostGPUProfile::BckwdStreamGlobalGhostGPUProfile(paths& _paths, EInterpolationMode _mode, std::size_t _cpubuffersize, std::size_t _gpubuffersize, bool _asBytes) :
		DeformProfileSetup("GPU_stream_global_ghost_bckwd" + mode2str(_mode), _paths, _mode),
		CPUStreamProfile(_cpubuffersize),
		GPUStreamProfile(_gpubuffersize, _asBytes){

	}
	void BckwdStreamGlobalGhostGPUProfile::runIteration(profileResults& _res) {
#ifdef ASTRA_CUDA
		volsourcegeom3D volgeomSource;
		dvfsource3D dvfSource;
		//get input image
		BEGINCHRONO
			readImage3D(m_paths.inimage, volgeomSource, m_iSlicesCPU);
		readDVF3D(m_paths.indvf, dvfSource, m_iSlicesCPU);
		ENDCHRONOSAVE(_res.m_readDisk)

			//prepare output image
			volsourcegeom3D volgeomTarget;
		_res.m_sizex = volgeomSource.m_volume->getWidth();
		_res.m_sizey = volgeomSource.m_volume->getHeight();
		_res.m_sizez = volgeomSource.m_volume->getDepth();
		readImage3D(m_paths.outimage, volgeomTarget, new CVolumeGeometry3D(_res.m_sizex, _res.m_sizey, _res.m_sizez), m_iSlicesCPU);


		//start the algorithm
		astraCUDA3d::GlobalGhostSliceDeformAlgorithm3DBckwd algorithm;
		algorithm.setGPUIndex(0);
		algorithm.initialize(volgeomSource.m_volume, dvfSource.m_dvf, volgeomTarget.m_volume);
		algorithm.setInterpolationMode(m_eMode);
		algorithm.setMaxMemory(m_iBytesGPU ? m_iBytesGPU : calcBufferSize(m_iSlicesGPU, _res.m_sizex, _res.m_sizey));
#ifdef _DEBUG
		std::cout << "  > max used memory: " << misc::prnthelp<BYTES>(algorithm.getMaxMemory()) << '\n';
#endif
		BEGINCHRONO
			algorithm.run();
		ENDCHRONOSAVE(_res.m_mixedCompute)
#else
		_res.m_iterations = 0;
#endif
	}
}