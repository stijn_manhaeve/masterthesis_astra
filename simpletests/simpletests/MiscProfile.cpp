#include "MiscProfile.h"
#include <cmath>
#include <cstdint>
namespace profile {

	template<typename _tFloat, typename _tInt>
	inline _tInt fastfloor2int(_tFloat v) {
		return _tInt(v) - (_tInt(v) > v);
	}
	template<typename _tFloat, typename _tInt>
	inline _tInt fastceil2int(_tFloat v) {
		return _tInt(v) + (_tInt(v) < v);
	}
	template<typename _tFloat, typename _tInt>
	inline _tInt fastroundgt(_tFloat v) {
		if (v > _tFloat(0.0f))
			return fastfloor2int<_tFloat, _tInt>(v + 0.5f);
		else
			return fastceil2int<_tFloat, _tInt>(v - 0.5f);
	}
	template<typename _tFloat, typename _tInt>
	inline _tInt fastroundlt(_tFloat v) {
		if (v < _tFloat(0.0f))
			return fastceil2int<_tFloat, _tInt>(v - 0.5f);
		else
			return fastfloor2int<_tFloat, _tInt>(v + 0.5f);
	}
	template<typename _tFloat, typename _tInt>
	inline _tInt fastroundle(_tFloat v) {
		if (v <= _tFloat(0.0f))
			return fastceil2int<_tFloat, _tInt>(v - 0.5f);
		else
			return fastfloor2int<_tFloat, _tInt>(v + 0.5f);
	}
	template<typename _tFloat, typename _tInt>
	inline _tInt fastroundge(_tFloat v) {
		if (v >= 0.0f)
			return fastfloor2int<_tFloat, _tInt>(v + 0.5f);
		else
			return fastceil2int<_tFloat, _tInt>(v - 0.5f);
	}

	template<typename _tFloat, typename _tInt>
	inline _tInt fastroundsignbit(_tFloat v) {
		if (std::signbit(v))
			return fastceil2int<_tFloat, _tInt>(v - 0.5f);
		else
			return fastfloor2int<_tFloat, _tInt>(v + 0.5f);
	}

	MathTestSetup::MathTestSetup(std::string _description, std::size_t _numcalls) :
		ProfileSetup(_description), m_uiNumCalls(_numcalls) {}


	FastRoundSetupGT::FastRoundSetupGT(std::size_t _numcalls) :
		MathTestSetup("v > 0.0", _numcalls) {

	}
	void FastRoundSetupGT::runIteration(profileResults& _res) {
		volatile int x = 0;
		for (std::size_t i = 0; i < m_uiNumCalls; ++i) {
			x = fastroundgt<float, int>(float(i));
		}
	}

	FastRoundSetupGE::FastRoundSetupGE(std::size_t _numcalls) :
		MathTestSetup("v >= 0.0", _numcalls) {

	}
	void FastRoundSetupGE::runIteration(profileResults& _res) {
		volatile int x = 0;
		for (std::size_t i = 0; i < m_uiNumCalls; ++i) {
			x = fastroundge<float, int>(float(i));
		}
	}


	FastRoundSetupLT::FastRoundSetupLT(std::size_t _numcalls) :
		MathTestSetup("v < 0.0", _numcalls) {

	}
	void FastRoundSetupLT::runIteration(profileResults& _res) {
		volatile int x = 0;
		for (std::size_t i = 0; i < m_uiNumCalls; ++i) {
			x = fastroundlt<float, int>(float(i));
		}
	}

	FastRoundSetupLE::FastRoundSetupLE(std::size_t _numcalls) :
		MathTestSetup("v <= 0.0", _numcalls) {

	}
	void FastRoundSetupLE::runIteration(profileResults& _res) {
		volatile int x = 0;
		for (std::size_t i = 0; i < m_uiNumCalls; ++i) {
			x = fastroundle<float, int>(float(i));
		}
	}

	FastRoundSetupSignbit::FastRoundSetupSignbit(std::size_t _numcalls) :
		MathTestSetup("std::signbit", _numcalls) {

	}
	void FastRoundSetupSignbit::runIteration(profileResults& _res) {
		volatile int x = 0;
		for (std::size_t i = 0; i < m_uiNumCalls; ++i) {
			x = fastroundsignbit<float, int>(float(i));
		}
	}


	FastRoundSetupStdRound::FastRoundSetupStdRound(std::size_t _numcalls) :
		MathTestSetup("std::round", _numcalls) {

	}
	void FastRoundSetupStdRound::runIteration(profileResults& _res) {
		volatile int x = 0;
		for (std::size_t i = 0; i < m_uiNumCalls; ++i) {
			x = int(std::round(float(i)));
		}
	}


	FastRoundSetupStdRint::FastRoundSetupStdRint(std::size_t _numcalls) :
		MathTestSetup("std::rint", _numcalls) {

	}
	void FastRoundSetupStdRint::runIteration(profileResults& _res) {
		volatile int x = 0;
		for (std::size_t i = 0; i < m_uiNumCalls; ++i) {
			x = int(std::rint(float(i)));
		}
	}


	FMASetup::FMASetup(std::size_t _numcalls) :
		MathTestSetup("std::fma", _numcalls) {

	}
	void FMASetup::runIteration(profileResults& _res) {
		volatile int x = 0;
		for (std::size_t i = 0; i < m_uiNumCalls; ++i) {
			float t = 1.0f / (i + 1);
			float a = (i + 1);
			float b = (i - 1);
			x = std::fma(t, b, std::fma(-t, a, a));
		}
	}


	NoFMASetup::NoFMASetup(std::size_t _numcalls) :
		MathTestSetup("lerp", _numcalls) {

	}
	void NoFMASetup::runIteration(profileResults& _res) {
		volatile int x = 0;
		for (std::size_t i = 0; i < m_uiNumCalls; ++i) {
			float t = 1.0f / (i + 1);
			float a = (i + 1);
			float b = (i - 1);
			x = (1.0f - t)*a + t*b;
		}
	}

}