#ifndef TESTS_H
#define TESTS_H

#include "DeformProfile.h"

namespace profile {
	struct testpaths: paths {
		std::string m_correctNearest;
		std::string m_correctLinear;
	};

	class TestRunner {
	public:
		TestRunner();
		~TestRunner();
		void initialize(const testpaths& paths);
		void addTest(DeformProfileSetup* _profile);
		bool runTests();
		inline std::ostream* out() const { return m_out; }
		inline void out(std::ostream* _out) { m_out = _out; }
		inline bool breakOnFirstError() const { return m_breakOnFirstError; }
		inline void breakOnFirstError(bool _breakOnFirstError) { m_breakOnFirstError = _breakOnFirstError; }
	private:
		bool compareOutputs(const std::string& f1n, const std::string& f2n);

		bool m_bIsInitialized;
		bool m_breakOnFirstError;
		testpaths m_paths;
		std::vector<DeformProfileSetup*> m_tests;
		std::vector<std::string> m_failed;
		std::ostream* m_out;
		astra::float32 m_diffthresh;
		unsigned int m_maxallowed;
		inline std::ostream& print() { m_out->flush(); return *m_out; }
	};
}

#endif