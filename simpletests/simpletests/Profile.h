#ifndef PROFILE_H
#define PROFILE_H

#include "Misc.h"
#include <string>
#include <vector>

namespace profile {
	struct profileResults {
		misc::timediff m_total;				//total time taken
		misc::timediff m_readDisk;			//time taken exclusively by reading from disk
		misc::timediff m_writeDisk;			//time taken exclusively by writing to disk
		misc::timediff m_copyToDevice;		//time taken exclusively by copying to GPU device
		misc::timediff m_copyFromDevice;	//time taken exclusively by copying back to the host
		misc::timediff m_pureCompute;		//time taken exclusively for computation (GPU or CPU)
		misc::timediff m_mixedCompute;		//time taken mixing computation & memory copying/reading
		misc::timediff m_other;				//time taken by anything else, including overhead
		std::string m_description;			//description
		std::size_t m_sizex;				//size along x axis
		std::size_t m_sizey;				//size along y axis
		std::size_t m_sizez;				//size along z axis
		std::size_t m_iterations;			//number of iterations tested & averaged in this result

		static char delimiter;
		inline static void setDelimiter(char delim) { delimiter = delim; }
		inline static char getDelimiter() { return delimiter; }

		static void printHeader(std::ostream& out);

		profileResults();
		void calcOther();
		profileResults toIncremental() const;

		profileResults& operator+=(const profileResults& r);
		profileResults& operator*=(int r);
		profileResults& operator/=(int r);
	};

	std::ostream& operator<<(std::ostream& out, const profileResults& r);

	class ProfileSetup {
		friend std::ostream& operator<<(std::ostream& out, const ProfileSetup& setup);
	public:
		void run(int _iterations);

		virtual void runIteration(profileResults& _res) = 0;

		inline int resultsAveraged() const { return m_resultsAveraged; }

		inline void printAverage(bool _value) { m_printAverage = _value; }
		inline bool printAverage() const { return m_printAverage; }
		inline void warmupRound(bool _value) { m_warmupRound = _value; }
		inline bool warmupRound() const { return m_warmupRound; }
		inline std::string description() const { return m_description; }
		inline virtual ~ProfileSetup() {}
	protected:
		ProfileSetup(std::string _description);
		void averageResults();
	private:
		std::vector<profileResults> m_results;
		profileResults m_average;
		std::string m_description;
		int m_resultsAveraged;
		bool m_printAverage;
		bool m_warmupRound;
	};
} /* namespace profile */


#endif