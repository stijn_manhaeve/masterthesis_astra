#ifndef DEFORMPROFILE_H
#define DEFORMPROFILE_H

#include "Profile.h"
#include "astra\CudaDeformAlgorithm3D.h"

namespace profile {
	struct paths {
		std::string inimage;
		std::string indvf;
		std::string outimage;
	};

	using astraCUDA3d::EInterpolationMode;

	class DeformProfileSetup : public ProfileSetup {
	public:
		DeformProfileSetup(std::string _description, paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT);
		inline void paths_(paths& _paths) { m_paths = _paths; }
		inline paths paths_() const { return m_paths; }
		inline void mode(EInterpolationMode _mode) { m_eMode = _mode; }
		inline EInterpolationMode mode() const { return m_eMode; }
	protected:
		paths m_paths;
		EInterpolationMode m_eMode;
	};

	class CPUStreamProfile {
	public:
		CPUStreamProfile(std::size_t _slices);
		static const std::size_t iDefault = 40;
	protected:
		const std::size_t m_iSlicesCPU;
	};

	class GPUStreamProfile {
	public:
		GPUStreamProfile(std::size_t _slices, bool _asBytes);
		static const std::size_t iDefault = 100;
		static const std::size_t iDefaultB = 8 * 1024 * 1024;
	protected:
		std::size_t calcBufferSize(std::size_t _in, std::size_t width, std::size_t height) const;

		const std::size_t m_iSlicesGPU;
		const std::size_t m_iBytesGPU;
	};

	class FwdNoStreamCPUProfile : public DeformProfileSetup {
	public:
		FwdNoStreamCPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT);
		virtual void runIteration(profileResults& _res);
	};
	class FwdStreamCPUProfile : public DeformProfileSetup, public CPUStreamProfile {
	public:
		FwdStreamCPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _buffersize = CPUStreamProfile::iDefault);
		virtual void runIteration(profileResults& _res);
	};

	class BckwdNoStreamCPUProfile : public DeformProfileSetup {
	public:
		BckwdNoStreamCPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT);
		virtual void runIteration(profileResults& _res);
	};
	class BckwdStreamCPUProfile : public DeformProfileSetup, public CPUStreamProfile {
	public:
		BckwdStreamCPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _buffersize = CPUStreamProfile::iDefault);
		virtual void runIteration(profileResults& _res);
	};

#ifdef _OPENMP
	class BckwdOpenMPNoStreamCPUProfile : public DeformProfileSetup {
	public:
		BckwdOpenMPNoStreamCPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT);
		virtual void runIteration(profileResults& _res);
	};
	class BckwdOpenMPStreamCPUProfile : public DeformProfileSetup, public CPUStreamProfile {
	public:
		BckwdOpenMPStreamCPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _buffersize = CPUStreamProfile::iDefault);
		virtual void runIteration(profileResults& _res);
	};
#endif

	class FwdNoStreamSingleCopyGPUProfile : public DeformProfileSetup {
	public:
		FwdNoStreamSingleCopyGPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT);
		virtual void runIteration(profileResults& _res);
	};
	class FwdNoStreamMultipleCopyGPUProfile : public DeformProfileSetup, public GPUStreamProfile {
	public:
		FwdNoStreamMultipleCopyGPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _buffersize = GPUStreamProfile::iDefault, bool _asBytes = false);
		virtual void runIteration(profileResults& _res);
	};

	class BckwdNoStreamSingleCopyGPUProfile : public DeformProfileSetup {
	public:
		BckwdNoStreamSingleCopyGPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT);
		virtual void runIteration(profileResults& _res);
	};
	class BckwdNoStreamMultipleCopyGPUProfile : public DeformProfileSetup, public GPUStreamProfile {
	public:
		BckwdNoStreamMultipleCopyGPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _buffersize = GPUStreamProfile::iDefault, bool _asBytes = false);
		virtual void runIteration(profileResults& _res);
	};
	class BckwdStreamMultipleCopyGPUProfile : public DeformProfileSetup, public CPUStreamProfile, public GPUStreamProfile {
	public:
		BckwdStreamMultipleCopyGPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _cpubuffersize = CPUStreamProfile::iDefault, std::size_t _gpubuffersize = GPUStreamProfile::iDefault, bool _asBytes = false);
		virtual void runIteration(profileResults& _res);
	};

	class FwdNoStreamGlobalGhostGPUProfile : public DeformProfileSetup, public GPUStreamProfile {
	public:
		FwdNoStreamGlobalGhostGPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _buffersize = GPUStreamProfile::iDefault, bool _asBytes = false);
		virtual void runIteration(profileResults& _res);
	};
	class FwdStreamGlobalGhostGPUProfile : public DeformProfileSetup, public CPUStreamProfile, public GPUStreamProfile {
	public:
		FwdStreamGlobalGhostGPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _cpubuffersize = CPUStreamProfile::iDefault, std::size_t _gpubuffersize = GPUStreamProfile::iDefault, bool _asBytes = false);
		virtual void runIteration(profileResults& _res);
	};

	class FwdStreamMultipleCopyGPUProfile : public DeformProfileSetup, public CPUStreamProfile, public GPUStreamProfile {
	public:
		FwdStreamMultipleCopyGPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _cpubuffersize = CPUStreamProfile::iDefault, std::size_t _gpubuffersize = GPUStreamProfile::iDefault, bool _asBytes = false);
		virtual void runIteration(profileResults& _res);
	};

	class BckwdNoStreamGlobalGhostGPUProfile : public DeformProfileSetup, public GPUStreamProfile {
	public:
		BckwdNoStreamGlobalGhostGPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _buffersize = GPUStreamProfile::iDefault, bool _asBytes = false);
		virtual void runIteration(profileResults& _res);
	};
	class BckwdStreamGlobalGhostGPUProfile : public DeformProfileSetup, public CPUStreamProfile, public GPUStreamProfile {
	public:
		BckwdStreamGlobalGhostGPUProfile(paths& _paths, EInterpolationMode _mode = EInterpolationMode::DEFAULT, std::size_t _cpubuffersize = CPUStreamProfile::iDefault, std::size_t _gpubuffersize = GPUStreamProfile::iDefault, bool _asBytes = false);
		virtual void runIteration(profileResults& _res);
	};
}

#endif