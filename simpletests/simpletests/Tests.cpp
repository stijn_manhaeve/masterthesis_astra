#include "Tests.h"
#include "astra\Float32DataSource.h"

namespace profile {

	TestRunner::TestRunner() : m_bIsInitialized(false), m_out(&std::cerr),
							   m_diffthresh(1e-5f), m_maxallowed(0u)
	{}
	TestRunner::~TestRunner() {
		for (DeformProfileSetup* pr : m_tests) {
			delete pr;
		}
	}
	void TestRunner::initialize(const testpaths& paths) {
		m_paths = paths;
	}
	void TestRunner::addTest(DeformProfileSetup* _profile) {
		m_tests.push_back(_profile);
		paths p = _profile->paths_();
		p.outimage += "_test_out_" + std::to_string(m_tests.size()) + ".bin";
		_profile->paths_(p);
		_profile->warmupRound(false);
	}
	bool TestRunner::runTests() {
		int i = 0;
		for (DeformProfileSetup* pr : m_tests) {
			try{
				++i;
				std::string out = pr->paths_().outimage;
				std::string* cmp;
				switch (pr->mode()) {
				case EInterpolationMode::NEAREST_NEIGHBOR:
					cmp = &m_paths.m_correctNearest;
					break;
				case EInterpolationMode::LINEAR:
					cmp = &m_paths.m_correctLinear;
					break;
				default:
					throw std::exception("Unknown interpolation mode used.");
					break;
				}
				print() << "Running test " << i << '/' << m_tests.size() << ": " << pr->description() << '\n';
				{
					bool exists = std::ifstream(out).is_open();
					if (exists) {
						std::remove(out.c_str());
					}
				}
				print() << " > excecuting code...\n";
				pr->run(1);
				print() << " > comparing output...\n";
				bool success = compareOutputs(out, *cmp);
				if (success){
					print() << " > ok\n";
				}
				else {
					print() << " > failure\n";
					m_failed.push_back(pr->description());
					if (m_breakOnFirstError) {
						print() << ">>>breaking on first error.\n";
						return false;
					}
				}
			}
			catch (std::exception e) {
				print() << " > Caught exception: " << e.what() << '\n';
				m_failed.push_back(pr->description());
			}
			catch (...) {
				print() << " > Caught unknown exception\n";
				m_failed.push_back(pr->description());
			}
		}
		print() << "=============\n";
		print() << "== results ==\n";
		print() << "=============\n";
		if (m_failed.empty()) {
			print() << " > all tests successful\n";
		}
		else {
			print() << " > " << m_failed.size() << " tests failed: \n";
			for (const std::string& str : m_failed) {
				print() << "     " << str << '\n';
			}
		}
		return m_failed.empty();
	}

	struct imgsource3D {
		CFloat32DataBufferedFile* m_file;
		CFloat32RawDataFile* m_source;
		imgsource3D() : m_file(NULL), m_source(NULL) {}
		~imgsource3D() {
			delete m_file;
			delete m_source;
		}
	};
	bool TestRunner::compareOutputs(const std::string& f1n, const std::string& f2n) {
		print() << "   comparing " << f1n << " with " << f2n << '\n';
		imgsource3D f1c, f2c;
		f1c.m_source = new CFloat32RawDataFile();
		f1c.m_source->open(f1n, CFloat32DataSource::READ);
		f1c.m_file = new CFloat32DataBufferedFile();
		f1c.m_file->initialize(f1c.m_source);
		CFloat32DataBufferedFile* f1 = f1c.m_file;

		f2c.m_source = new CFloat32RawDataFile();
		f2c.m_source->open(f2n, CFloat32DataSource::READ);
		f2c.m_file = new CFloat32DataBufferedFile();
		f2c.m_file->initialize(f2c.m_source);
		CFloat32DataBufferedFile* f2 = f2c.m_file;
		
		int dims = f1->readDimensionCount();
		if (dims != 2 && dims != 3) {
			print() << "   expected number of dimensions in {2, 3}, but got " << dims << " instead \n";
			return false;
		}
		if (dims != f2->readDimensionCount()) {
			print() << "   number of dimensions are not the same: " << dims << "!=" << f2->readDimensionCount() << 'n';
			return false;
		}
		for (int i = 0; i < dims; ++i) {
			if (f1->readDimensionSize(i) != f2->readDimensionSize(i)) {
				print() << "   dimension " << i << " is not the same: " << f1->readDimensionSize(i) << "!=" << f2->readDimensionSize(i) << 'n';
				return false;
			}
		}
		int width = f1->readDimensionSize(0);
		int height = f1->readDimensionSize(1);
		int depth = dims == 2 ? 1 : f1->readDimensionSize(2);
		std::size_t index = 0u;
		std::size_t errornum = 0u;
		float32 errorlarge = -1.0f;
		bool noerror = true;
		int x = 0;
		int y = 0;
		int z = 0;
		try {
			for (z = 0; z < depth; ++z) {
				for (y = 0; y < height; ++y) {
					for (x = 0; x < width; ++x, ++index) {
						float32 val1 = f1->readValue(index);
						float32 val2 = f2->readValue(index);
						float32 diff = std::abs(val1 - val2);
						errorlarge = std::max(errorlarge, diff);
						if (diff > m_diffthresh) {
							++errornum;
							if (errornum > m_maxallowed) {
								noerror = false;
							}
							if (m_breakOnFirstError) {
								if (errornum > m_maxallowed) {
									print() << "   Error: value difference too large\n";
								}
								else {
									print() << "   Warning: value difference too large\n";
								}
								print() << "    > location = [" << x << ", " << y << ", " << z << "] out of [" << width << ", " << height << ", " << depth << "]\n";
								print() << "    > diff = " << diff << ", max recorded diff = " << errorlarge << ", number of errors = " << errornum << '\n';
								print() << "    > max allowed diff = " << m_diffthresh << ", max number of errors = " << m_maxallowed << '\n';
							}
							if (m_breakOnFirstError && !noerror) {
								return noerror;
							}
						}
					}
				}
			}
		}
		catch (std::exception e) {
			print() << "   Caught exception: " << e.what() << '\n';
			print() << "    > location = [" << x << ", " << y << ", " << z << "] out of [" << width << ", " << height << ", " << depth << "]\n";
			return false;
		}
		catch (...) {
			print() << "   Caught unknown exception\n";
			print() << "    > location = [" << x << ", " << y << ", " << z << "] out of [" << width << ", " << height << ", " << depth << "]\n";
			return false;
		}

		return noerror;
	}
}