#include "Profile.h"
#include <assert.h>

using namespace misc;

namespace profile {
	profileResults::profileResults() : m_sizex(0u), m_sizey(0u), m_sizez(0u), m_iterations(1u) {}
	void profileResults::calcOther() {
		m_other = m_total
				- m_readDisk
				- m_writeDisk
				- m_copyToDevice
				- m_copyFromDevice
				- m_pureCompute
				- m_mixedCompute;
	}
	profileResults profileResults::toIncremental() const {
		profileResults res;
		res.m_readDisk = m_readDisk;
		res.m_writeDisk = res.m_readDisk + m_writeDisk;
		res.m_copyToDevice = res.m_writeDisk + m_copyToDevice;
		res.m_copyFromDevice = res.m_copyToDevice + m_copyFromDevice;
		res.m_pureCompute = res.m_copyFromDevice + m_pureCompute;
		res.m_mixedCompute = res.m_pureCompute + m_mixedCompute;
		res.m_other = res.m_mixedCompute + m_other;
		res.m_total = m_total;
		res.m_description = m_description;

		return res;
	}

	char profileResults::delimiter = ';';
	void profileResults::printHeader(std::ostream& out) {
		out << "description" << delimiter
			<< "time total (s)" << delimiter
			<< "time read from disk (s)" << delimiter
			<< "time write to disk (s)" << delimiter
			<< "time copy to device (s)" << delimiter
			<< "time copy from device (s)" << delimiter
			<< "time pure computation (s)" << delimiter
			<< "time mixed computation (s)" << delimiter
			<< "time other (s)" << delimiter
			<< "size x" << delimiter
			<< "size y" << delimiter
			<< "size z" << delimiter
			<< "iterations" << '\n';
	}

	profileResults& profileResults::operator+=(const profileResults& r) {
		assert(m_sizex == 0 || r.m_sizex == 0 || m_sizex == r.m_sizex);
		assert(m_sizey == 0 || r.m_sizey == 0 || m_sizey == r.m_sizey);
		assert(m_sizez == 0 || r.m_sizez == 0 || m_sizez == r.m_sizez);

		m_total += r.m_total;
		m_readDisk += r.m_readDisk;
		m_writeDisk += r.m_writeDisk;
		m_copyToDevice += r.m_copyToDevice;
		m_copyFromDevice += r.m_copyFromDevice;
		m_pureCompute += r.m_pureCompute;
		m_mixedCompute += r.m_mixedCompute;
		m_other += r.m_other;
		m_iterations += r.m_iterations;
		if (!m_sizex) m_sizex = r.m_sizex;
		if (!m_sizey) m_sizey = r.m_sizey;
		if (!m_sizez) m_sizez = r.m_sizez;
		return *this;
	}
	profileResults& profileResults::operator*=(int r) {
		m_total *= r;
		m_readDisk *= r;
		m_writeDisk *= r;
		m_copyToDevice *= r;
		m_copyFromDevice *= r;
		m_pureCompute *= r;
		m_mixedCompute *= r;
		m_other *= r;
		return *this;
	}
	profileResults& profileResults::operator/=(int r) {
		m_total /= r;
		m_readDisk /= r;
		m_writeDisk /= r;
		m_copyToDevice /= r;
		m_copyFromDevice /= r;
		m_pureCompute /= r;
		m_mixedCompute /= r;
		m_other /= r;
		return *this;
	}
	std::ostream& operator<<(std::ostream& out, const profileResults& r) {
		out << r.m_description << profileResults::delimiter
			<< prnthelp<TIMESECONDS>(r.m_total) << profileResults::delimiter
			<< prnthelp<TIMESECONDS>(r.m_readDisk) << profileResults::delimiter
			<< prnthelp<TIMESECONDS>(r.m_writeDisk) << profileResults::delimiter
			<< prnthelp<TIMESECONDS>(r.m_copyToDevice) << profileResults::delimiter
			<< prnthelp<TIMESECONDS>(r.m_copyFromDevice) << profileResults::delimiter
			<< prnthelp<TIMESECONDS>(r.m_pureCompute) << profileResults::delimiter
			<< prnthelp<TIMESECONDS>(r.m_mixedCompute) << profileResults::delimiter
			<< prnthelp<TIMESECONDS>(r.m_other) << profileResults::delimiter
			<< r.m_sizex << profileResults::delimiter
			<< r.m_sizey << profileResults::delimiter
			<< r.m_sizez << profileResults::delimiter
			<< r.m_iterations;
		return out;
	}


	ProfileSetup::ProfileSetup(std::string _description) : m_description(_description),
															m_printAverage(true),
															m_resultsAveraged(0)
	{
		m_average.m_description = _description;
		m_average.m_iterations = 0u;
	}

	void ProfileSetup::averageResults() {
		m_average *= m_resultsAveraged;
		for (auto it = m_results.begin() + m_resultsAveraged; it < m_results.end(); ++it) {
			m_average += *it;
		}
		m_resultsAveraged = m_results.size();
		m_average /= m_resultsAveraged;
	}
	void ProfileSetup::run(int _iterations) {
		auto it = m_results.insert(m_results.end(), _iterations, profileResults());
		if (m_warmupRound) {
			std::cout << m_description << " warmup run...";
			profileResults res;
			try {
				runIteration(res);
			}
			catch (const std::exception& e) {
				std::cout << "\n > caught exception: " << e.what() << '\n';
				it->m_description = m_description + std::string("_exception: ") + e.what();
				return;
			}
			std::cout << "done\n";
		}
		for (int i = 0; i < _iterations; ++i) {
			if (i) std::cout << '\n';
			it->m_description = m_description;
			std::cout << m_description << " iteration " << i + 1 << "/" << _iterations << "...";

			try {
				BEGINCHRONO
					runIteration(*it);
				ENDCHRONOSAVE(it->m_total)
			}
			catch (const std::exception& e) {
				std::cout << "\n > caught exception: " << e.what() << '\n';
				it->m_description += std::string("_exception: ") + e.what();
				++it;
				continue;
			}
			std::cout << "done";
			it->calcOther();
			++it;
		}
		averageResults();
		std::cout << " \t(" << prnthelp<TIME>(m_average.m_total) << ")\n";
	}
	std::ostream& operator<<(std::ostream& out, const ProfileSetup& setup) {
		if (setup.m_printAverage) {
			out << setup.m_average << '\n';
		} else {
			for (const profileResults& res : setup.m_results) {
				out << res << '\n';
			}
		}
		return out;
	}
}