/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
*/


#ifndef _CUDA_DEFORM_H
#define _CUDA_DEFORM_H

#include "dims.h"
#include "util.h"
#include "astra/Interpolation.h"
#include <iostream>
#include <driver_functions.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>

namespace astraCUDA {

	/**
	 * Structure for packing the parameters for the 2D deformation kernels
	 */
	struct SKernelArguments {
		float* __restrict__ d_imagedata;	/// device pointer to input image data
		float* __restrict__ d_dx;			/// device pointer to the X component of the DVF data
		float* __restrict__ d_dy;			/// device pointer to the Y component of the DVF data
		float* __restrict__ d_result;		/// device pointer to where the output is stored
		unsigned int pitch;					/// pitch of the device memory in bytes
		unsigned int width;					/// width of the image
		unsigned int height;				/// height of the image
		int startx;							/// offset along the x axis to the first voxel processed by a kernel
		int starty;							/// offset along the y axis to the first voxel processed by a kernel

		SKernelArguments();
	};

	// output operators for easy printing
	std::ostream& operator<<(std::ostream& out, const SKernelArguments& args);
	std::ostream& operator<<(std::ostream& out, const dim3& d);

	/**
	 * Calculates the absolute offset from the relative offset stored in the DVF
	 */
	__device__ __host__ float rel2absolute(float delta, size_t size);

	// CUDA kernels can't be static member functions.
	/**
	 * Kernel for 2D deformation, nearest neighbour interpolation, forward method.
	 */
	__global__ void interpnearestfwd(SKernelArguments args);

	/**
	 * Kernel for 2D deformation, linear interpolation, forward method.
	 */
	__global__ void interplinearfwd(SKernelArguments args);

	/**
	 * Kernel for 2D deformation, nearest neighbour interpolation, backward method.
	 */
	__global__ void interpnearestbckwd(SKernelArguments args);

	/**
	 * Kernel for 2D deformation, linear interpolation, backward method.
	 */
	__global__ void interplinearbckwd(SKernelArguments args);


	//putting the kernels into a struct template makes it easier to call them
	//and to add more interpolation modes later on.
	//inlining everything allows the compiler to optimize away the boilerplate code

	//reuse the same enum for the interpolation mode
	typedef astra::EInterpolationMode EInterpolationMode;
	using astra::DEFAULT;
	using astra::NEAREST_NEIGHBOR;
	using astra::LINEAR;

	/**
	 * Struct template for the 2D deformation kernels, forward method
	 * All implementations provide a static run function.
	 */
	template<EInterpolationMode mode>
	struct interpolate2Dfwd {};

	/**
	 * Specialization for nearest neighbor interpolation, forward method.
	 */
	template<>
	struct interpolate2Dfwd<NEAREST_NEIGHBOR> {
		/**
		 * Executes a deformation kernel
		 * @param kernelargs The arguments struct that is passed on to the kernel itself.
		 * @param args		 Arguments for starting the kernel. See the CUDA programming guide.
		 */
		template<typename... Args>
		static void run(const SKernelArguments& kernelargs, Args... args) {
			interpnearestfwd<<<args... >>>(kernelargs);
		}
	};

	/**
	* Specialization for linear interpolation, forward method.
	*/
	template<>
	struct interpolate2Dfwd<LINEAR> {
		/**
		 * Executes a deformation kernel
		 * @param kernelargs The arguments struct that is passed on to the kernel itself.
		 * @param args		 Arguments for starting the kernel. See the CUDA programming guide.
		 */
		template<typename... Args>
		static void run(const SKernelArguments& kernelargs, Args... args) {
			interplinearfwd<<<args...>>>(kernelargs);
		}
	};


	/**
	 * Struct template for the 2D deformation kernels, backward method
	 * All implementations provide a static run function.
	 */
	template<EInterpolationMode mode>
	struct interpolate2Dbckwd {};

	/**
	 * Specialization for nearest neighbor interpolation, backward method.
	 */
	template<>
	struct interpolate2Dbckwd<NEAREST_NEIGHBOR> {
		/**
		 * Executes a deformation kernel
		 * @param kernelargs The arguments struct that is passed on to the kernel itself.
		 * @param args		 Arguments for starting the kernel. See the CUDA programming guide.
		 */
		template<typename... Args>
		static void run(const SKernelArguments& kernelargs, Args... args) {
			interpnearestbckwd<<<args...>>>(kernelargs);
		}
	};

	/**
	 * Specialization for linear interpolation, backward method.
	 */
	template<>
	struct interpolate2Dbckwd<LINEAR> {
		/**
		* Executes a deformation kernel
		* @param kernelargs The arguments struct that is passed on to the kernel itself.
		* @param args		 Arguments for starting the kernel. See the CUDA programming guide.
		*/
		template<typename... Args>
		static void run(const SKernelArguments& kernelargs, Args... args) {
			interplinearbckwd<<<args...>>>(kernelargs);
		}
	};

	/**
	 * Function template for starting the deformation kernels, forward method.
	 * This function removes the reliance on a template parameter.
	 */
	template<typename... Args>
	void doInterpolationfwd(EInterpolationMode mode, Args... args) {
		switch (mode){
			case NEAREST_NEIGHBOR:
				interpolate2Dfwd<NEAREST_NEIGHBOR>::run(args...);
				break;
			case LINEAR:
				interpolate2Dfwd<LINEAR>::run(args...);
				break;
		}
	}

	/**
	 * Function template for starting the deformation kernels, backward method.
	 * This function removes the reliance on a template parameter.
	 */
	template<typename... Args>
	void doInterpolationbckwd(EInterpolationMode mode, Args... args) {
		switch (mode){
			case NEAREST_NEIGHBOR:
				interpolate2Dbckwd<NEAREST_NEIGHBOR>::run(args...);
				break;
			case LINEAR:
				interpolate2Dbckwd<LINEAR>::run(args...);
				break;
		}
	}

	/**
	 * Base class for the 2D CUDA deformation algorithms
	 */
	class _AstraExport DeformAlgorithm2D {
	public:
		DeformAlgorithm2D();
		~DeformAlgorithm2D();

		/**
		 * Finishes algorithm initialization
		 */
		virtual bool init();

		/**
		 * Sets the dimensions of the image that will be processed.
		 * setDimensions and optionally setBuffers must be called before invoking run.
		 */
		void setDimensions(const SDimensions& _dims);

		/**
		* Sets the pointers to the buffers in device memory where the data is located.
		* Use this method if you already have these buffers or want to manually manage them.
		 * setDimensions and optionally setBuffers must be called before invoking run.
		*/
		bool setBuffers(const cudaPitchedPtr& _D_fSource,
						const cudaPitchedPtr& _D_fDVFx,
						const cudaPitchedPtr& _D_fDVFy,
						const cudaPitchedPtr& _D_fTarget);

		/**
		 * Stores pointers to the current configured device memory buffers in the function arguments.
		 */
		bool getBuffers(cudaPitchedPtr& _D_fSource,
						cudaPitchedPtr& _D_fDVFx,
						cudaPitchedPtr& _D_fDVFy,
						cudaPitchedPtr& _D_fTarget);

		/**
		 * Runs the algorithm.
		 * This function may only be called after setDimensions and optionally setBuffers were called.
		 */
		virtual void run() = 0;

		/**
		 * Resets the algorithm initialization
		 */
		void reset();

		/**
		 * Sets the index of the GPU that will be used to perform computation.
		 * The first GPU has index 0.
		 *
		 * @param _iGPUIndex New GPU index.
		 * @see getGPUIndex
		 */
		void setGPUIndex(int _iGPUIndex) { m_iGPUIndex = _iGPUIndex; }

		/**
		 * Sets the index of the GPU that will be used to perform computation.
		 * The first GPU has index 0.
		 *
		 * @return the index of the GPU that will be used to perform computation.
		 * @see getGPUIndex
		 */
		inline int getGPUIndex() const { return m_iGPUIndex;  }

		/** Signal the algorithm it should abort soon.
		 *  This is intended to be called from a different thread
		 *  while the algorithm is running. There are no guarantees
		 *  on how soon the algorithm will abort. The state of the
		 *  algorithm object will be consistent (so it is safe to delete it
		 *  normally afterwards), but the algorithm's output is undefined.
		 *
		 *  Note that specific algorithms may give guarantees on their
		 *  state after an abort. Check their documentation for details.
		 */
		void signalAbort() { m_shouldAbort = true; }

		/**
		 * Sets the interpolation mode.
		 * @see getInterpolationMode
		 * @see EInterpolationMode
		 */
		inline void setInterpolationMode(EInterpolationMode _eMode) { m_eMode = _eMode;	}

		/**
		 * Returns the currently used interpolation mode.
		 * @see setInterpolationMode
		 * @see EInterpolationMode
		 */
		inline EInterpolationMode getInterpolationMode() const { return m_eMode; }

	protected:
		virtual void _allocateBuffers();
		bool _check() const;

		bool m_bInitialized;
		volatile bool m_shouldAbort;
		int m_iGPUIndex;
		SDimensions dims;
		EInterpolationMode m_eMode;

		cudaPitchedPtr D_fSource;
		cudaPitchedPtr D_fDVFx;
		cudaPitchedPtr D_fDVFy;
		cudaPitchedPtr D_fTarget;

	};

	/**
	 * Implementation of the 2D deformation algorithm class for the forward method.
	 */
	class _AstraExport DeformAlgorithm2DFwd : public DeformAlgorithm2D {
	public:
		// run must be called after setDimensions and setBuffers
		virtual void run();
	};

	/**
	 * Implementation of the 3D deformation algorithm class for the forward method.
	 */
	class _AstraExport DeformAlgorithm2DBckwd : public DeformAlgorithm2D {
	public:
		// run must be called after setDimensions and setBuffers
		virtual void run();
	};

}

#endif

