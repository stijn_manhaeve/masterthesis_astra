/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "deform.h"

#include "util.h"
#include "astra/Globals.h"
#include <iostream>
#include <vector>

namespace astraCUDA {

	std::ostream& operator<<(std::ostream& out, const SKernelArguments& args) {
		out << "{ " //"d_image: " << args.d_imagedata
			//<< ", d_result: " << args.d_result
			//<< ", d_d*: [" << args.d_dx << ", " << args.d_dy << ", " << args.d_dz
			<< "], whd: [" << args.width << ", " << args.height
			<< "], start*: [" << args.startx << ", " << args.starty
			<< "], pitch: " << args.pitch
			<< "}";
		return out;
	}

	std::ostream& operator<<(std::ostream& out, const dim3& d) {
		return out << '[' << d.x << ", " << d.y << ", " << d.z << ']';
	}

	__device__ __host__ float rel2absolute(float delta, size_t size) {
		return delta * float(size) / 2.0f;
	}

	__device__ unsigned int getOffset(unsigned int posx, unsigned int posy,
									  unsigned int pitch) {
		return pitch * posy + posx;
	}

	__device__ void putAddItem(float v, float* d, int posx, int posy,
							   unsigned int pitch, int width, int height) {
		if (posx >= 0 && posx < width &&
			posy >= 0 && posy < height) {
			unsigned int offset = getOffset(posx, posy, pitch);
			atomicAdd(d + offset, v);
		}
	}

	__device__ void setItem(float v, float* d, int posx, int posy,
							unsigned int pitch) {
		unsigned int offset = getOffset(posx, posy, pitch);
		d[offset] = v;
	}

	__device__ float readItem(float* d, int posx, int posy,
							  unsigned int pitch, int width, int height) {

		if (posx >= 0 && posx < width &&
			posy >= 0 && posy < height) {
			unsigned int offset = getOffset(posx, posy, pitch);
			return d[offset];
		}
		return 0.0f;

	}

	SKernelArguments::SKernelArguments() :d_imagedata(0),
		d_dx(0), d_dy(0),
		d_result(0),
		pitch(0u),
		width(0u), height(0u),
		startx(0u), starty(0u) {}

	__global__ void interpnearestfwd(SKernelArguments args) {
		// get current position
		int posx = blockIdx.x * blockDim.x + threadIdx.x;
		int posy = blockIdx.y * blockDim.y + threadIdx.y;
		if ((posx + args.startx >= args.width) ||
			(posx + args.startx < 0) ||
			(posy + args.starty >= args.height) ||
			(posy + args.starty < 0)) {
			return;
		}
		// get the value and delta
		size_t offset = getOffset(posx, posy, args.pitch);
		float value = args.d_imagedata[offset];
		float dx = args.d_dx[offset];
		float dy = args.d_dy[offset];

		dx = rel2absolute(dx, args.width);
		dy = rel2absolute(dy, args.height);

		posx += args.startx + int(rintf(dx));
		posy += args.starty + int(rintf(dy));
		putAddItem(value, args.d_result,
				   posx, posy,
				   args.pitch, args.width, args.height);
	}

	__global__ void interplinearfwd(SKernelArguments args) {
		// get current position
		int posx = blockIdx.x * blockDim.x + threadIdx.x;
		int posy = blockIdx.y * blockDim.y + threadIdx.y;

		if ((posx + args.startx >= args.width) ||
			(posx + args.startx < 0) ||
			(posy + args.starty >= args.height) ||
			(posy + args.starty < 0)) {
			return;
		}
		// get the value and delta
		size_t offset = getOffset(posx, posy, args.pitch);
		float value = args.d_imagedata[offset];
		float dx = args.d_dx[offset];
		float dy = args.d_dy[offset];

		dx = rel2absolute(dx, args.width);
		dy = rel2absolute(dy, args.height);

		float factualx = posx + dx;
		float factualy = posy + dy;

		//round and calculate the fractions of the eight pixels
		//the value must be spread over
		float froundx = rintf(factualx);
		float froundy = rintf(factualy);

		int ileftx, itopy;
		float fleftfraction, ftopfraction;

		if (froundx <= factualx) {
			ileftx = int(froundx);
			fleftfraction = 1.0f - (factualx - froundx);
		} else {
			ileftx = int(froundx) - 1;
			fleftfraction = (froundx - factualx);
		}
		if (froundy <= factualy) {
			itopy = int(froundy);
			ftopfraction = 1.0f - (factualy - froundy);
		} else {
			itopy = int(froundy) - 1;
			ftopfraction = (froundy - factualy);
		}
		//calculate the four values added to the four pixels
		float a = (fleftfraction          * ftopfraction) * value;
		float b = ((1.0f - fleftfraction) * ftopfraction) * value;
		float c = (fleftfraction          * (1.0f - ftopfraction)) * value;
		float d = ((1.0f - fleftfraction) * (1.0f - ftopfraction)) * value;

		//add these values to the output image
		ileftx += args.startx;
		itopy += args.starty;
		putAddItem(a, args.d_result, ileftx, itopy, args.pitch, args.width, args.height);
		putAddItem(b, args.d_result, ileftx + 1, itopy, args.pitch, args.width, args.height);
		putAddItem(c, args.d_result, ileftx, itopy + 1, args.pitch, args.width, args.height);
		putAddItem(d, args.d_result, ileftx + 1, itopy + 1, args.pitch, args.width, args.height);
	}

	__global__ void interpnearestbckwd(SKernelArguments args) {
		// get current position
		const int posx = blockIdx.x * blockDim.x + threadIdx.x;
		const int posy = blockIdx.y * blockDim.y + threadIdx.y;

		if ((posx >= args.width) ||
			(posy >= args.height)) {
			return;
		}
		// get the position
		size_t offset = getOffset(posx, posy, args.pitch);
		float dx = args.d_dx[offset];
		float dy = args.d_dy[offset];

		dx = rel2absolute(dx, args.width);
		dy = rel2absolute(dy, args.height);

		int actualx = posx + args.startx + int(rintf(dx));
		int actualy = posy + args.starty + int(rintf(dy));
		//get the value
		float val = readItem(args.d_imagedata, actualx, actualy, args.pitch, args.width, args.height);

		setItem(val, args.d_result, posx, posy, args.pitch);
	}

	__device__ float lerp(float a, float b, float t) {
		return fma(t, b, fma(-t, a, a));
	}
	__device__ float lerp(float a, float b, float c, float d, float t1, float t2) {
		return lerp(lerp(a, b, t1), lerp(c, d, t1), t2);
	}

	__global__ void interplinearbckwd(SKernelArguments args) {
		// get current position
		const int posx = blockIdx.x * blockDim.x + threadIdx.x;
		const int posy = blockIdx.y * blockDim.y + threadIdx.y;

		if (posx >= args.width || posy >= args.height) {
			return;
		}
		// get the position
		size_t offset = getOffset(posx, posy, args.pitch);
		float dx = args.d_dx[offset];
		float dy = args.d_dy[offset];

		dx = posx + args.startx + rel2absolute(dx, args.width);
		dy = posy + args.starty + rel2absolute(dy, args.height);

		const int lx = floorf(dx);
		const int ly = floorf(dy);
		const int lxp1 = lx + 1;
		const int lyp1 = ly + 1;
		const float tx = dx - lx;
		const float ty = dy - ly;

		float a = readItem(args.d_imagedata, lx, ly, args.pitch, args.width, args.height);
		float b = readItem(args.d_imagedata, lxp1, ly, args.pitch, args.width, args.height);
		float c = readItem(args.d_imagedata, lx, lyp1, args.pitch, args.width, args.height);
		float d = readItem(args.d_imagedata, lxp1, lyp1, args.pitch, args.width, args.height);
		float val = lerp(a, b, c, d, tx, ty);

		setItem(val, args.d_result, posx, posy, args.pitch);
	}

	DeformAlgorithm2D::DeformAlgorithm2D() {
		D_fSource.ptr = 0;
		D_fDVFx.ptr = 0;
		D_fDVFy.ptr = 0;
		D_fTarget.ptr = 0;

		m_shouldAbort = false;
		m_bInitialized = false;
		m_iGPUIndex = -1;
		m_eMode = DEFAULT;
	}
	DeformAlgorithm2D::~DeformAlgorithm2D() {
		reset();
	}

	bool DeformAlgorithm2D::init() {
		if (m_bInitialized) return true;
		if (m_iGPUIndex < 0)
			m_iGPUIndex = 0;

		if (!_check()) _allocateBuffers();
		m_bInitialized = true;
		return true;
	}
	void DeformAlgorithm2D::setDimensions(const SDimensions& _dims) {
		dims = _dims;
	}
	bool DeformAlgorithm2D::setBuffers(const cudaPitchedPtr& _D_fSource,
									   const cudaPitchedPtr& _D_fDVFx,
									   const cudaPitchedPtr& _D_fDVFy,
									   const cudaPitchedPtr& _D_fTarget) {
		D_fSource = _D_fSource;
		D_fDVFx = _D_fDVFx;
		D_fDVFy = _D_fDVFy;
		D_fTarget = _D_fTarget;

		return true;
	}
	bool DeformAlgorithm2D::getBuffers(cudaPitchedPtr& _D_fSource,
									   cudaPitchedPtr& _D_fDVFx,
									   cudaPitchedPtr& _D_fDVFy,
									   cudaPitchedPtr& _D_fTarget) {
		_D_fSource = D_fSource;
		_D_fDVFx = D_fDVFx;
		_D_fDVFy = D_fDVFy;
		_D_fTarget = D_fTarget;
		return true;
	}

	void DeformAlgorithm2D::reset() {
		cudaFree(D_fSource.ptr);
		cudaFree(D_fDVFx.ptr);
		cudaFree(D_fDVFy.ptr);
		cudaFree(D_fTarget.ptr);
		D_fSource.ptr = 0;
		D_fDVFx.ptr = 0;
		D_fDVFy.ptr = 0;
		D_fTarget.ptr = 0;

		m_shouldAbort = false;
	}

	void DeformAlgorithm2D::_allocateBuffers() {
		unsigned int pitch;
		float* ptr;
		allocateVolumeData(ptr, pitch, dims);
		D_fSource.ptr = ptr;
		D_fSource.pitch = pitch;
		allocateVolumeData(ptr, pitch, dims);
		D_fDVFx.ptr = ptr;
		D_fDVFx.pitch = pitch;
		allocateVolumeData(ptr, pitch, dims);
		D_fDVFy.ptr = ptr;
		D_fDVFy.pitch = pitch;
		allocateVolumeData(ptr, pitch, dims);
		D_fTarget.ptr = ptr;
		D_fTarget.pitch = pitch;
		ASTRA_ASSERT(_check());
	}

	bool DeformAlgorithm2D::_check() const {
		if (D_fSource.ptr == 0) return false;
		if (D_fDVFx.ptr == 0) return false;
		if (D_fDVFy.ptr == 0) return false;
		if (D_fTarget.ptr == 0) return false;

		if (D_fSource.pitch != D_fDVFx.pitch) return false;
		if (D_fDVFx.pitch != D_fDVFy.pitch) return false;
		if (D_fDVFy.pitch != D_fTarget.pitch) return false;
		return true;
	}

	void DeformAlgorithm2DFwd::run() {
		const unsigned int bldim = 16u;
		dim3 blocksize(bldim, bldim);
		dim3 gridsize((dims.iVolWidth + bldim - 1) / bldim, (dims.iVolHeight + bldim - 1) / bldim, 1);

		SKernelArguments args;
		args.d_imagedata = (float*)D_fSource.ptr;
		args.d_dx = (float*)D_fDVFx.ptr;
		args.d_dy = (float*)D_fDVFy.ptr;
		args.d_result = (float*)D_fTarget.ptr;
		args.pitch = D_fSource.pitch / sizeof(float);
		args.width = dims.iVolWidth;
		args.height = dims.iVolHeight;

		doInterpolationfwd(m_eMode, args, gridsize, blocksize);
		cudaTextForceKernelsCompletion();
		cudaError_t err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
	}

	void DeformAlgorithm2DBckwd::run() {
		const unsigned int bldim = 16u;
		dim3 blocksize(bldim, bldim);
		dim3 gridsize((dims.iVolWidth + bldim - 1) / bldim, (dims.iVolHeight + bldim - 1) / bldim, 1);

		SKernelArguments args;
		args.d_imagedata = (float*)D_fSource.ptr;
		args.d_dx = (float*)D_fDVFx.ptr;
		args.d_dy = (float*)D_fDVFy.ptr;
		args.d_result = (float*)D_fTarget.ptr;
		args.pitch = D_fSource.pitch / sizeof(float);
		args.width = dims.iVolWidth;
		args.height = dims.iVolHeight;

		doInterpolationbckwd(m_eMode, args, gridsize, blocksize);
		cudaTextForceKernelsCompletion();
	}
}