/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
*/

#ifndef _CUDA_DEFORM3D_H
#define _CUDA_DEFORM3D_H

#include "dims3d.h"
#include "util3d.h"
#include "astra/Interpolation.h"
#include "astra/Float32Data2D.h"
#include <iostream>
#include <driver_functions.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>

namespace astraCUDA3d {

	struct SKernelArguments3D {
		float* __restrict__ d_imagedata;
		float* __restrict__ d_dx;
		float* __restrict__ d_dy;
		float* __restrict__ d_dz;
		float* __restrict__ d_result;
		unsigned int pitch;
		unsigned int width;
		unsigned int height;
		unsigned int depth;
		int startx;
		int starty;
		int startz;
		int offsetSlices;
		unsigned int targetSlices;
		unsigned int sourceSlices;

		SKernelArguments3D();
	};

	std::ostream& operator<<(std::ostream& out, const SKernelArguments3D& args);

	__device__ __host__ float rel2absolute(float delta, size_t size);
	__global__ void interpnearestfwd(SKernelArguments3D args);
	__global__ void interplinearfwd(SKernelArguments3D args);
	__global__ void interpnearestbckwd(SKernelArguments3D args);
	__global__ void interplinearbckwd(SKernelArguments3D args);

	typedef astra::EInterpolationMode EInterpolationMode;
	using astra::DEFAULT;
	using astra::NEAREST_NEIGHBOR;
	using astra::LINEAR;
	using astra::CUBICBSPLINE;

	template<EInterpolationMode mode>
	struct interpolate3Dfwd {};

	template<>
	struct interpolate3Dfwd<NEAREST_NEIGHBOR> {
		/**
		 * Executes a deformation kernel
		 * @param kernelargs The arguments struct that is passed on to the kernel itself.
		 * @param args		 Arguments for starting the kernel. See the CUDA programming guide.
		 */
		template<typename... Args>
		static void run(const SKernelArguments3D& kernelargs, Args... args) {
			interpnearestfwd<<<args... >>>(kernelargs);
		}
	};

	template<>
	struct interpolate3Dfwd<LINEAR> {
		/**
		 * Executes a deformation kernel
		 * @param kernelargs The arguments struct that is passed on to the kernel itself.
		 * @param args		 Arguments for starting the kernel. See the CUDA programming guide.
		 */
		template<typename... Args>
		static void run(const SKernelArguments3D& kernelargs, Args... args) {
			interplinearfwd<<<args... >>>(kernelargs);
		}
	};

	template<EInterpolationMode mode>
	struct interpolate3Dbckwd {};

	template<>
	struct interpolate3Dbckwd<NEAREST_NEIGHBOR> {
		/**
		 * Executes a deformation kernel
		 * @param kernelargs The arguments struct that is passed on to the kernel itself.
		 * @param args		 Arguments for starting the kernel. See the CUDA programming guide.
		 */
		template<typename... Args>
		static void run(const SKernelArguments3D& kernelargs, Args... args) {
			interpnearestbckwd<<<args... >>>(kernelargs);
		}
	};

	template<>
	struct interpolate3Dbckwd<LINEAR> {
		/**
		 * Executes a deformation kernel
		 * @param kernelargs The arguments struct that is passed on to the kernel itself.
		 * @param args		 Arguments for starting the kernel. See the CUDA programming guide.
		 */
		template<typename... Args>
		static void run(const SKernelArguments3D& kernelargs, Args... args) {
			interplinearbckwd<<<args... >>>(kernelargs);
		}
	};

	/**
	* Function template for starting the 3D deformation kernels, forward method.
	* This function removes the reliance on a template parameter.
	*/
	template<typename... Args>
	void doInterpolationfwd(EInterpolationMode mode, Args... args) {
		switch (mode){
			case NEAREST_NEIGHBOR:
				interpolate3Dfwd<NEAREST_NEIGHBOR>::run(args...);
				break;
			case LINEAR:
				interpolate3Dfwd<LINEAR>::run(args...);
				break;
		}
	}

	/**
	* Function template for starting the 3D deformation kernels, backward method.
	* This function removes the reliance on a template parameter.
	*/
	template<typename... Args>
	void doInterpolationbckwd(EInterpolationMode mode, Args... args) {
		switch (mode){
			case NEAREST_NEIGHBOR:
				interpolate3Dbckwd<NEAREST_NEIGHBOR>::run(args...);
				break;
			case LINEAR:
				interpolate3Dbckwd<LINEAR>::run(args...);
				break;
		}
	}

	/**
	 * Base class for the naive 3D CUDA deformation implementations
	 * These implementations assume all data fits into device memory.
	 */
	class _AstraExport DeformAlgorithm3D {
	public:
		DeformAlgorithm3D();
		~DeformAlgorithm3D();

		/**
		 * Finishes algorithm initialization
		 */
		virtual bool init();

		/**
		 * Sets the dimensions of the image that will be processed.
		 * setDimensions and optionally setBuffers must be called before invoking run.
		 */
		void setDimensions(const SDimensions3D& _dims);

		/**
		 * Sets the pointers to the buffers in device memory where the data is located.
		 * Use this method if you already have these buffers or want to manually manage them.
		 * setDimensions and optionally setBuffers must be called before invoking run.
		 */
		bool setBuffers(const cudaPitchedPtr& _D_fSource,
						const cudaPitchedPtr& _D_fDVFx,
						const cudaPitchedPtr& _D_fDVFy,
						const cudaPitchedPtr& _D_fDVFz,
						const cudaPitchedPtr& _D_fTarget);

		/**
		* Stores pointers to the current configured device memory buffers in the function arguments.
		*/
		bool getBuffers(cudaPitchedPtr& _D_fSource,
						cudaPitchedPtr& _D_fDVFx,
						cudaPitchedPtr& _D_fDVFy,
						cudaPitchedPtr& _D_fDVFz,
						cudaPitchedPtr& _D_fTarget);

		/**
		 * Runs the algorithm.
		 * This function may only be called after setDimensions and optionally setBuffers were called.
		 */
		virtual void run() = 0;

		/**
		 * Resets the algorithm initialization
		 */
		void reset();

		/**
		 * Sets the index of the GPU that will be used to perform computation.
		 * The first GPU has index 0.
		 *
		 * @param _iGPUIndex New GPU index.
		 * @see getGPUIndex
		 */
		void setGPUIndex(int _iGPUIndex) { m_iGPUIndex = _iGPUIndex; }

		/**
		 * Sets the index of the GPU that will be used to perform computation.
		 * The first GPU has index 0.
		 *
		 * @return the index of the GPU that will be used to perform computation.
		 * @see getGPUIndex
		 */
		inline int getGPUIndex() const { return m_iGPUIndex; }

		/** Signal the algorithm it should abort soon.
		 *  This is intended to be called from a different thread
		 *  while the algorithm is running. There are no guarantees
		 *  on how soon the algorithm will abort. The state of the
		 *  algorithm object will be consistent (so it is safe to delete it
		 *  normally afterwards), but the algorithm's output is undefined.
		 *
		 *  Note that specific algorithms may give guarantees on their
		 *  state after an abort. Check their documentation for details.
		 */
		void signalAbort() { m_shouldAbort = true; }

		/**
		 * Sets the interpolation mode.
		 * @see getInterpolationMode
		 * @see EInterpolationMode
		 */
		inline void setInterpolationMode(EInterpolationMode _eMode) { m_eMode = _eMode; }

		/**
		 * Returns the currently used interpolation mode.
		 * @see setInterpolationMode
		 * @see EInterpolationMode
		 */
		inline EInterpolationMode getInterpolationMode() const { return m_eMode; }

	protected:
		virtual void _allocateBuffers();
		bool _check() const;

		bool m_bInitialized;
		volatile bool m_shouldAbort;
		int m_iGPUIndex;
		SDimensions3D dims;
		EInterpolationMode m_eMode;

		cudaPitchedPtr D_fSource;
		cudaPitchedPtr D_fDVFx;
		cudaPitchedPtr D_fDVFy;
		cudaPitchedPtr D_fDVFz;
		cudaPitchedPtr D_fTarget;

	};

	/**
	 * Implementation of the 3D deformation algorithm class for the forward method.
	 */
	class _AstraExport DeformAlgorithm3DFwd : public DeformAlgorithm3D {
	public:
		// run must be called after setDimensions and setBuffers
		virtual void run();
	};

	/**
	 * Implementation of the 3D deformation algorithm class for the backward method.
	 */
	class _AstraExport DeformAlgorithm3DBckwd : public DeformAlgorithm3D {
	public:
		// run must be called after setDimensions and setBuffers
		virtual void run();
	};

	/**
	 * Utility class for creating 2D data objects based on external memory
	 */
	class CFloat32ExternalCustomMemory : public astra::CFloat32CustomMemory {
	public:
		inline CFloat32ExternalCustomMemory(float* data) {
			m_fPtr = data;
		}
		inline CFloat32ExternalCustomMemory(const CFloat32ExternalCustomMemory &other) {
			m_fPtr = other.m_fPtr;
		}
		inline virtual ~CFloat32ExternalCustomMemory() {}
	};
}

#endif
