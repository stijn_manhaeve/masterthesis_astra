/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
*/


#ifndef _CUDA_DEFORM3DGHOST_H
#define _CUDA_DEFORM3DGHOST_H

#include "deform3d.h"

#include "astra\Float32VolumeData3D.h"
#include "astra\Float32DVFData3D.h"
#include "astra\CudaDeformAlgorithm3D.h"
#include <driver_types.h>

using namespace astra;
using namespace astraCUDA;

namespace astraCUDA3d {

	/**
	 * Base class for the 3D CUDA 'Ghost Slice' deformation implementations
	 * These implementations try to minimize memory use by segmenting the image in smaller parts
	 * that can be processed individually.
	 */
	class _AstraExport GhostSliceDeformAlgorithm3D: public CCudaDeformAlgorithm3D {

	public:
		GhostSliceDeformAlgorithm3D();
		~GhostSliceDeformAlgorithm3D();

		bool initialize(const astra::Config &);
		bool initialize(CFloat32VolumeData3D* _pSource,
						CFloat32DVFData3D* _pDVF,
						CFloat32VolumeData3D* _pOutput);

		virtual void run(int _iNrIterations = 0) = 0;

		void unInit();

	protected:
		virtual void _finishInit();
		//allocate a number of slices
		//note: can't allocate more than m_iSliceCacheSize slices
		void _allocateSlices(int _iSliceCount);
		//updates the number of allocated slices if more are needed and available
		void _updateAllocation(int _iNewSliceCount);
		//deallocate memory
		void _reset();
		//move the device pointer with a certain number of slices
		void moveDevicePointer(cudaPitchedPtr& _ptr, int _sliceCount);


		CFloat32VolumeData3D* m_pSource;
		CFloat32DVFData3D* m_pDVF;
		CFloat32VolumeData3D* m_pOutput;

		size_t m_iPitch;			//The pitch in bytes.
		size_t m_iSliceSize;		//The size in bytes of each slice.
		size_t m_iPitchedSliceSize;	//The size in bytes of each slice, counting the pitch.
		size_t m_iSliceCacheSize;	//The maximum number of slices that can be stored.
		size_t m_iSlicesAllocated;	//The number of slices currently allocated.
		//This is a total, based on the maximum memory usage
		// and size of each slice

		cudaPitchedPtr m_dpfMemory;	//keep all memory into one block & use pointer arithmetic
		//cache in host memory for storing the result.
		//This cache is necessary as data can't be copied directly into 
		float* m_pfCache;
	};

	/**
	 * Extended base class for the 3D CUDA 'Ghost Slice' deformation implementations
	 * These implementations try to minimize memory use by segmenting the image in smaller parts
	 * that can be processed individually.
	 * This class limits these segments to batches of full slices only.
	 */
	class _AstraExport GlobalGhostSliceDeformAlgorithm3D : public GhostSliceDeformAlgorithm3D {
	public:
		GlobalGhostSliceDeformAlgorithm3D();
		virtual void run(int _iNrIterations = 0);
	protected:
		//get the minimum and maximum delta z
		void calculateDeltaZ();
		//sets up the different pointers to device memory.
		//must be called after calculateDeltaZ
		virtual void setupPointers() = 0;
		//copies the slices from host to device
		//param iz	The first slice index
		virtual void copySourceSlices(int iz) = 0;
		//execute the compute kernel
		//param iz	The first slice index
		virtual void executeKernel(int iz) = 0;
		//copies the output slices from device to host
		//param iz	The first slice index
		virtual void fetchResult(int iz) = 0;

		void copyDVFSlices(int iz);

		int m_iMinDz;	///minimum delta z
		int m_iMaxDz;	///maximum delta z

		//pointers to the parts of device memory
		//do NOT deallocate them manually. They are automatically deallocated with m_dpfMemory
		cudaPitchedPtr m_dpfSource;		///pointer to device memory holding the source image
		cudaPitchedPtr m_dpfTarget;		///pointer to device memory holding the target image
		cudaPitchedPtr m_dpfDVFx;		///pointer to device memory holding the x DVF data
		cudaPitchedPtr m_dpfDVFy;		///pointer to device memory holding the y DVF data
		cudaPitchedPtr m_dpfDVFz;		///pointer to device memory holding the z DVF data
		//number of slices per category and their offset relative
		//to the first source slice being processed
		size_t m_iSlicesStep;	///number of slices done per step
		size_t m_iSlicesSource;	///number of slices in device memory used for the source image
		size_t m_iSlicesTarget;	///number of slices in device memory used for the target image
		size_t m_iSlicesDVF;	///number of slices in device memory used for the DVF data
		int m_iOffsetSlices;	///offset of the first slice index of the image in device memory
		int m_iOffsetDVF;		///offset of the first slice index of the DVF data in device memory
	};

	/**
	 * Implementation of the 3D CUDA 'Ghost Slice' deformation implementation for the forward method.
	 */
	class _AstraExport GlobalGhostSliceDeformAlgorithm3DFwd : public GlobalGhostSliceDeformAlgorithm3D {
	protected:
		virtual void setupPointers();
		virtual void copySourceSlices(int iz) override;
		virtual void executeKernel(int iz) override;
		virtual void fetchResult(int iz) override;

		cudaPitchedPtr m_dpfTargetOverlapZero;	//pointer to the start of the new target slices
		cudaPitchedPtr m_dpfTargetOverlapCopy;	//pointer to the beginning of the last overlapping slices

		int m_iTargetOverlapCopy; //numer of slices to copy
		int m_iTargetOverlapZero; //number of slices to zero
	};

	/**
	 * Implementation of the 3D CUDA 'Ghost Slice' deformation implementation for the backward method.
	 */
	class _AstraExport GlobalGhostSliceDeformAlgorithm3DBckwd : public GlobalGhostSliceDeformAlgorithm3D {
	protected:
		virtual void setupPointers();
		virtual void copySourceSlices(int iz) override;
		virtual void executeKernel(int iz) override;
		virtual void fetchResult(int iz) override;

		cudaPitchedPtr m_dpfSourceOverlapEnd;	//pointer to the end of the first overlapping slices
		cudaPitchedPtr m_dpfSourceOverlapBegin;	//pointer to the beginning of the last overlapping slices

		int m_iSourceOverlapSize; //numer of slices to copy
		int m_iSourceOverlapOther; //number of slices not in overlap
	};
}

#endif