/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "../cuda/2d/deform.h"
#include "deform3d.h"
#include "util3d.h"
#include "astra/Globals.h"
#include <iostream>
#include <vector>

namespace astraCUDA3d {
	using astraCUDA::operator<<;

	std::ostream& operator<<(std::ostream& out, const SKernelArguments3D& args) {
		out << "{ " //"d_image: " << args.d_imagedata
			//<< ", d_result: " << args.d_result
			//<< ", d_d*: [" << args.d_dx << ", " << args.d_dy << ", " << args.d_dz
			<< "], whd: [" << args.width << ", " << args.height << ", " << args.depth
			<< "], start*: [" << args.startx << ", " << args.starty << ", " << args.startz
			<< "], pitch: " << args.pitch
			<< ", offsetSlices: " << args.offsetSlices
			<< ", sourceSlices: " << args.sourceSlices
			<< ", targetSlices: " << args.targetSlices
			<< "}";
		return out;
	}

	__device__ __host__ float rel2absolute(float delta, size_t size) {
		return delta * float(size) / 2.0f;
	}

	__device__ unsigned int getOffset(unsigned int posx, unsigned int posy, unsigned int posz,
									unsigned int pitch, unsigned int height) {
		return pitch * (posy + height * posz) + posx;
	}

	__device__ void putAddItem(float v, float* d, int posx, int posy, int posz,
							   unsigned int pitch, int width, int height, int depth) {
		if (posx >= 0 && posx < width &&
			posy >= 0 && posy < height &&
			posz >= 0 && posz < depth) {
			unsigned int offset = getOffset(posx, posy, posz, pitch, height);
			atomicAdd(d + offset, v);
		}
	}

	__device__ void setItem(float v, float* d, int posx, int posy, int posz,
							unsigned int pitch, int width, int height, int depth) {
		unsigned int offset = getOffset(posx, posy, posz, pitch, height);
		d[offset] = v;
	}

	__device__ float readItem(float* d, int posx, int posy, int posz,
							unsigned int pitch, int width, int height, int depth) {

		if (posx >= 0 && posx < width &&
			posy >= 0 && posy < height &&
			posz >= 0 && posz < depth) {
			unsigned int offset = getOffset(posx, posy, posz, pitch, height);
			return d[offset];
		}
		return 0.0f;
		
	}

	SKernelArguments3D::SKernelArguments3D() :d_imagedata(0),
		d_dx(0), d_dy(0), d_dz(0),
		d_result(0),
		pitch(0u),
		width(0u), height(0u), depth(0u),
		startx(0), starty(0), startz(0),
		offsetSlices(0),
		targetSlices(0u), sourceSlices(0u)
	{ }

	__global__ void interpnearestfwd(SKernelArguments3D args) {
		// get current position
		int posx = blockIdx.x * blockDim.x + threadIdx.x;
		int posy = blockIdx.y * blockDim.y + threadIdx.y;
		int posz = blockIdx.z * blockDim.z + threadIdx.z;
		if ((posx + args.startx >= args.width) ||
			(posx + args.startx < 0) ||
			(posy + args.starty >= args.height) ||
			(posy + args.starty < 0) ||
			(posz + args.startz >= args.depth) ||
			(posz + args.startz < 0)) {
			return;
		}
		// get the value and delta
		size_t offset = getOffset(posx, posy, posz, args.pitch, args.height);
		float value = args.d_imagedata[offset];
		float dx = args.d_dx[offset];
		float dy = args.d_dy[offset];
		float dz = args.d_dz[offset];

		dx = rel2absolute(dx, args.width);
		dy = rel2absolute(dy, args.height);
		dz = rel2absolute(dz, args.depth);

		posx += args.startx + int(rintf(dx));
		posy += args.starty + int(rintf(dy));
		posz += args.startz + int(rintf(dz)) + args.offsetSlices;
		putAddItem(value, args.d_result,
					posx, posy, posz,
					args.pitch, args.width, args.height, args.targetSlices);
	}

	__global__ void interplinearfwd(SKernelArguments3D args) {
		// get current position
		int posx = blockIdx.x * blockDim.x + threadIdx.x;
		int posy = blockIdx.y * blockDim.y + threadIdx.y;
		int posz = blockIdx.z * blockDim.z + threadIdx.z;

		if ((posx + args.startx >= args.width) ||
			(posx + args.startx < 0) ||
			(posy + args.starty >= args.height) ||
			(posy + args.starty < 0) ||
			(posz + args.startz >= args.depth) ||
			(posz + args.startz < 0)) {
			return;
		}
		// get the value and delta
		size_t offset = getOffset(posx, posy, posz, args.pitch, args.height);
		float value = args.d_imagedata[offset];
		float dx = args.d_dx[offset];
		float dy = args.d_dy[offset];
		float dz = args.d_dz[offset];

		dx = rel2absolute(dx, args.width);
		dy = rel2absolute(dy, args.height);
		dz = rel2absolute(dz, args.depth);

		float factualx = posx + dx;
		float factualy = posy + dy;
		float factualz = posz + dz;

		//round and calculate the fractions of the eight pixels
		//the value must be spread over
		float froundx = rintf(factualx);
		float froundy = rintf(factualy);
		float froundz = rintf(factualz);

		int ileftx, itopy, ifrontz;
		float fleftfraction, ftopfraction, ffrontfraction;

		if (froundx <= factualx) {
			ileftx = int(froundx);
			fleftfraction = 1.0f - (factualx - froundx);
		} else {
			ileftx = int(froundx) - 1;
			fleftfraction = (froundx - factualx);
		}
		if (froundy <= factualy) {
			itopy = int(froundy);
			ftopfraction = 1.0f - (factualy - froundy);
		} else {
			itopy = int(froundy) - 1;
			ftopfraction = (froundy - factualy);
		}
		if (froundz <= factualz) {
			ifrontz = int(froundz);
			ffrontfraction = 1.0f - (factualz - froundz);
		} else {
			ifrontz = int(froundz) - 1;
			ffrontfraction = (froundz - factualz);
		}
		//calculate the four values added to the four pixels
		float a = (fleftfraction          * ftopfraction * ffrontfraction) * value;
		float b = ((1.0f - fleftfraction) * ftopfraction * ffrontfraction) * value;
		float c = (fleftfraction          * (1.0f - ftopfraction) * ffrontfraction) * value;
		float d = ((1.0f - fleftfraction) * (1.0f - ftopfraction) * ffrontfraction) * value;
		float e = (fleftfraction          * ftopfraction * (1.0f - ffrontfraction)) * value;
		float f = ((1.0f - fleftfraction) * ftopfraction * (1.0f - ffrontfraction)) * value;
		float g = (fleftfraction          * (1.0f - ftopfraction) * (1.0f - ffrontfraction)) * value;
		float h = ((1.0f - fleftfraction) * (1.0f - ftopfraction) * (1.0f - ffrontfraction)) * value;

		//add these values to the output image
		ileftx += args.startx;
		itopy += args.starty;
		ifrontz += args.offsetSlices + args.startz;
		putAddItem(a, args.d_result, ileftx, itopy, ifrontz, args.pitch, args.width, args.height, args.targetSlices);
		putAddItem(b, args.d_result, ileftx + 1, itopy, ifrontz, args.pitch, args.width, args.height, args.targetSlices);
		putAddItem(c, args.d_result, ileftx, itopy + 1, ifrontz, args.pitch, args.width, args.height, args.targetSlices);
		putAddItem(d, args.d_result, ileftx + 1, itopy + 1, ifrontz, args.pitch, args.width, args.height, args.targetSlices);
		putAddItem(e, args.d_result, ileftx, itopy, ifrontz + 1, args.pitch, args.width, args.height, args.targetSlices);
		putAddItem(f, args.d_result, ileftx + 1, itopy, ifrontz + 1, args.pitch, args.width, args.height, args.targetSlices);
		putAddItem(g, args.d_result, ileftx, itopy + 1, ifrontz + 1, args.pitch, args.width, args.height, args.targetSlices);
		putAddItem(h, args.d_result, ileftx + 1, itopy + 1, ifrontz + 1, args.pitch, args.width, args.height, args.targetSlices);
	}

	__global__ void interpnearestbckwd(SKernelArguments3D args) {
		// get current position
		const int posx = blockIdx.x * blockDim.x + threadIdx.x;
		const int posy = blockIdx.y * blockDim.y + threadIdx.y;
		const int posz = blockIdx.z * blockDim.z + threadIdx.z;

		if ((posx >= args.width) ||
			(posy >= args.height) ||
			(posz >= args.targetSlices)) {
			return;
		}
		// get the position
		size_t offset = getOffset(posx, posy, posz, args.pitch, args.height);
		float dx = args.d_dx[offset];
		float dy = args.d_dy[offset];
		float dz = args.d_dz[offset];

		dx = rel2absolute(dx, args.width);
		dy = rel2absolute(dy, args.height);
		dz = rel2absolute(dz, args.depth);

		int actualx = posx + args.startx + int(rintf(dx));
		int actualy = posy + args.starty + int(rintf(dy));
		int actualz = posz + args.startz + int(rintf(dz));
		//get the value
		float val = readItem(args.d_imagedata, actualx, actualy, actualz, args.pitch, args.width, args.height, args.sourceSlices);

		setItem(val, args.d_result, posx, posy, posz, args.pitch, args.width, args.height, args.targetSlices);
}

	__device__ float lerp(float a, float b, float t) {
		return fma(t, b, fma(-t, a, a));
	}
	__device__ float lerp(float a, float b, float c, float d, float t1, float t2) {
		return lerp(lerp(a, b, t1), lerp(c, d, t1), t2);
	}
	__device__ float lerp(float a, float b, float c, float d,
						  float e, float f, float g, float h,
						  float t1, float t2, float t3) {
		return lerp(lerp(lerp(a, b, t1), lerp(c, d, t1), t2),
					lerp(lerp(e, f, t1), lerp(g, h, t1), t2),
					t3);
	}

	__global__ void interplinearbckwd(SKernelArguments3D args) {
		// get current position
		const int posx = blockIdx.x * blockDim.x + threadIdx.x;
		const int posy = blockIdx.y * blockDim.y + threadIdx.y;
		const int posz = blockIdx.z * blockDim.z + threadIdx.z;

		if (posx >= args.width || posy >= args.height || posz >= args.targetSlices) {
			return;
		}
		// get the position
		size_t offset = getOffset(posx, posy, posz, args.pitch, args.height);
		float dx = args.d_dx[offset];
		float dy = args.d_dy[offset];
		float dz = args.d_dz[offset];

		dx = posx + args.startx + rel2absolute(dx, args.width);
		dy = posy + args.starty + rel2absolute(dy, args.height);
		dz = posz + args.startz + rel2absolute(dz, args.depth);

		int lx = floorf(dx);
		int ly = floorf(dy);
		int lz = floorf(dz);
		int lxp1 = lx + 1;
		int lyp1 = ly + 1;
		int lzp1 = lz + 1;
		float tx = dx - lx;
		float ty = dy - ly;
		float tz = dz - lz;

		float a = readItem(args.d_imagedata, lx, ly, lz, args.pitch, args.width, args.height, args.sourceSlices);
		float b = readItem(args.d_imagedata, lxp1, ly, lz, args.pitch, args.width, args.height, args.sourceSlices);
		float c = readItem(args.d_imagedata, lx, lyp1, lz, args.pitch, args.width, args.height, args.sourceSlices);
		float d = readItem(args.d_imagedata, lxp1, lyp1, lz, args.pitch, args.width, args.height, args.sourceSlices);
		float e = readItem(args.d_imagedata, lx, ly, lzp1, args.pitch, args.width, args.height, args.sourceSlices);
		float f = readItem(args.d_imagedata, lxp1, ly, lzp1, args.pitch, args.width, args.height, args.sourceSlices);
		float g = readItem(args.d_imagedata, lx, lyp1, lzp1, args.pitch, args.width, args.height, args.sourceSlices);
		float h = readItem(args.d_imagedata, lxp1, lyp1, lzp1, args.pitch, args.width, args.height, args.sourceSlices);
		float val = lerp(a, b, c, d, e, f, g, h, tx, ty, tz);
		
		setItem(val, args.d_result, posx, posy, posz + args.offsetSlices, args.pitch, args.width, args.height, args.targetSlices);
	}

	DeformAlgorithm3D::DeformAlgorithm3D() {
		D_fSource.ptr = 0;
		D_fDVFx.ptr = 0;
		D_fDVFy.ptr = 0;
		D_fDVFz.ptr = 0;
		D_fTarget.ptr = 0;

		m_shouldAbort = false;
		m_bInitialized = false;
		m_iGPUIndex = -1;
		m_eMode = DEFAULT;
	}
	DeformAlgorithm3D::~DeformAlgorithm3D() {
		reset();
	}

	bool DeformAlgorithm3D::init() {
		if (m_bInitialized) return true;
		if (m_iGPUIndex < 0)
			m_iGPUIndex = 0;

		if (!_check()) _allocateBuffers();
		m_bInitialized = true;
		return true;
	}
	void DeformAlgorithm3D::setDimensions(const SDimensions3D& _dims) {
		dims = _dims;
	}
	bool DeformAlgorithm3D::setBuffers(const cudaPitchedPtr& _D_fSource,
								  const cudaPitchedPtr& _D_fDVFx,
								  const cudaPitchedPtr& _D_fDVFy,
								  const cudaPitchedPtr& _D_fDVFz,
								  const cudaPitchedPtr& _D_fTarget) {
		D_fSource = _D_fSource;
		D_fDVFx = _D_fDVFx;
		D_fDVFy = _D_fDVFy;
		D_fDVFz = _D_fDVFz;
		D_fTarget = _D_fTarget;

		return true;
	}
	bool DeformAlgorithm3D::getBuffers(cudaPitchedPtr& _D_fSource,
								  cudaPitchedPtr& _D_fDVFx,
								  cudaPitchedPtr& _D_fDVFy,
								  cudaPitchedPtr& _D_fDVFz,
								  cudaPitchedPtr& _D_fTarget) {
		_D_fSource = D_fSource;
		_D_fDVFx = D_fDVFx;
		_D_fDVFy = D_fDVFy;
		_D_fDVFz = D_fDVFz;
		_D_fTarget = D_fTarget;
		return true;
	}

	void DeformAlgorithm3D::reset() {
		cudaFree(D_fSource.ptr);
		cudaFree(D_fDVFx.ptr);
		cudaFree(D_fDVFy.ptr);
		cudaFree(D_fDVFz.ptr);
		cudaFree(D_fTarget.ptr);
		D_fSource.ptr = 0;
		D_fDVFx.ptr = 0;
		D_fDVFy.ptr = 0;
		D_fDVFz.ptr = 0;
		D_fTarget.ptr = 0;

		m_shouldAbort = false;
	}

	void DeformAlgorithm3D::_allocateBuffers() {
		D_fSource = allocateVolumeData(dims);
		D_fDVFx = allocateVolumeData(dims);
		D_fDVFy = allocateVolumeData(dims);
		D_fDVFz = allocateVolumeData(dims);
		D_fTarget = allocateVolumeData(dims);
		ASTRA_ASSERT(_check());
	}

	bool DeformAlgorithm3D::_check() const {
		if (D_fSource.ptr == 0) return false;
		if (D_fDVFx.ptr == 0) return false;
		if (D_fDVFy.ptr == 0) return false;
		if (D_fDVFz.ptr == 0) return false;
		if (D_fTarget.ptr == 0) return false;

		if (D_fSource.pitch != D_fDVFx.pitch) return false;
		if (D_fDVFx.pitch != D_fDVFy.pitch) return false;
		if (D_fDVFy.pitch != D_fDVFz.pitch) return false;
		if (D_fDVFz.pitch != D_fTarget.pitch) return false;
		return true;
	}

	void DeformAlgorithm3DFwd::run() {
		const unsigned int bldim = 16u;
		dim3 blocksize(bldim, bldim);
		dim3 gridsize((dims.iVolX + bldim - 1) / bldim, (dims.iVolY + bldim - 1) / bldim, dims.iVolZ);

		SKernelArguments3D args;
		args.d_imagedata = (float*)D_fSource.ptr;
		args.d_dx = (float*)D_fDVFx.ptr;
		args.d_dy = (float*)D_fDVFy.ptr;
		args.d_dz = (float*)D_fDVFz.ptr;
		args.d_result = (float*)D_fTarget.ptr;
		args.pitch = D_fSource.pitch / sizeof(float);
		args.width = dims.iVolX;
		args.height = dims.iVolY;
		args.depth = dims.iVolZ;
		args.targetSlices = dims.iVolZ;
		
		doInterpolationfwd(m_eMode, args, gridsize, blocksize);
		cudaTextForceKernelsCompletion();
		cudaError_t err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
	}

	void DeformAlgorithm3DBckwd::run() {
		const unsigned int bldim = 16u;
		dim3 blocksize(bldim, bldim);
		dim3 gridsize((dims.iVolX + bldim - 1) / bldim, (dims.iVolY + bldim - 1) / bldim, dims.iVolZ);

		SKernelArguments3D args;
		args.d_imagedata = (float*)D_fSource.ptr;
		args.d_dx = (float*)D_fDVFx.ptr;
		args.d_dy = (float*)D_fDVFy.ptr;
		args.d_dz = (float*)D_fDVFz.ptr;
		args.d_result = (float*)D_fTarget.ptr;
		args.pitch = D_fSource.pitch / sizeof(float);
		args.width = dims.iVolX;
		args.height = dims.iVolY;
		args.depth = dims.iVolZ;
		args.targetSlices = dims.iVolZ;
		args.sourceSlices = dims.iVolZ;

		doInterpolationbckwd(m_eMode, args, gridsize, blocksize);
		cudaTextForceKernelsCompletion();
	}
}