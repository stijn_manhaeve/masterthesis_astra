/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
*/

#include "../cuda/2d/deform.h"
#include "deform3dghost.h"
#include "dims3d.h"

#include "astra3d.h"
#include "deform3d.h"
#include "deform3dstream.h"
#include "mem3d.h"
#include "astra/AstraObjectManager.h"
#include "../2d/util.h"
#include <driver_functions.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <device_functions.h>

using namespace astraCUDA;
namespace astraCUDA3d {
	using astraCUDA::operator<<;

	GhostSliceDeformAlgorithm3D::GhostSliceDeformAlgorithm3D() {
		m_pSource = 0;
		m_pDVF = 0;
		m_pOutput = 0;

		m_iPitch = 0u;
		m_iSliceSize = 0u;
		m_iPitchedSliceSize = 0u;
		m_iSliceCacheSize = 0u;
		m_iSlicesAllocated = 0u;

		m_dpfMemory.ptr = 0;
		m_pfCache = 0;
	}
	GhostSliceDeformAlgorithm3D::~GhostSliceDeformAlgorithm3D() {

	}
	//----------------------------------------------------------------------------------------
	// GhostSliceDeformAlgorithm3D
	//----------------------------------------------------------------------------------------

	bool GhostSliceDeformAlgorithm3D::initialize(const Config& _cfg) {
		ASTRA_ASSERT(_cfg.self);
		ConfigStackCheck<CAlgorithm> CC("DeformationAlgorithm3D", this, _cfg);

		// Source Data
		XMLNode node = _cfg.self.getSingleNode("SourceDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No SourceDataId tag specified.");

		int id;
		id = node.getContentInt();
		CFloat32VolumeData3D* pSource = dynamic_cast<CFloat32VolumeData3D*>(CData3DManager::getSingleton().get(id));
		CC.markNodeParsed("SourceDataId");

		// Output Data
		node = _cfg.self.getSingleNode("OutputDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No OutputDataId tag specified.");
		id = node.getContentInt();
		CFloat32VolumeData3D* pOutput = dynamic_cast<CFloat32VolumeData3D*>(CData3DManager::getSingleton().get(id));
		CC.markNodeParsed("OutputDataId");

		// DVF Data
		node = _cfg.self.getSingleNode("DVFDataId");
		ASTRA_CONFIG_CHECK(node, "Deformation3D", "No DVFDataId tag specified.");
		id = node.getContentInt();
		CFloat32DVFData3D* pDVF = dynamic_cast<CFloat32DVFData3D*>(CDVFData3DManager::getSingleton().get(id));
		CC.markNodeParsed("DVFDataId");

		return initialize(pSource, pDVF, pOutput);
	}
	bool GhostSliceDeformAlgorithm3D::initialize(CFloat32VolumeData3D* _pSource,
												 CFloat32DVFData3D* _pDVF,
												 CFloat32VolumeData3D* _pOutput) {
		m_pSource = _pSource;
		m_pDVF = _pDVF;
		m_pOutput = _pOutput;

		ASTRA_ASSERT(m_pSource->getWidth() > 0 && "The data objects can't have a width of 0.");
		ASTRA_ASSERT(m_pSource->getHeight() > 0 && "The data objects can't have a height of 0.");
		ASTRA_ASSERT(m_pSource->getDepth() > 0 && "The data objects can't have a depth of 0.");
		ASTRA_ASSERT(m_pSource->getWidth() == m_pDVF->getWidth() && m_pSource->getWidth() == m_pOutput->getWidth()
					 && "The data objects must have the same width.");
		ASTRA_ASSERT(m_pSource->getHeight() == m_pDVF->getHeight() && m_pSource->getHeight() == m_pOutput->getHeight()
					 && "The data objects must have the same height.");
		ASTRA_ASSERT(m_pSource->getDepth() == m_pDVF->getDepth() && m_pSource->getDepth() == m_pOutput->getDepth()
					 && "The data objects must have the same depth.");

		m_width = m_pSource->getWidth();
		m_height = m_pSource->getHeight();
		m_depth = m_pSource->getDepth();
		return true;
	}
	void GhostSliceDeformAlgorithm3D::_finishInit() {
		if (getGPUIndex() < 0)
			setGPUIndex(0);

		if (m_iMaxMemory == 0) {
			m_iMaxMemory = availableGPUMemory();
		}

		m_iPitch = astraCUDA::calculatePitch(m_width * sizeof(float));
		m_iSliceSize = m_width * m_height * sizeof(float);
		m_iPitchedSliceSize = m_iPitch * m_height;
		m_iSliceCacheSize = m_iMaxMemory / m_iPitchedSliceSize;
	}

	void GhostSliceDeformAlgorithm3D::_allocateSlices(int _iSliceCount) {
		//allocate a number of slices
		//can't allocate more than m_iSliceCacheSize slices
		ASTRA_ASSERT(_iSliceCount <= m_iSliceCacheSize);
		SDimensions3D dims;
		dims.iVolX = m_width;
		dims.iVolY = m_height;
		dims.iVolZ = _iSliceCount;
		m_dpfMemory = allocateVolumeData(dims);

		if (m_dpfMemory.pitch != m_iPitch) {
			std::cout << "calculated pitch: " << m_iSliceSize << ", actual pitch: " << m_dpfMemory.pitch << '\n';
			std::cout.flush();
			cudaFree(m_dpfMemory.ptr);
			ASTRA_ASSERT(false && "pitch calulation is incorrect.");
		}

		zeroVolumeData(m_dpfMemory, dims);
		m_iSlicesAllocated += _iSliceCount;
	}

	void GhostSliceDeformAlgorithm3D::_updateAllocation(int _iNewSliceCount) {
		if (_iNewSliceCount <= m_iSlicesAllocated)
			return;
		ASTRA_ASSERT(_iNewSliceCount <= m_iSliceCacheSize);
		if (m_iSlicesAllocated){
			cudaFree(m_dpfMemory.ptr);
		}
		_allocateSlices(_iNewSliceCount);
	}

	void GhostSliceDeformAlgorithm3D::_reset() {
		if (m_dpfMemory.ptr)
			cudaFree(m_dpfMemory.ptr);
		m_dpfMemory.ptr = 0;
		if (m_pfCache)
			delete[] m_pfCache;
		m_pfCache = 0;
	}

	void GhostSliceDeformAlgorithm3D::moveDevicePointer(cudaPitchedPtr& _ptr, int _sliceCount) {
		_ptr.ptr = (char*)_ptr.ptr + _sliceCount * m_iPitchedSliceSize;
	}

	//size of shared data must be sizeof(float) * # of threads per block
	__global__ void reduceminmax3D(float* data, size_t width, size_t height, size_t depth, size_t pitch) {
		// get current position
		const size_t posx = blockIdx.x * blockDim.x + threadIdx.x;
		const size_t posy = blockIdx.y * blockDim.y + threadIdx.y;
		const size_t posz = blockIdx.z * blockDim.z + threadIdx.z;
		if (posx >= width || posy >= height || posz >= depth) {
			return;
		}
		size_t indexPitched = pitch * (posy + height * posz) + posx;
		size_t bid = gridDim.x * (blockIdx.y + gridDim.y * blockIdx.z) + blockIdx.x;
		size_t gridsize = gridDim.x * gridDim.y * gridDim.z;

		dim3 blocksize(min(size_t(blockDim.x), width - blockIdx.x * blockDim.x),
			min(size_t(blockDim.y), height - blockIdx.y * blockDim.y),
			min(size_t(blockDim.z), depth - blockIdx.z * blockDim.z));
		size_t index = blocksize.x * (threadIdx.y + blocksize.y * threadIdx.z) + threadIdx.x;

		//copy over data into the shared memory
		extern __shared__ float shared[];
		shared[index] = data[indexPitched];

		__syncthreads();

		size_t totalItems = blocksize.x * blocksize.y * blocksize.z;
		//in case the number of items is uneven, just copy the last value
		if (totalItems & 1) {
			if (index == 0) shared[totalItems] = shared[totalItems - 1];
			++totalItems;
		}
		//first minmax creates two separate arrays of min & max values
		size_t i = totalItems / 2u;
		if (index < i) {
			float fmin = min(shared[index], shared[index + i]);
			float fmax = max(shared[index], shared[index + i]);
			shared[index] = fmin;
			shared[index + i] = fmax;
		}
		__syncthreads();
		//now, reduce these two until only two values remain
		size_t oldi = i;
		i = oldi / 2u;
		while (i){
			if (oldi & 1u) {
				//need to reduce an uneven number of items.
				//items stored in ranges [0, ..., oldi - 1][oldi, ..., 2*oldi - 1]
				//must be reduced to [0, ..., oldi / 2][oldi / 2 + 1, ..., oldi]
				//in total, oldi / 2 + 1 items are kept
				if (index <= i) {
					float v1 = shared[index];
					float v2 = shared[index + i];
					float v3 = shared[oldi + index];
					float v4 = shared[(oldi + i + index) * (index != i)];
					float fmin = min(v1, v2);
					float fmax = max(v3, v4);

					__syncthreads();
					shared[index] = fmin;
					shared[index + i + 1] = fmax;
					i += 1;
				}
			}
			else {
				//need to reduce an even number of items.
				//min value in index, index + i
				//max value in index + 2i, index + 3i
				if (index < i) {
					float v1 = shared[index];
					float v2 = shared[index + i];
					float v3 = shared[index + 2u * i];
					float v4 = shared[index + 3u * i];
					float fmin = min(v1, v2);
					float fmax = max(v3, v4);

					__syncthreads();
					shared[index] = fmin;
					shared[index + i] = fmax;
				}
			}
			oldi = i;
			i /= 2;
			__syncthreads();
		}
		//save the value again in global memory
		if (index == 0) {
			data[bid] = shared[0];
			data[bid + gridsize] = shared[1];
		}
	}

	//size of shared data must be sizeof(float) * 2 * numitems
	__global__ void reduceminmax1D(float* data, size_t numItems) {
		const size_t index = threadIdx.x;
		const size_t bid = blockIdx.x;
		const unsigned int blocksize = min(blockDim.x, unsigned int(numItems) - unsigned int(bid)*blockDim.x);
		const size_t gridsize = gridDim.x;
		const size_t globalIndex = bid * blocksize + index;
		if (globalIndex >= numItems) {
			return;
		}

		extern __shared__ float shared[];
		shared[index] = data[globalIndex];
		shared[index + blocksize] = data[globalIndex + numItems];
		__syncthreads();

		size_t oldi = blocksize;
		size_t i = oldi / 2u;
		while (i){
			if (oldi & 1u) {
				//need to reduce an uneven number of items.
				//items stored in ranges [0, ..., oldi - 1][oldi, ..., 2*oldi - 1]
				//must be reduced to [0, ..., oldi / 2][oldi / 2 + 1, ..., oldi]
				//in total, oldi / 2 + 1 items are kept
				if (index <= i) {
					float v1 = shared[index];
					float v2 = shared[index + i];
					float v3 = shared[oldi + index];
					float v4 = shared[(oldi + i + index) * (index != i)];
					float fmin = min(v1, v2);
					float fmax = max(v3, v4);

					__syncthreads();
					shared[index] = fmin;
					shared[index + i + 1] = fmax;
					i += 1;
				}
			} else {
				//need to reduce an even number of items.
				//min value in index, index + i
				//max value in index + 2i, index + 3i
				if (index < i) {
					float v1 = shared[index];
					float v2 = shared[index + i];
					float v3 = shared[index + 2u * i];
					float v4 = shared[index + 3u * i];
					float fmin = min(v1, v2);
					float fmax = max(v3, v4);

					__syncthreads();
					shared[index] = fmin;
					shared[index + i] = fmax;
				}
			}
			oldi = i;
			i /= 2;
			__syncthreads();
		}
		//save the value again in global memory
		if (index == 0) {
			data[bid] = shared[0];
			data[bid + gridsize] = shared[1];
		}
	}

	void reduceminmax(const cudaPitchedPtr& data, size_t width, size_t height, size_t depth) {
		//first, reduce the 3D data to a 1D array
		const unsigned int bs = 16u;
		dim3 blocksize(bs, bs);
		dim3 gridsize((width + bs - 1) / bs, (height + bs - 1) / bs, depth);
		size_t sharedsize = sizeof(float) * blocksize.x * blocksize.y * blocksize.z;

		cudaError_t err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
		reduceminmax3D<<<gridsize, blocksize, sharedsize>>>((float*)data.ptr, width, height, depth, data.pitch / sizeof(float));
		err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
		//keep reducing until only one value is left
		size_t itemsLeft = gridsize.x * gridsize.y * gridsize.z;
		while (itemsLeft > 1) {
			blocksize = dim3(min(size_t(bs * bs), itemsLeft));
			gridsize = dim3((itemsLeft + bs * bs - 1) / (bs * bs));
			//std::cout << "\nblocksize: " << blocksize << "\ngridsize: " << gridsize << '\n';
			//std::cout.flush();
			cudaTextForceKernelsCompletion();
			err = cudaGetLastError();
			ASTRA_CUDA_ASSERT(err);
			reduceminmax1D<<<gridsize, blocksize, blocksize.x * sizeof(float) * 2 >>>((float*)data.ptr, itemsLeft);
			itemsLeft = gridsize.x;
		}
		cudaTextForceKernelsCompletion();
		err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
	}

	//----------------------------------------------------------------------------------------
	// GlobalGhostSliceDeformAlgorithm3D
	//----------------------------------------------------------------------------------------
	GlobalGhostSliceDeformAlgorithm3D::GlobalGhostSliceDeformAlgorithm3D() {
		m_iMinDz = 0;
		m_iMaxDz = 0;

		m_iSlicesStep = 0;
		m_iSlicesSource = 0;
		m_iSlicesTarget = 0;
		m_iSlicesDVF = 0;

		m_iOffsetSlices = 0;
		m_iOffsetDVF = 0;
		m_bShouldAbort = false;
	}

	void writehexfloat(std::ostream& out, float val) {
		static char* digits = "0123456789ABCDEF";
		const unsigned char* chp = reinterpret_cast<const unsigned char*>(&val);
		for (size_t i = 0; i < sizeof(float); ++i) {
			unsigned char ch = chp[i];
			out << digits[int(ch >> 4u)] << digits[int(ch & unsigned char(15))];
		}
	}
	void GlobalGhostSliceDeformAlgorithm3D::calculateDeltaZ() {
		//send m_iSliceCacheSize slices to GPU
		//reduce these slices, finding the minimum and maximum
		//rince & repeat
		//data is processed in batches, keeping a global minimum and maximum.
		float fmin = std::numeric_limits<float>::max();
		float fmax = std::numeric_limits<float>::lowest();
		if (m_pDVF->hasMinZ() && m_pDVF->hasMaxZ()) {
			fmin = m_pDVF->getMinZ();
			fmax = m_pDVF->getMaxZ();
		} else {
			float* workptr = 0;
			//slices themselves are kept whole.
			SDimensions3D dims;
			dims.iVolX = m_width;
			dims.iVolY = m_height;
			//the size of each batch in slices. Every time, the entire cache is filled
			const size_t gpuBatchSize = min(m_iSliceCacheSize, m_depth);
			//not all data might be immediately available in RAM.
			size_t cpuBatchSize = m_pDVF->getConsecutiveDepth();
			size_t cpuBatchLeft = 0u;
			//loop until everything has been processed
			size_t slicesToDo = m_depth;
			while (slicesToDo) {
				size_t slicesToWrite = min(gpuBatchSize, slicesToDo);
				size_t slicesCounter = 0;
				cudaPitchedPtr pTarget = m_dpfMemory;
				//write into memory 
				if (cpuBatchLeft) {
					//there's still data available in memory
					dims.iVolZ = min(min(cpuBatchLeft, gpuBatchSize), slicesToDo);
					//copy over this data
					copyVolumeToDevice(workptr, pTarget, dims);
					//update pointers
					workptr += dims.iVolZ * m_iSliceSize / sizeof(float);
					moveDevicePointer(pTarget, dims.iVolZ);
					//update stuff to do
					cpuBatchLeft -= dims.iVolZ;
					slicesToWrite -= dims.iVolZ;
					slicesCounter += dims.iVolZ;
				}
				if (slicesToWrite) {
					//get the pointer to the new slices
					workptr = m_pDVF->getDispZ(m_depth - slicesToDo + slicesCounter);
					//copy the data
					dims.iVolZ = min(slicesToWrite, cpuBatchSize);
					copyVolumeToDevice(workptr, pTarget, dims);
					//update stuff to do
					slicesCounter += dims.iVolZ;
					cpuBatchLeft = cpuBatchSize - dims.iVolZ;
				}
				//reduce fase
				reduceminmax(m_dpfMemory, m_width, m_height, slicesCounter);
				//update result fase
				float fminmax[2];
				//get the result
				cudaMemcpy(fminmax, m_dpfMemory.ptr, 2 * sizeof(float), cudaMemcpyDeviceToHost);
				//update values
				fmin = min(fminmax[0], fmin);
				fmax = max(fminmax[1], fmax);
				slicesToDo -= slicesCounter;
			}
			m_pDVF->setMinZ(fmin);
			m_pDVF->setMaxZ(fmax);
		}
		m_iMinDz = rel2absolute(fmin, m_depth);
		m_iMaxDz = rel2absolute(fmax, m_depth);
	}

	void GlobalGhostSliceDeformAlgorithm3D::run(int) {
		cudaError_t err = cudaThreadSynchronize();
		ASTRA_CUDA_ASSERT(err);
		err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);

		_finishInit();
		_allocateSlices(min(5 * m_depth + 2, m_iSliceCacheSize));
		calculateDeltaZ();
		setupPointers();

		for (int i = 0; i < m_depth; i += m_iSlicesStep) {
			if (m_bShouldAbort) break;

			copySourceSlices(i);
			copyDVFSlices(i);
			err = cudaGetLastError();
			ASTRA_CUDA_ASSERT(err);

			executeKernel(i);
			err = cudaGetLastError();
			ASTRA_CUDA_ASSERT(err);

			fetchResult(i);
			err = cudaGetLastError();
			ASTRA_CUDA_ASSERT(err);
		}
		_reset();
		err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
	}


	void GlobalGhostSliceDeformAlgorithm3D::copyDVFSlices(int iz){
		SDimensions3D dims;
		dims.iVolX = m_width;
		dims.iVolY = m_height;
		//calculate the offsets
		int start = iz;
		//calculate how many slices to copy
		size_t copySize = min(m_depth - iz, m_iSlicesDVF);
		//copy until enough data is sent to the device
		size_t maxReadSize = m_pDVF->getConsecutiveDepth();
		cudaPitchedPtr pTargetx = m_dpfDVFx;
		cudaPitchedPtr pTargety = m_dpfDVFy;
		cudaPitchedPtr pTargetz = m_dpfDVFz;
		while (copySize) {
			const float* pfDatax = m_pDVF->getDispXConst(start);
			const float* pfDatay = m_pDVF->getDispYConst(start);
			const float* pfDataz = m_pDVF->getDispZConst(start);
			dims.iVolZ = min(copySize, maxReadSize - (start % maxReadSize));
			copyVolumeToDevice(pfDatax, pTargetx, dims);
			copyVolumeToDevice(pfDatay, pTargety, dims);
			copyVolumeToDevice(pfDataz, pTargetz, dims);
			moveDevicePointer(pTargetx, dims.iVolZ);
			moveDevicePointer(pTargety, dims.iVolZ);
			moveDevicePointer(pTargetz, dims.iVolZ);
			copySize -= dims.iVolZ;
			start += dims.iVolZ;
		}
	}

	//----------------------------------------------------------------------------------------
	// GlobalGhostSliceDeformAlgorithm3DFwd
	//----------------------------------------------------------------------------------------
	void GlobalGhostSliceDeformAlgorithm3DFwd::setupPointers() {
		//forward write: 4i input slices to (max - min) + i + 2 output slices
		//               = 4i + (max - min) + i + 2
		//               = 5i + max - min + 3
		//               < m_iSliceCacheSize
		// => 5i = m_iSliceCacheSize + min - max - 2
		// => i = (m_iSliceCacheSize + min - max - 2) / 5
		int i = (int(m_iSliceCacheSize) + m_iMinDz - m_iMaxDz - 2) / 5;
		i = min(i, (int)m_depth);

		ASTRA_ASSERT(i > 0);

		m_iSlicesStep = i;
		m_iSlicesSource = i;
		m_iSlicesDVF = i;
		m_iSlicesTarget = i + m_iMaxDz - m_iMinDz + 2;


		m_iOffsetSlices = -m_iMinDz + 1;
		m_iOffsetDVF = 0;

		ASTRA_ASSERT(i > 0);
		m_dpfSource = m_dpfMemory;
		m_dpfDVFx = m_dpfSource;
		moveDevicePointer(m_dpfDVFx, i);
		m_dpfDVFy = m_dpfDVFx;
		moveDevicePointer(m_dpfDVFy, i);
		m_dpfDVFz = m_dpfDVFy;
		moveDevicePointer(m_dpfDVFz, i);
		m_dpfTarget = m_dpfDVFz;
		moveDevicePointer(m_dpfTarget, i);


		m_iTargetOverlapCopy = m_iMaxDz - m_iMinDz + 2;
		m_iTargetOverlapZero = m_iSlicesTarget - m_iTargetOverlapCopy;
		m_dpfTargetOverlapCopy = m_dpfTarget;
		moveDevicePointer(m_dpfTargetOverlapCopy, i);
		m_dpfTargetOverlapZero = m_dpfTarget;
		moveDevicePointer(m_dpfTargetOverlapZero, m_iTargetOverlapCopy);
	}

	void GlobalGhostSliceDeformAlgorithm3DFwd::copySourceSlices(int iz) {
		//slices themselves are kept whole.
		SDimensions3D dims;
		dims.iVolX = m_width;
		dims.iVolY = m_height;
		// Copy Source
		//calculate the offsets
		int start = iz;
		//calculate how many slices to copy
		size_t copySize = min(m_depth - iz, m_iSlicesSource);
		//copy until enough data is sent to the device
		size_t maxReadSize = m_pSource->getConsecutiveDepth();
		cudaPitchedPtr pTarget = m_dpfSource;
		if (start < 0) {
			moveDevicePointer(pTarget, -start);
			start = 0;
			copySize -= start;
		}
		while (copySize) {
			const float* pfData = m_pSource->getDataZ(start);
			dims.iVolZ = min(copySize, maxReadSize - (start % maxReadSize));
			copyVolumeToDevice(pfData, pTarget, dims);
			moveDevicePointer(pTarget, dims.iVolZ);
			copySize -= dims.iVolZ;
			start += dims.iVolZ;
		}

		cudaError_t err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
		// Copy overlapping target slices & wipe rest
		dims.iVolZ = m_iTargetOverlapCopy;
		duplicateVolumeData(m_dpfTarget, m_dpfTargetOverlapCopy, dims);
		dims.iVolZ = m_iTargetOverlapZero;
		zeroVolumeData(m_dpfTargetOverlapZero, dims);
	}

	void GlobalGhostSliceDeformAlgorithm3DFwd::executeKernel(int iz) {
		const unsigned int bldim = 16u;
		dim3 blocksize(bldim, bldim);
		dim3 gridsize((m_width + bldim - 1) / bldim, (m_height + bldim - 1) / bldim, min(m_depth - iz, m_iSlicesSource));

		SKernelArguments3D args;
		args.d_imagedata = (float*)m_dpfSource.ptr;
		args.d_dx = (float*)m_dpfDVFx.ptr;
		args.d_dy = (float*)m_dpfDVFy.ptr;
		args.d_dz = (float*)m_dpfDVFz.ptr;
		args.d_result = (float*)m_dpfTarget.ptr;
		args.pitch = m_dpfSource.pitch / sizeof(float);
		args.width = m_width;
		args.height = m_height;
		args.depth = m_depth;
		args.offsetSlices = m_iOffsetSlices;
		args.targetSlices = m_iSlicesTarget;
		args.sourceSlices = m_iSlicesSource;

		doInterpolationfwd(m_eMode, args, gridsize, blocksize);
		cudaTextForceKernelsCompletion();
		cudaError_t err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
	}

	void GlobalGhostSliceDeformAlgorithm3DFwd::fetchResult(int iz) {
		if (!m_pfCache) {
			size_t numElements = m_iSlicesTarget * m_width * m_height;
			m_pfCache = new float[numElements];
		}
		SDimensions3D dims;
		dims.iVolX = m_width;
		dims.iVolY = m_height;
		dims.iVolZ = m_iSlicesTarget;
		cudaError_t err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
		copyVolumeFromDevice(m_pfCache, m_dpfTarget, dims);

		CVolumeGeometry2D geom(m_width, m_height);
		CFloat32ExternalCustomMemory mem(m_pfCache);
		int todo = m_iSlicesTarget - m_iTargetOverlapCopy;
		int offs = m_iMinDz - 1;
		iz += offs;

		for (int i = 0; i < todo; ++i) {
			int index = iz + i;
			if (index >= 0 && index < m_depth) {
				CFloat32VolumeData2D data2D(&geom, new CFloat32ExternalCustomMemory(mem));
				m_pOutput->returnSliceZ(index, &data2D);
			}
			mem.m_fPtr += m_width * m_height;
		}
	}


	//----------------------------------------------------------------------------------------
	// GlobalGhostSliceDeformAlgorithm3DBckwd
	//----------------------------------------------------------------------------------------
	void GlobalGhostSliceDeformAlgorithm3DBckwd::setupPointers() {
		//backward read: 4i output slices from (max - min) + i + 2 input slices
		//               = 4i + (max - min) + i + 2
		//               = 5i + max - min + 2
		//               < m_iSliceCacheSize
		// => 5i = m_iSliceCacheSize + min - max - 2
		// => i = (m_iSliceCacheSize + min - max - 2) / 5
		int i = (int(m_iSliceCacheSize) + m_iMinDz - m_iMaxDz - 2) / 5;
		i = min(i, (int)m_depth);
		ASTRA_ASSERT(i > 0);
		_updateAllocation(5 * i + m_iMaxDz - m_iMinDz + 2);

		m_iSlicesStep = i;
		m_iSlicesSource = i + m_iMaxDz - m_iMinDz + 2;
		m_iSlicesDVF = i;
		m_iSlicesTarget = i;


		m_iOffsetSlices = -m_iMinDz + 1;
		m_iOffsetDVF = 0;

		m_dpfSource = m_dpfMemory;
		m_dpfDVFx = m_dpfSource;
		moveDevicePointer(m_dpfDVFx, m_iSlicesSource);
		m_dpfDVFy = m_dpfDVFx;
		moveDevicePointer(m_dpfDVFy, m_iSlicesDVF);
		m_dpfDVFz = m_dpfDVFy;
		moveDevicePointer(m_dpfDVFz, m_iSlicesDVF);
		m_dpfTarget = m_dpfDVFz;
		moveDevicePointer(m_dpfTarget, m_iSlicesDVF);


		m_iSourceOverlapSize = m_iMaxDz - m_iMinDz + 2;
		m_iSourceOverlapOther = m_iSlicesSource - m_iSourceOverlapSize;
		m_dpfSourceOverlapEnd = m_dpfSource;
		moveDevicePointer(m_dpfSourceOverlapEnd, m_iSourceOverlapSize);
		m_dpfSourceOverlapBegin = m_dpfSource;
		moveDevicePointer(m_dpfSourceOverlapBegin, m_iSourceOverlapOther);

		SDimensions3D dims;
		dims.iVolX = m_width;
		dims.iVolY = m_height;
		dims.iVolZ = m_iSliceCacheSize;
		zeroVolumeData(m_dpfMemory, dims);
	}

	void GlobalGhostSliceDeformAlgorithm3DBckwd::copySourceSlices(int iz) {
		//slices themselves are kept whole.
		SDimensions3D dims;
		dims.iVolX = m_width;
		dims.iVolY = m_height;
		// Copy Source
		{
			cudaPitchedPtr pDestination = m_dpfSource;
			size_t slicesToCopy = m_iSlicesSource;
			size_t maxReadSize = m_pSource->getConsecutiveDepth();
			//offset current slice index.
			//note, if offset positive, start counting from negative numbers
			int slicesStart = iz - m_iOffsetSlices;
			if (slicesStart < 0) {
				//zero first slices
				//slices start counting < 0
				dims.iVolZ = min(size_t(-slicesStart), slicesToCopy);
				zeroVolumeData(pDestination, dims);
				//move destination pointer
				moveDevicePointer(pDestination, dims.iVolZ);
				slicesToCopy -= dims.iVolZ;
				slicesStart = 0;
			}
			//copy remaining slices
			while (slicesToCopy && slicesStart < m_depth) {
				dims.iVolZ = min(slicesToCopy, maxReadSize - (slicesStart % maxReadSize));
				//dims.iVolZ = min(dims.iVolZ, unsigned int(m_depth - slicesStart));
				const float* pfData = m_pSource->getDataZ(slicesStart);
				copyVolumeToDevice(pfData, pDestination, dims);
				moveDevicePointer(pDestination, dims.iVolZ);
				slicesToCopy -= dims.iVolZ;
				slicesStart += dims.iVolZ;
			}
			//wipe remaining slices if they exist
			if (slicesToCopy) {
				dims.iVolZ = slicesToCopy;
				zeroVolumeData(pDestination, dims);
			}
		}
		// wipe output
		dims.iVolZ = m_iSlicesTarget;
		zeroVolumeData(m_dpfTarget, dims);
	}

	void GlobalGhostSliceDeformAlgorithm3DBckwd::executeKernel(int iz) {
		const unsigned int bldim = 16u;
		dim3 blocksize(bldim, bldim);
		dim3 gridsize((m_width + bldim - 1) / bldim, (m_height + bldim - 1) / bldim, min(m_depth - iz, m_iSlicesStep));

		SKernelArguments3D args;
		args.d_imagedata = (float*)m_dpfSource.ptr;
		args.d_dx = (float*)m_dpfDVFx.ptr;
		args.d_dy = (float*)m_dpfDVFy.ptr;
		args.d_dz = (float*)m_dpfDVFz.ptr;
		args.d_result = (float*)m_dpfTarget.ptr;
		args.pitch = m_dpfSource.pitch / sizeof(float);
		args.width = m_width;
		args.height = m_height;
		args.depth = m_depth;
		args.startz = m_iOffsetSlices;
		args.targetSlices = m_iSlicesTarget;
		args.sourceSlices = m_iSlicesSource;

		doInterpolationbckwd(m_eMode, args, gridsize, blocksize);
		cudaTextForceKernelsCompletion();
		cudaError_t err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
	}

	void GlobalGhostSliceDeformAlgorithm3DBckwd::fetchResult(int iz) {
		if (!m_pfCache) {
			size_t numElements = m_iSlicesTarget * m_width * m_height;
			m_pfCache = new float[numElements];
		}
		SDimensions3D dims;
		dims.iVolX = m_width;
		dims.iVolY = m_height;
		dims.iVolZ = m_iSlicesTarget;
		cudaError_t err = cudaGetLastError();
		ASTRA_CUDA_ASSERT(err);
		copyVolumeFromDevice(m_pfCache, m_dpfTarget, dims);

		CVolumeGeometry2D geom(m_width, m_height);
		CFloat32ExternalCustomMemory mem(m_pfCache);
		int todo = m_iSlicesTarget;

		for (int i = 0; i < todo; ++i, ++iz) {
			if (iz >= 0 && iz < m_depth) {
				CFloat32VolumeData2D data2D(&geom, new CFloat32ExternalCustomMemory(mem));
				m_pOutput->returnSliceZ(iz, &data2D);
			}
			mem.m_fPtr += m_width * m_height;
		}
	}
}