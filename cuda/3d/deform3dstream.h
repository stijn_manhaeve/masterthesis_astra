/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
*/

#ifndef _CUDA_DEFORM3DSTREAM_H
#define _CUDA_DEFORM3DSTREAM_H

#include "astra\Float32VolumeData3D.h"
#include "astra\Float32VolumeData3DMemory.h"
#include "astra\Float32DVFData3D.h"

#include "deform3d.h"

namespace astraCUDA3d {

	/**
	 * Base class for the 3D CUDA deformation implementations using CUDA streams
	 * These implementations try to overlap data copies with computation.
	 * Data copies are done in smaller batches of a few slices.
	 */
	class StreamDeformAlgorithm3D {
	public:
		/**
		 * Constructor for the StreamDeformAlgorithm3D class
		 * @param _iStreamsize The number of slices copied per channel per atomic task.
		 */
		StreamDeformAlgorithm3D(int _iStreamsize = 64);
		virtual ~StreamDeformAlgorithm3D();

		/**
		* Runs the algorithm.
		* This function may only be called after setDimensions and optionally setBuffers were called.
		*/
		void run();

		/**
		 * Sets the number of slices copied per channel per atomic task.
		 */
		inline void setStreamsize(int _iStreamsize) { m_iStreamsize = _iStreamsize; }

		/**
		 * Returns the number of slices copied per channel per atomic task.
		 */
		inline int getStreamsize() const { return m_iStreamsize; }

		/** Signal the algorithm it should abort soon.
		*  This is intended to be called from a different thread
		*  while the algorithm is running. There are no guarantees
		*  on how soon the algorithm will abort. The state of the
		*  algorithm object will be consistent (so it is safe to delete it
		*  normally afterwards), but the algorithm's output is undefined.
		*
		*  Note that specific algorithms may give guarantees on their
		*  state after an abort. Check their documentation for details.
		*/
		void signalAbort() { m_shouldAbort = true; }

		/**
		* Sets the interpolation mode.
		* @see getInterpolationMode
		* @see EInterpolationMode
		*/
		inline void setInterpolationMode(EInterpolationMode _eMode) { m_eMode = _eMode; }

		/**
		* Returns the currently used interpolation mode.
		* @see setInterpolationMode
		* @see EInterpolationMode
		*/
		inline EInterpolationMode getInterpolationMode() const { return m_eMode; }
	protected:
		int m_iWidth;
		int m_iHeight;
		int m_iDepth;
		int m_iStreamsize;
		bool m_isInitialized;
		volatile bool m_shouldAbort;
		EInterpolationMode m_eMode;

		std::vector<cudaStream_t> m_streams;

		void createStreams();
		void destroyStreams();

		virtual void allocateBuffers() = 0;
		virtual void destroyBuffers() = 0;
		virtual void runImplementation() = 0;
	};

	/**
	 * Implementation of the 3D deformation algorithm with CUDA streams class for the forward method.
	 */
	class StreamDeformAlgorithm3DFwd : public StreamDeformAlgorithm3D {
	public:
		/**
		* Constructor for the StreamDeformAlgorithm3DFwd class
		* @param _iStreamsize The number of slices copied per channel per atomic task.
		*/
		StreamDeformAlgorithm3DFwd(int _iStreamsize = 64);
		virtual ~StreamDeformAlgorithm3DFwd();

		/**
		 * Initializes the deformation algorithm with the appropriate data objects.
		 * @param _pSource The input image.
		 * @param _pDVF The DVF data
		 * @param _pOutput The output is stored in this image. It is the only one that can't be a streamed data object.
		 */
		void initialize(astra::CFloat32VolumeData3D* _pSource, astra::CFloat32DVFData3D* _pDVF, astra::CFloat32VolumeData3DMemory* _pOutput);

	protected:
		cudaPitchedPtr D_fSource[2];	//each stream has its own memory
		cudaPitchedPtr D_fDvfx[2];
		cudaPitchedPtr D_fDvfy[2];
		cudaPitchedPtr D_fDvfz[2];
		cudaPitchedPtr D_fTarget;

		astra::CFloat32VolumeData3D* m_pSource;
		astra::CFloat32DVFData3D* m_pDVF;
		astra::CFloat32VolumeData3DMemory* m_pOutput;

		virtual void allocateBuffers();
		virtual void destroyBuffers();
		virtual void runImplementation();

	};

	/**
	 * Implementation of the 3D deformation algorithm with CUDA streams class for the backward method.
	 */
	class StreamDeformAlgorithm3DBckwd : public StreamDeformAlgorithm3D {
	public:
		/**
		* Constructor for the StreamDeformAlgorithm3DBckwd class
		* @param _iStreamsize The number of slices copied per channel per atomic task.
		*/
		StreamDeformAlgorithm3DBckwd(int _iStreamsize = 64);
		virtual ~StreamDeformAlgorithm3DBckwd();

		/**
		* Initializes the deformation algorithm with the appropriate data objects.
		* @param _pSource The input image. It is the only one that can't be a streamed data object.
		* @param _pDVF The DVF data
		* @param _pOutput The output is stored in this image.
		*/
		void initialize(astra::CFloat32VolumeData3DMemory* _pSource, astra::CFloat32DVFData3D* _pDVF, astra::CFloat32VolumeData3D* _pOutput);

	protected:
		cudaPitchedPtr D_fSource;	//each stream has its own memory
		cudaPitchedPtr D_fDvfx[2];
		cudaPitchedPtr D_fDvfy[2];
		cudaPitchedPtr D_fDvfz[2];
		cudaPitchedPtr D_fTarget[2];

		float* m_pfResultBuffer;

		astra::CFloat32VolumeData3DMemory* m_pSource;
		astra::CFloat32DVFData3D* m_pDVF;
		astra::CFloat32VolumeData3D* m_pOutput;

		virtual void allocateBuffers();
		virtual void destroyBuffers();
		virtual void runImplementation();
	};

}

#endif