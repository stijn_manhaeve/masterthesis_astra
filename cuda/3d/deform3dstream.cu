/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#include "../cuda/2d/deform.h"
#include "astra3d.h"
#include "deform3dstream.h"
#include "util3d.h"
#include <iostream>
#include <vector>

namespace astraCUDA3d {
	using astraCUDA::operator<<;

	StreamDeformAlgorithm3D::StreamDeformAlgorithm3D(int _iStreamsize) :
		m_iWidth(0),
		m_iHeight(0),
		m_iDepth(0),
		m_iStreamsize(_iStreamsize),
		m_isInitialized(false),
		m_shouldAbort(false),
		m_eMode(EInterpolationMode::DEFAULT)
	{}

	StreamDeformAlgorithm3D::~StreamDeformAlgorithm3D() {
		destroyStreams();
	}

	void StreamDeformAlgorithm3D::createStreams() {
		for (int i = 0; i < 2; ++i) {
			auto iter = m_streams.insert(m_streams.end(), cudaStream_t());
			cudaError_t error = cudaStreamCreateWithFlags(iter._Ptr, cudaStreamNonBlocking);
			ASTRA_CUDA_ASSERT(error);
		}
	}

	void StreamDeformAlgorithm3D::destroyStreams() {
		for (cudaStream_t& str : m_streams) {
			cudaError_t error = cudaStreamDestroy(str);
			ASTRA_CUDA_ASSERT(error);
		}
		m_streams.clear();
	}

	void StreamDeformAlgorithm3D::run() {
		ASTRA_ASSERT(m_isInitialized && "The stream algorithm must be initialized.");

		allocateBuffers();
		bool streamsEmpty = m_streams.empty();
		if (streamsEmpty) {
			createStreams();
		}
		runImplementation();
		destroyBuffers();

		if (streamsEmpty) {
			destroyStreams();
		}
	}

	//-----------------
	StreamDeformAlgorithm3DFwd::StreamDeformAlgorithm3DFwd(int _iStreamsize) : StreamDeformAlgorithm3D(_iStreamsize) {
		D_fSource[0].ptr = NULL;
		D_fSource[1].ptr = NULL;
		D_fDvfx[0].ptr = NULL;
		D_fDvfx[1].ptr = NULL;
		D_fDvfy[0].ptr = NULL;
		D_fDvfy[1].ptr = NULL;
		D_fDvfz[0].ptr = NULL;
		D_fDvfz[1].ptr = NULL;
		D_fTarget.ptr = NULL;
	}
	StreamDeformAlgorithm3DFwd::~StreamDeformAlgorithm3DFwd() {
		destroyBuffers();
	}
	void StreamDeformAlgorithm3DFwd::initialize(astra::CFloat32VolumeData3D* _pSource, astra::CFloat32DVFData3D* _pDVF, astra::CFloat32VolumeData3DMemory* _pOutput) {
		m_pSource = _pSource;
		m_pDVF = _pDVF;
		m_pOutput = _pOutput;
		ASTRA_ASSERT(m_pSource && m_pSource->isInitialized() && "The source image must be initialized.");
		ASTRA_ASSERT(m_pDVF && m_pDVF->isInitialized() && "The DVF data must be initialized.");
		ASTRA_ASSERT(m_pOutput && m_pOutput->isInitialized() && "The output image must be initialized.");
		m_iWidth = m_pSource->getWidth();
		m_iHeight = m_pSource->getHeight();
		m_iDepth = m_pSource->getDepth();
		m_isInitialized = true;
	}

	void StreamDeformAlgorithm3DFwd::allocateBuffers() {
		SDimensions3D dims;
		dims.iVolX = m_iWidth;
		dims.iVolY = m_iHeight;
		dims.iVolZ = m_iStreamsize;
		D_fSource[0] = allocateVolumeData(dims);
		D_fDvfx[0] = allocateVolumeData(dims);
		D_fDvfy[0] = allocateVolumeData(dims);
		D_fDvfz[0] = allocateVolumeData(dims);
		D_fSource[1] = allocateVolumeData(dims);
		D_fDvfx[1] = allocateVolumeData(dims);
		D_fDvfy[1] = allocateVolumeData(dims);
		D_fDvfz[1] = allocateVolumeData(dims);
		dims.iVolZ = m_iDepth;
		D_fTarget = allocateVolumeData(dims);
	}
	void StreamDeformAlgorithm3DFwd::destroyBuffers() {
		if (D_fSource[0].ptr != NULL) { cudaFree(D_fSource[0].ptr); D_fSource[0].ptr = NULL; }
		if (D_fSource[1].ptr != NULL) { cudaFree(D_fSource[1].ptr); D_fSource[1].ptr = NULL; }
		if (D_fDvfx[0].ptr != NULL) { cudaFree(D_fDvfx[0].ptr); D_fDvfx[0].ptr = NULL; }
		if (D_fDvfx[1].ptr != NULL) { cudaFree(D_fDvfx[1].ptr); D_fDvfx[1].ptr = NULL; }
		if (D_fDvfy[0].ptr != NULL) { cudaFree(D_fDvfy[0].ptr); D_fDvfy[0].ptr = NULL; }
		if (D_fDvfy[1].ptr != NULL) { cudaFree(D_fDvfy[1].ptr); D_fDvfy[1].ptr = NULL; }
		if (D_fDvfz[0].ptr != NULL) { cudaFree(D_fDvfz[0].ptr); D_fDvfz[0].ptr = NULL; }
		if (D_fDvfz[1].ptr != NULL) { cudaFree(D_fDvfz[1].ptr); D_fDvfz[1].ptr = NULL; }
		if (D_fTarget.ptr != NULL) { cudaFree(D_fTarget.ptr); D_fTarget.ptr = NULL; }
	}

	void StreamDeformAlgorithm3DFwd::runImplementation() {
		m_shouldAbort = false;
		const int depthParts = m_pSource->getConsecutiveDepth();
		const int streamsize = min(depthParts, m_iStreamsize);	//size of each stream in depth
		const int nstreamblocks = (depthParts + streamsize - 1) / streamsize; //number of blocks that can be processed, a stream can take one block per time
		const unsigned int bldim = 16u;
		dim3 blocksize(bldim, bldim);
		dim3 gridsize((m_iWidth + bldim - 1) / bldim, (m_iHeight + bldim - 1) / bldim, m_iDepth);

		SDimensions3D dims;
		dims.iVolX = m_iWidth;
		dims.iVolY = m_iHeight;
		dims.iVolZ = streamsize;

		SKernelArguments3D args;
		args.d_result = (float*)D_fTarget.ptr;
		args.pitch = D_fSource[0].pitch / sizeof(float);
		args.width = m_iWidth;
		args.height = m_iHeight;
		args.depth = m_iDepth;
		args.startx = 0;
		args.starty = 0;
		args.targetSlices = m_iDepth;

		//start the streams for each block of consecutive data
		for (args.startz = 0; args.startz < m_iDepth;) {
			if (m_shouldAbort) {
				m_shouldAbort = false;
				break;
			}
			//give the stream something to do
			int startslice = args.startz;
			for (int i = 0; i < nstreamblocks; ++i) {
				const int index = i & 1;
				args.d_imagedata = (float*)D_fSource[index].ptr;
				args.d_dx = (float*)D_fDvfx[index].ptr;
				args.d_dy = (float*)D_fDvfy[index].ptr;
				args.d_dz = (float*)D_fDvfz[index].ptr;
				dims.iVolZ = min(min(streamsize, m_iDepth - args.startz), depthParts - (args.startz - startslice));
				if (dims.iVolZ == 0) break;
				gridsize.z = dims.iVolZ;
				args.sourceSlices = dims.iVolZ;

				//copy data
				const float* H_source = m_pSource->getDataZ(args.startz);
				copyVolumeToDeviceAsync(H_source, D_fSource[index], dims, 0u, m_streams[index]);
				const float* H_dvfx = m_pDVF->getDispXConst(args.startz);
				copyVolumeToDeviceAsync(H_dvfx, D_fDvfx[index], dims, 0u, m_streams[index]);
				const float* H_dvfy = m_pDVF->getDispYConst(args.startz);
				copyVolumeToDeviceAsync(H_dvfy, D_fDvfy[index], dims, 0u, m_streams[index]);
				const float* H_dvfz = m_pDVF->getDispZConst(args.startz);
				copyVolumeToDeviceAsync(H_dvfz, D_fDvfz[index], dims, 0u, m_streams[index]);
				//calculate
				doInterpolationfwd(m_eMode, args, gridsize, blocksize, 0, m_streams[index]);
				args.startz += dims.iVolZ;
			}
			//wait until stream is done
			for (cudaStream_t& str : m_streams) {
				cudaError_t error = cudaStreamSynchronize(str);
				ASTRA_CUDA_ASSERT(error);
			}
		}
		cudaError_t err = cudaThreadSynchronize();
		ASTRA_CUDA_ASSERT(err);

		//copy back the result
		dims.iVolZ = m_iDepth;
		copyVolumeFromDevice(m_pOutput->getData(), D_fTarget, dims);
	}

	//-----------------
	StreamDeformAlgorithm3DBckwd::StreamDeformAlgorithm3DBckwd(int _iStreamsize) : StreamDeformAlgorithm3D(_iStreamsize) {
		D_fSource.ptr = NULL;
		D_fDvfx[0].ptr = NULL;
		D_fDvfx[1].ptr = NULL;
		D_fDvfy[0].ptr = NULL;
		D_fDvfy[1].ptr = NULL;
		D_fDvfz[0].ptr = NULL;
		D_fDvfz[1].ptr = NULL;
		D_fTarget[0].ptr = NULL;
		D_fTarget[1].ptr = NULL;
	}
	StreamDeformAlgorithm3DBckwd::~StreamDeformAlgorithm3DBckwd() {
		destroyBuffers();
	}
	void StreamDeformAlgorithm3DBckwd::initialize(astra::CFloat32VolumeData3DMemory* _pSource, astra::CFloat32DVFData3D* _pDVF, astra::CFloat32VolumeData3D* _pOutput) {
		m_pSource = _pSource;
		m_pDVF = _pDVF;
		m_pOutput = _pOutput;
		ASTRA_ASSERT(m_pSource && m_pSource->isInitialized() && "The source image must be initialized.");
		ASTRA_ASSERT(m_pDVF && m_pDVF->isInitialized() && "The DVF data must be initialized.");
		ASTRA_ASSERT(m_pOutput && m_pOutput->isInitialized() && "The output image must be initialized.");
		m_iWidth = m_pSource->getWidth();
		m_iHeight = m_pSource->getHeight();
		m_iDepth = m_pSource->getDepth();
		m_isInitialized = true;
	}

	void StreamDeformAlgorithm3DBckwd::allocateBuffers() {
		SDimensions3D dims;
		dims.iVolX = m_iWidth;
		dims.iVolY = m_iHeight;
		dims.iVolZ = m_iStreamsize;
		D_fDvfx[0] = allocateVolumeData(dims);
		D_fDvfy[0] = allocateVolumeData(dims);
		D_fDvfz[0] = allocateVolumeData(dims);
		D_fDvfx[1] = allocateVolumeData(dims);
		D_fDvfy[1] = allocateVolumeData(dims);
		D_fDvfz[1] = allocateVolumeData(dims);
		D_fTarget[0] = allocateVolumeData(dims);
		D_fTarget[1] = allocateVolumeData(dims);
		dims.iVolZ = m_iDepth;
		D_fSource = allocateVolumeData(dims);

		m_pfResultBuffer = new float[m_iWidth * m_iHeight * m_pOutput->getConsecutiveDepth()];
	}
	void StreamDeformAlgorithm3DBckwd::destroyBuffers() {
		if (D_fSource.ptr != NULL) { cudaFree(D_fSource.ptr); D_fSource.ptr = NULL; }
		if (D_fDvfx[0].ptr != NULL) { cudaFree(D_fDvfx[0].ptr); D_fDvfx[0].ptr = NULL; }
		if (D_fDvfx[1].ptr != NULL) { cudaFree(D_fDvfx[1].ptr); D_fDvfx[1].ptr = NULL; }
		if (D_fDvfy[0].ptr != NULL) { cudaFree(D_fDvfy[0].ptr); D_fDvfy[0].ptr = NULL; }
		if (D_fDvfy[1].ptr != NULL) { cudaFree(D_fDvfy[1].ptr); D_fDvfy[1].ptr = NULL; }
		if (D_fDvfz[0].ptr != NULL) { cudaFree(D_fDvfz[0].ptr); D_fDvfz[0].ptr = NULL; }
		if (D_fDvfz[1].ptr != NULL) { cudaFree(D_fDvfz[1].ptr); D_fDvfz[1].ptr = NULL; }
		if (D_fTarget[0].ptr != NULL) { cudaFree(D_fTarget[0].ptr); D_fTarget[0].ptr = NULL; }
		if (D_fTarget[1].ptr != NULL) { cudaFree(D_fTarget[1].ptr); D_fTarget[1].ptr = NULL; }
		if (m_pfResultBuffer != NULL) { delete[] m_pfResultBuffer; m_pfResultBuffer = NULL; }
	}

	void StreamDeformAlgorithm3DBckwd::runImplementation() {
		m_shouldAbort = false;
		const int depthParts = m_pDVF->getConsecutiveDepth();
		const int streamsize = min(depthParts, m_iStreamsize);	//size of each stream in depth
		const int nstreamblocks = (depthParts + streamsize - 1) / streamsize; //number of blocks that can be processed, a stream can take one block per time
		const unsigned int bldim = 16u;
		dim3 blocksize(bldim, bldim);
		dim3 gridsize((m_iWidth + bldim - 1) / bldim, (m_iHeight + bldim - 1) / bldim, m_iDepth);

		astra::CVolumeGeometry2D geom(m_iWidth, m_iHeight);
		CFloat32ExternalCustomMemory mem(m_pfResultBuffer);


		SKernelArguments3D args;
		args.d_imagedata = (float*)D_fSource.ptr;
		args.pitch = D_fSource.pitch / sizeof(float);
		args.width = m_iWidth;
		args.height = m_iHeight;
		args.depth = m_iDepth;
		args.startx = 0;
		args.starty = 0;
		args.sourceSlices = m_iDepth;

		//copy source image
		SDimensions3D dims;
		dims.iVolX = m_iWidth;
		dims.iVolY = m_iHeight;
		dims.iVolZ = m_iDepth;
		const float* H_source = m_pSource->getDataConst();
		copyVolumeToDevice(H_source, D_fSource, dims, 0u);

		//start the streams for each block of consecutive data
		for (args.startz = 0; args.startz < m_iDepth;) {
			if (m_shouldAbort) {
				m_shouldAbort = false;
				break;
			}
			//give the stream something to do
			float* pfTarget = m_pfResultBuffer;
			int startslice = args.startz;
			for (int i = 0; i < nstreamblocks; ++i) {
				const int index = i & 1;
				args.d_dx = (float*)D_fDvfx[index].ptr;
				args.d_dy = (float*)D_fDvfy[index].ptr;
				args.d_dz = (float*)D_fDvfz[index].ptr;
				args.d_result = (float*)D_fTarget[index].ptr;
				dims.iVolZ = min(min(streamsize, m_iDepth - args.startz), depthParts - (args.startz - startslice));
				if (dims.iVolZ == 0) break;
				gridsize.z = dims.iVolZ;
				args.targetSlices = dims.iVolZ;
				args.offsetSlices = args.startz - startslice;

				//copy data
				const float* H_dvfx = m_pDVF->getDispXConst(args.startz);
				copyVolumeToDeviceAsync(H_dvfx, D_fDvfx[index], dims, 0u, m_streams[index]);
				const float* H_dvfy = m_pDVF->getDispYConst(args.startz);
				copyVolumeToDeviceAsync(H_dvfy, D_fDvfy[index], dims, 0u, m_streams[index]);
				const float* H_dvfz = m_pDVF->getDispZConst(args.startz);
				copyVolumeToDeviceAsync(H_dvfz, D_fDvfz[index], dims, 0u, m_streams[index]);
				//calculate
				SKernelArguments3D* args2 = new SKernelArguments3D(args);
				doInterpolationbckwd(m_eMode, *args2, gridsize, blocksize, 0, m_streams[index]);
				delete args2;
				//fetch result
				copyVolumeFromDeviceAsync(pfTarget, D_fTarget[index], dims, 0u, m_streams[index]);
				pfTarget += dims.iVolX * dims.iVolY * dims.iVolZ;
				args.startz += dims.iVolZ;
			}
			//wait until stream is done
			for (cudaStream_t& str : m_streams) {
				cudaError_t error = cudaStreamSynchronize(str);
				ASTRA_CUDA_ASSERT(error);
			}
			//store result from buffer in output file
			int endslice = min(m_iDepth, startslice + depthParts);
			for (int index = startslice; index < endslice; ++index) {
				astra::CFloat32VolumeData2D data2D(&geom, new CFloat32ExternalCustomMemory(mem));
				m_pOutput->returnSliceZ(index, &data2D);
				mem.m_fPtr += m_iWidth * m_iHeight;
			}
			mem.m_fPtr = m_pfResultBuffer;
		}
	}
}