/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#ifndef _INC_ASTRA_FLOAT32DVFDATA2D
#define _INC_ASTRA_FLOAT32DVFDATA2D

#include "Globals.h"
#include "Float32Data.h"

#include <sstream>

namespace astra {

	/**
	* This class represents a two-dimensional block of floating point
	* displacement vector data.
	* The displacement vector data per axis (x and y) are stored in
	* contiguous blocks in column-major order. There is no guarantee that
	* the block of data for the y-part of the displacement vectors is stored
	* contiguously to the block of data for the x-part.
	* It contains member functions for accessing this data.
	* The data blocks are "owned" by the class, meaning that the class is
	* responsible for deallocation of the memory involved.
	* 
	*/
	class _AstraExport CFloat32DVFData2D : public CFloat32Data {
	protected:

		int m_iWidth;			///< width of the data (x)
		int m_iHeight;			///< height of the data (y)
		int m_iSize;			///< total size of the data

		/** Default constructor. Sets all numeric member variables to 0 and all pointer member variables to NULL.
		*
		* If an object is constructed using this default constructor, it must always be followed by a call
		* to one of the initialize() methods before the object can be used. Any use before calling init() is not allowed,
		* except calling the member function isInitialized().
		*
		*/
		CFloat32DVFData2D();
	public:

		/** Destructor. Free allocated memory
		*/
		virtual ~CFloat32DVFData2D();

		/** Get the number of dimensions of this object.
		*
		* @return number of dimensions
		*/
		int getDimensionCount() const;

		/** Get the width of the data block.
		*
		* @return width of the data block
		*/
		int getWidth() const;

		/** Get the height of the data block.
		*
		* @return height of the data block
		*/
		int getHeight() const;

		/** Get the total size (width*height) of the data block.
		*
		* @return size of the data block
		*/
		int getSize() const;

		/** get a description of the class
		*
		* @return description string
		*/
		std::string description() const;

		/** Get a pointer to the x displacement data starting from column y.
		*/
		virtual float32* getDispX(size_t y) = 0;
		virtual const float32* getDispXConst(size_t y) const = 0;

		/** Get a pointer to the y displacement data starting from column y.
		*/
		virtual float32* getDispY(size_t y) = 0;
		virtual const float32* getDispYConst(size_t y) const = 0;
	};

	inline int CFloat32DVFData2D::getDimensionCount() const {
		return 2;
	}


	//----------------------------------------------------------------------------------------
	// Get the width of the data block.
	inline int CFloat32DVFData2D::getWidth() const {
		ASTRA_ASSERT(m_bInitialized);
		return m_iWidth;
	}

	//----------------------------------------------------------------------------------------
	// Get the height of the data block.
	inline int CFloat32DVFData2D::getHeight() const {
		ASTRA_ASSERT(m_bInitialized);
		return m_iHeight;
	}

	//----------------------------------------------------------------------------------------
	// Get the total size (width*height) of the data block.
	inline int CFloat32DVFData2D::getSize() const {
		ASTRA_ASSERT(m_bInitialized);
		return m_iSize;
	}

	//----------------------------------------------------------------------------------------
	// Get the total size (width*height*depth) of the data block.
	inline std::string CFloat32DVFData2D::description() const {
		std::stringstream res;
		res << m_iWidth << "x" << m_iHeight << " DVF data \t";
		return res.str();
	}
} // end namespace

#endif