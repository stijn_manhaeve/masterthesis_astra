/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#ifndef _INC_ASTRA_CUDADEFORMALGORITHM
#define _INC_ASTRA_CUDADEFORMALGORITHM

#include "Globals.h"
#include "Interpolation.h"
#include "Config.h"
#include "Algorithm.h"
#include "Float32DVFData3DMemory.h"
#include "Float32VolumeData3DMemory.h"
#include "../cuda/3d/deform3d.h"

#ifdef ASTRA_CUDA

namespace astra {

	class _AstraExport CCudaDeformAlgorithm3D : public CAlgorithm {
	public:
		/**
		 * Sets the index of the GPU that will be used to perform computation.
		 * The first GPU has index 0.
		 *
		 * @param _iGPUIndex New GPU index.
		 * @see getGPUIndex
		 */
		void setGPUIndex(int _iGPUIndex);

		/**
		 * Sets the index of the GPU that will be used to perform computation.
		 * The first GPU has index 0.
		 *
		 * @return the index of the GPU that will be used to perform computation.
		 * @see getGPUIndex
		 */
		int getGPUIndex() const;

		/**
		 * Sets the maximum memory use of the algorithm on the GPU.
		 * A value of 0 indicates no limit, allowing the algorithm to take up all available memory.
		 * This is the default value.
		 * @note Not every implementation supports this feature
		 *
		 * @param _iMaxMemory The new maximum memory limit
		 * @see getMaxMemory
		 */
		void setMaxMemory(std::size_t _iMaxMemory);

		/**
		 * Returns the maximum GPU memory use of the algorithm.
		 * A value of 0 indicates no limit, allowing the algorithm to take up all available memory.
		 * @see setMaxMemory
		 */
		std::size_t getMaxMemory() const;

		/**
		 * Sets the interpolation mode.
		 * @see getInterpolationMode
		 * @see EInterpolationMode
		 */
		void setInterpolationMode(EInterpolationMode _eMode);

		/**
		 * Returns the currently used interpolation mode.
		 * @see setInterpolationMode
		 * @see EInterpolationMode
		 */
		EInterpolationMode getInterpolationMode() const;
	protected:
		virtual void _finishInit();	//checks initialization & finalizes variables to their actual values

		EInterpolationMode m_eMode;	/// The interpolation technique used
		std::size_t m_iMaxMemory;	/// The maximum amount of (global) memory to use on that GPU in bytes

		size_t m_width;		//image width
		size_t m_height;	//image height
		size_t m_depth;		//image depth
	private:
		int m_iGPUIndex;			/// The index of the GPU we'll use
	};

	/**
	 * Warpper class around the CUDA implementation of the 3D deformation implementation.
	 * These implementations assume all data fits into device memory.
	 */
	class _AstraExport CCudaBasicDeformAlgorithm3D : public CCudaDeformAlgorithm3D {
	public:
		/** Initialize the algorithm with a config object.
		 *
		 * @param _cfg Configuration Object
		 * @return initialization successful?
		 */
		virtual bool initialize(const Config& _cfg);

		/**
		 * Initializes the deformation algorithm with the appropriate data objects.
		 * @param _pSource The input image.
		 * @param _pDVF The DVF data
		 * @param _pOutput The output is stored in this image.
		 */
		bool initialize(CFloat32VolumeData3DMemory* _pSource,
						CFloat32DVFData3DMemory* _pDVF,
						CFloat32VolumeData3DMemory* _pOutput);

		/**
		 * Runs the algorithm.
		 */
		virtual void run(int _iNrIterations = 0) = 0;

	protected:
		CFloat32VolumeData3DMemory* m_pSource;
		CFloat32DVFData3DMemory* m_pDVF;
		CFloat32VolumeData3DMemory* m_pOutput;
	};

	/**
	 * Implementation of the 3D CUDA deformation wrapper class for the forward method.
	 */
	class _AstraExport CCudaBasicDeformAlgorithm3Dfwd : public CCudaBasicDeformAlgorithm3D {
	public:
		void run(int _iNrIterations = 0);
	};

	/**
	 * Implementation of the 3D CUDA deformation wrapper class for the backward method.
	 */
	class _AstraExport CCudaBasicDeformAlgorithm3Dbckwd : public CCudaBasicDeformAlgorithm3D {
	public:
		void run(int _iNrIterations = 0);
	};

	/**
	 * Warpper class around the CUDA implementation of the 3D deformation implementation, forward method using CUDA streams.
	 * These implementations try to overlap data copies with computation.
	 * Data copies are done in smaller batches of a few slices.
	 */
	class _AstraExport CCudaStreamDeformAlgorithm3Dfwd : public CCudaDeformAlgorithm3D {
	public:
		CCudaStreamDeformAlgorithm3Dfwd();

		/** Initialize the algorithm with a config object.
		 *
		 * @param _cfg Configuration Object
		 * @return initialization successful?
		 */
		virtual bool initialize(const Config& _cfg);

		/**
		 * Initializes the deformation algorithm with the appropriate data objects.
		 * @param _pSource The input image.
		 * @param _pDVF The DVF data
		 * @param _pOutput The output is stored in this image. It is the only one that can't be a streamed data object.
		 */
		bool initialize(CFloat32VolumeData3D* _pSource,
						CFloat32DVFData3D* _pDVF,
						CFloat32VolumeData3DMemory* _pOutput);

		/**
		 * Sets the number of slices copied per channel per atomic task.
		 */
		inline int getStreamSize() const { return m_iStreamSize; }

		/**
		 * Returns the number of slices copied per channel per atomic task.
		 */
		inline void setStreamSize(int _iStreamSize) { m_iStreamSize = _iStreamSize;  }

		/**
		 * Runs the algorithm.
		 */
		void run(int _iNrIterations = 0);

	protected:
		CFloat32VolumeData3D* m_pSource;
		CFloat32DVFData3D* m_pDVF;
		CFloat32VolumeData3DMemory* m_pOutput;
		int m_iStreamSize;
	};

	/**
	 * Warpper class around the CUDA implementation of the 3D deformation implementation, backward method using CUDA streams.
	 * These implementations try to overlap data copies with computation.
	 * Data copies are done in smaller batches of a few slices.
	 */
	class _AstraExport CCudaStreamDeformAlgorithm3Dbckwd : public CCudaDeformAlgorithm3D {
	public:
		/** Initialize the algorithm with a config object.
		 *
		 * @param _cfg Configuration Object
		 * @return initialization successful?
		 */
		virtual bool initialize(const Config& _cfg);
		/**
		 * Initializes the deformation algorithm with the appropriate data objects.
		 * @param _pSource The input image. It is the only one that can't be a streamed data object.
		 * @param _pDVF The DVF data
		 * @param _pOutput The output is stored in this image.
		 */
		bool initialize(CFloat32VolumeData3DMemory* _pSource,
						CFloat32DVFData3D* _pDVF,
						CFloat32VolumeData3D* _pOutput);

		/**
		 * Sets the number of slices copied per channel per atomic task.
		 */
		inline int getStreamSize() const { return m_iStreamSize; }

		/**
		 * Returns the number of slices copied per channel per atomic task.
		 */
		inline void setStreamSize(int _iStreamSize) { m_iStreamSize = _iStreamSize; }

		/**
		 * Runs the algorithm.
		 */
		void run(int _iNrIterations = 0);

	protected:
		CFloat32VolumeData3DMemory* m_pSource;
		CFloat32DVFData3D* m_pDVF;
		CFloat32VolumeData3D* m_pOutput;
		int m_iStreamSize;
	};

} // end namespace

#include "../cuda/3d/deform3dghost.h"

#endif // ASTRA_CUDA
#endif
