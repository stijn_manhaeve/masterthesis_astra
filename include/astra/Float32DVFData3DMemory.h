/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#ifndef _INC_ASTRA_FLOAT32DVFDATA3DMEMORY
#define _INC_ASTRA_FLOAT32DVFDATA3DMEMORY

#include "Globals.h"
#include "Float32DVFData3D.h"

namespace astra {

	/**
	* This class represents a three-dimensional block of float32ing point data.
	* It contains member functions for accessing this data and for performing
	* elementary computations on the data.
	* The data block is "owned" by the class, meaning that the class is
	* responsible for deallocation of the memory involved.
	*/
	class _AstraExport CFloat32DVFData3DMemory : public CFloat32DVFData3D {
	protected:
		/** Pointer to the data block, represented as a 1-dimensional array.
		* Note that the data memory is "owned" by this class, meaning that the
		* class is responsible for deallocation of the memory involved.
		* To access element (ix, iy, iz) internally, use
		* m_pfData?[(iz * m_height + iy) * m_iWidth + ix]
		*/
		float32* m_pfDatax;
		float32* m_pfDatay;
		float32* m_pfDataz;

		/** Array of float32 pointers, each pointing to a single horizontal
		* line in the m_pfData memory block.
		* To access element (ix, iy) internally, use m_ppfData2D?[iy][ix]
		*/
		float32** m_ppfData2Dx;
		float32** m_ppfData2Dy;
		float32** m_ppfData2Dz;

		/** Allocate memory for m_pfData and m_ppfData2D arrays.
		*
		* The allocated block consists of m_iSize float32s. The block is
		* not cleared after allocation and its contents is undefined.
		* This function may NOT be called if memory has already been allocated.
		*/
		void _allocateData();

		/** Free memory for m_pfData and m_ppfData2D arrays.
		*
		* This function may ONLY be called if the memory for both blocks has been
		* allocated before.
		*/
		void _freeData();

		/** Clear all member variables, setting all numeric variables to 0 and all pointers to NULL.
		*/
		void _clear();

		/** Un-initialize the object, bringing it back in the uninitialized state.
		*/
		void _unInit();

		/** Initialization. Initializes an instance of the CFloat32DVFData3DMemory class,
		* without filling the data block.
		* Can only be called by derived classes.
		*
		* Initializes an instance of the CFloat32DVFData3DMemory class. Memory is allocated for the
		* data block. The allocated memory is not cleared and its contents after
		* construction is undefined. Initialization may be followed by a call to
		* copyData() to fill the memory block. If the object has been initialized before, the
		* object is reinitialized and memory is freed and reallocated if necessary.
		* This function does not set m_bInitialized to true if everything is ok.
		*
		* @param _iWidth width of the 2D data (x-axis), must be > 0
		* @param _iHeight height of the 2D data (y-axis), must be > 0
		* @param _iDepth depth of the 2D data (z-axis), must be > 0
		* @return initialization of the base class successfull
		*/
		bool _initialize(int _iWidth, int _iHeight, int _iDepth);

		/** Initialization. Initializes an instance of the CFloat32DVFData3DMemory class
		* with initialization of the data block.
		* Can only be called by derived classes.
		*
		* Initializes an instance of the CFloat32Data2D class. Memory
		* is allocated for the data block and the contents of the memory pointed to by
		* _pfData is copied into the allocated memory. If the object has been initialized before, the
		* object is reinitialized and memory is freed and reallocated if necessary.
		* This function does not set m_bInitialized to true if everything is ok.
		*
		* @param _iWidth width of the 2D data (x-axis), must be > 0
		* @param _iHeight height of the 2D data (y-axis), must be > 0
		* @param _iDepth depth of the 2D data (z-axis), must be > 0
		* @param _pfDatax pointer to a one-dimensional float32 data block
		* @param _pfDatay pointer to a one-dimensional float32 data block
		* @param _pfDataz pointer to a one-dimensional float32 data block
		*/
		bool _initialize(int _iWidth, int _iHeight, int _iDepth, const float32 *_pfDatax, const float32 *_pfDatay, const float32 *_pfDataz);

		/** Initialization. Initializes an instance of the CFloat32Data2D class with initialization of the data
		* block with a scalar value. Can only be called by derived classes.
		*
		* Initializes an instance of the CFloat32Data2D class. Memory
		* is allocated for the data block and the contents of the memory pointed to by
		* _pfData is copied into the allocated memory. If the object has been initialized before, the
		* object is reinitialized and memory is freed and reallocated if necessary.
		* This function does not set m_bInitialized to true if everything is ok.
		*
		* @param _iWidth width of the 2D data (x-axis), must be > 0
		* @param _iHeight height of the 2D data (y-axis), must be > 0
		* @param _iDepth depth of the 2D data (z-axis), must be > 0
		* @param _fScalar scalar value to put at each index
		*/
		bool _initialize(int _iWidth, int _iHeight, int _iDepth, float32 _fScalar);

		/** Initialization. Initializes an instance of the CFloat32Data2D class with initialization of the data
		* block with a scalar value. Can only be called by derived classes.
		*
		* Initializes an instance of the CFloat32Data2D class. Memory
		* is allocated for the data block and the contents of the memory pointed to by
		* _pfData is copied into the allocated memory. If the object has been initialized before, the
		* object is reinitialized and memory is freed and reallocated if necessary.
		* This function does not set m_bInitialized to true if everything is ok.
		*
		* @param _iWidth width of the 2D data (x-axis), must be > 0
		* @param _iHeight height of the 2D data (y-axis), must be > 0
		* @param _iDepth depth of the 2D data (z-axis), must be > 0
		* @param _fScalarx scalar value to put at each index for the x values
		* @param _fScalary scalar value to put at each index for the y values
		* @param _fScalarz scalar value to put at each index for the z values
		*/
		bool _initialize(int _iWidth, int _iHeight, int _iDepth, float32 _fScalarx, float32 _fScalary, float32 _fScalarz);

	public:
		/** Default constructor. Sets all numeric member variables to 0 and all pointer member variables to NULL.
		*
		* If an object is constructed using this default constructor, it must always be followed by a call
		* to one of the initialize() methods before the object can be used. Any use before calling init() is not allowed,
		* except calling the member function isInitialized().
		*
		*/
		CFloat32DVFData3DMemory();
		CFloat32DVFData3DMemory(int _iWidth, int _iHeight, int _iDepth);
		CFloat32DVFData3DMemory(int _iWidth, int _iHeight, int _iDepth, const float32 *_pfDatax, const float32 *_pfDatay, const float32 *_pfDataz);

		/** Destructor. Free allocated memory
		*/
		virtual ~CFloat32DVFData3DMemory();

		/** Copy the data block pointed to by _pfData to the data block pointed to by m_pfData.
		* The pointer _pfData must point to a block of m_iSize float32s.
		*
		* @param _pfData source data block
		*/
		void copyData(const float32* _pfDatax, const float32* _pfDatay, const float32 *_pfDataz);

		/** Set each element of the data to a specified scalar values.
		*
		* @param _fScalarx scalar value
		* @param _fScalary scalar value
		*/
		void setData(float32 _fScalarx, float32 _fScalary, float32 _fScalarz);

		/** Set each element of the data to a specified scalar value.
		*
		* @param _fScalar scalar value
		*/
		void setData(float32 _fScalar);

		/** Set all data to zero
		*/
		void clearData();
		/** Get a pointer to the data block, represented as a 1-dimensional
		* array of float32 values. The data memory is still "owned" by the
		* CFloat32Data2D instance; this memory may NEVER be freed by the
		* caller of this function. If changes are made to this data, the
		* function updateStatistics() should be called after completion of
		* all changes.
		*
		* @return pointer to the 1-dimensional 32-bit floating point data block
		*/
		float32* getDatax();
		float32* getDatay();
		float32* getDataz();

		/** Get a const pointer to the data block, represented as a 1-dimensional
		* array of float32 values. The data memory is still "owned" by the
		* CFloat32Data2D instance; this memory may NEVER be freed by the
		* caller of this function. If changes are made to this data, the
		* function updateStatistics() should be called after completion of
		* all changes.
		*
		* @return pointer to the 1-dimensional 32-bit floating point data block
		*/
		const float32* getDataxConst() const;
		const float32* getDatayConst() const;
		const float32* getDatazConst() const;

		/** Get a float32** to the data block, represented as a 2-dimensional array of float32 values.
		*
		* After the call p = getData2D(), use p[iy][ix] to access element (ix, iy).
		* The data memory and pointer array are still "owned" by the CFloat32Data2D
		* instance; this memory may NEVER be freed by the caller of this function.
		* If changes are made to this data, the function updateStatistics()
		* should be called after completion of all changes.
		*
		* @return pointer to the 2-dimensional 32-bit floating point data block
		*/
		float32** getDatax2D();
		float32** getDatay2D();
		float32** getDataz2D();

		/** Get a const float32** to the data block, represented as a 2-dimensional array of float32 values.
		*
		* After the call p = getData2D(), use p[iy][ix] to access element (ix, iy).
		* The data memory and pointer array are still "owned" by the CFloat32Data2D
		* instance; this memory may NEVER be freed by the caller of this function.
		* If changes are made to this data, the function updateStatistics()
		* should be called after completion of all changes.
		*
		* @return pointer to the 2-dimensional 32-bit floating point data block
		*/
		const float32** getDatax2DConst() const;
		const float32** getDatay2DConst() const;
		const float32** getDataz2DConst() const;

		/** Get a pointer to the x displacement data starting from slice iz.
		*/
		float32* getDispX(size_t iz);
		const float32* getDispXConst(size_t iz) const;

		/** Get a pointer to the y displacement data starting from slice iz.
		*/
		float32* getDispY(size_t iz);
		const float32* getDispYConst(size_t iz) const;

		/** Get a pointer to the y displacement data starting from slice iz.
		*/
		float32* getDispZ(size_t iz);
		const float32* getDispZConst(size_t iz) const;
	};


	//----------------------------------------------------------------------------------------
	// Get a pointer to the data block, represented as a 1-dimensional array of float32 values.
	inline float32* CFloat32DVFData3DMemory::getDatax() {
		ASTRA_ASSERT(m_bInitialized);
		return m_pfDatax;
	}
	inline float32* CFloat32DVFData3DMemory::getDatay() {
		ASTRA_ASSERT(m_bInitialized);
		return m_pfDatay;
	}
	inline float32* CFloat32DVFData3DMemory::getDataz() {
		ASTRA_ASSERT(m_bInitialized);
		return m_pfDataz;
	}

	//----------------------------------------------------------------------------------------
	// Get a const pointer to the data block, represented as a 1-dimensional array of float32 values.
	inline const float32* CFloat32DVFData3DMemory::getDataxConst() const {
		ASTRA_ASSERT(m_bInitialized);
		return (const float32*)m_pfDatax;
	}
	inline const float32* CFloat32DVFData3DMemory::getDatayConst() const {
		ASTRA_ASSERT(m_bInitialized);
		return (const float32*)m_pfDatay;
	}
	inline const float32* CFloat32DVFData3DMemory::getDatazConst() const {
		ASTRA_ASSERT(m_bInitialized);
		return (const float32*)m_pfDataz;
	}

	//----------------------------------------------------------------------------------------
	// Get a float32** to the data block, represented as a 2-dimensional array of float32 values.
	inline float32** CFloat32DVFData3DMemory::getDatax2D() {
		ASTRA_ASSERT(m_bInitialized);
		return m_ppfData2Dx;
	}
	inline float32** CFloat32DVFData3DMemory::getDatay2D() {
		ASTRA_ASSERT(m_bInitialized);
		return m_ppfData2Dy;
	}
	inline float32** CFloat32DVFData3DMemory::getDataz2D() {
		ASTRA_ASSERT(m_bInitialized);
		return m_ppfData2Dz;
	}

	//----------------------------------------------------------------------------------------
	// Get a const float32** to the data block, represented as a 2-dimensional array of float32 values.
	inline const float32** CFloat32DVFData3DMemory::getDatax2DConst() const {
		ASTRA_ASSERT(m_bInitialized);
		return (const float32**)m_ppfData2Dx;
	}
	inline const float32** CFloat32DVFData3DMemory::getDatay2DConst() const {
		ASTRA_ASSERT(m_bInitialized);
		return (const float32**)m_ppfData2Dy;
	}
	inline const float32** CFloat32DVFData3DMemory::getDataz2DConst() const {
		ASTRA_ASSERT(m_bInitialized);
		return (const float32**)m_ppfData2Dz;
	}

	//----------------------------------------------------------------------------------------
	// Get a pointer to the x displacement data starting from slice iz.
	inline float32* CFloat32DVFData3DMemory::getDispX(size_t iz) {
		ASTRA_ASSERT(m_bInitialized);
		return m_ppfData2Dx[iz];
	}
	inline const float32* CFloat32DVFData3DMemory::getDispXConst(size_t iz) const {
		ASTRA_ASSERT(m_bInitialized);
		return (const float32*)m_ppfData2Dx[iz];
	}
	//----------------------------------------------------------------------------------------
	// Get a pointer to the y displacement data starting from slice iz.
	inline float32* CFloat32DVFData3DMemory::getDispY(size_t iz) {
		ASTRA_ASSERT(m_bInitialized);
		return m_ppfData2Dy[iz];
	}
	inline const float32* CFloat32DVFData3DMemory::getDispYConst(size_t iz) const {
		ASTRA_ASSERT(m_bInitialized);
		return (const float32*)m_ppfData2Dy[iz];
	}
	//----------------------------------------------------------------------------------------
	// Get a pointer to the y displacement data starting from slice iz.
	inline float32* CFloat32DVFData3DMemory::getDispZ(size_t iz) {
		ASTRA_ASSERT(m_bInitialized);
		return m_ppfData2Dz[iz];
	}
	inline const float32* CFloat32DVFData3DMemory::getDispZConst(size_t iz) const {
		ASTRA_ASSERT(m_bInitialized);
		return (const float32*)m_ppfData2Dz[iz];
	}
} // end namespace

#endif