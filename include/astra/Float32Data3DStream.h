/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#ifndef _INC_ASTRA_FLOAT32DATA3DSTREAM
#define _INC_ASTRA_FLOAT32DATA3DSTREAM

#include <future>

#include "Globals.h"
#include "Float32Data3D.h"
#include "Float32DataSource.h"

namespace astra {

	/**
	* This class represents a three-dimensional block of float32ing point data.
	* It contains member functions for accessing this data. Not all functionality
	* is provided from the in-memory data objects.
	* The data block is "owned" by the class, meaning that the class is
	* responsible for deallocation of the memory involved.
	* This data will be read from a file and may therefore not be available immediately.
	*/
	class _AstraExport CFloat32Data3DStream : public virtual CFloat32Data3D {

	protected:
		mutable CFloat32DataBufferedFile* m_pSource;

		CFloat32Data3DStream();

		bool _initialize(CFloat32DataBufferedFile* _pSource);
	public:
		/** Get the maximum number of slices alom te Z axis of the data block that can be accessed at once.
		*
		* Some objects only keep a limited number of conscutive slices in memory at once.
		* The value returned by this function indicates how many slices are available.
		* Switchin sets of slices will happen in a transparant way if they are fetched one by one.
		*
		* @return maximum numer of consecutive slices stored in memory
		*/
		virtual int getConsecutiveDepth() const;

		std::size_t getBufferSize() const;

		CFloat32DataSource* getDataSource() const;

		float32* getSlice(int _iSliceIndex) const;

		void writeSlice(int _iz, const float32* _pData);

		float32 getDataPoint(int _iIndex) const;

		void setDataPoint(int _iIndex, float32 _fValue);

		virtual ~CFloat32Data3DStream();
	};
} // end namespace

#endif