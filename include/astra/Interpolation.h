/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#ifndef _INC_ASTRA_INTERPOLATION
#define _INC_ASTRA_INTERPOLATION

#include "Globals.h"


namespace astra {
	enum EInterpolationMode {
		NEAREST_NEIGHBOR = 0,
		LINEAR = 1,
		CUBICBSPLINE = 2,
		DEFAULT = 1
	};
	template<EInterpolationMode _t>
	struct SInterpolate {

	};

	template<>
	struct SInterpolate<NEAREST_NEIGHBOR> {
		static const int kernelsize;
		static float32 interp(float32 x, float32* arg);
		static float32 calcpart(float32 x, float32 v);
	};

	template<>
	struct SInterpolate<LINEAR> {
		static const int kernelsize;
		static float32 interp(float32 x, float32* arg);
		static float32 calcpart(float32 x, float32 v);
	};

	template<>
	struct SInterpolate<CUBICBSPLINE> {
		static const int kernelsize;
		static float32 interp(float32 x, float32* arg);
		static float32 calcpart(float32 x, float32 v);
	};
}

#endif