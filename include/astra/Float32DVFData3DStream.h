/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#ifndef _INC_ASTRA_FLOAT32DVFDATA3DSTREAM
#define _INC_ASTRA_FLOAT32DVFDATA3DSTREAM

#include "Globals.h"
#include "Float32DVFData3D.h"
#include "Float32DataSource.h"

namespace astra {

	class _AstraExport CFloat32DVFData3DStream : public CFloat32DVFData3D {
	protected:
		mutable CFloat32DataMultiBufferedFile* m_pSource;

		bool _initialize(CFloat32DataMultiBufferedFile* _pSource);
	public:
		CFloat32DVFData3DStream(CFloat32DataMultiBufferedFile* _pSource);
		/** Get the maximum number of slices alom te Z axis of the data block that can be accessed at once.
		*
		* Some objects only keep a limited number of conscutive slices in memory at once.
		* The value returned by this function indicates how many slices are available.
		* Switchin sets of slices will happen in a transparant way if they are fetched one by one.
		*
		* @return maximum numer of consecutive slices stored in memory
		*/
		virtual int getConsecutiveDepth() const;

		/** Get a pointer to the x displacement data starting from slice z.
		*/
		virtual float32* getDispX(size_t _iz);
		virtual const float32* getDispXConst(size_t _iz) const;

		/** Get a pointer to the y displacement data starting from slice z.
		*/
		virtual float32* getDispY(size_t _iz);
		virtual const float32* getDispYConst(size_t _iz) const;

		/** Get a pointer to the y displacement data starting from slice z.
		*/
		virtual float32* getDispZ(size_t _iz);
		virtual const float32* getDispZConst(size_t _iz) const;
	};

} // end namespace

#endif