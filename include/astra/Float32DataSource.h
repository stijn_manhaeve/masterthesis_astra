/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#ifndef _INC_ASTRA_FLOAT32DATASOURCE
#define _INC_ASTRA_FLOAT32DATASOURCE

#include "Globals.h"
#include <string>
#include <fstream>
#include <cstdint>
#include <vector>
#include <future>

namespace astra {
	class _AstraExport CFloat32DataSource {
	public:
		CFloat32DataSource();
		virtual ~CFloat32DataSource();
		enum ECapability { NONE = 0,	///< This source has no reading or writing capabilities
						READ = 1,		///< This source can read data
						WRITE = 2,		///< This source can write data to new files
						OVERWRITE = 3	///< This source can overwrite existing data
					 };
		virtual void readData(std::size_t _iStart, std::size_t _iAmount, float32* _pfData) = 0;
		virtual void writeData(std::size_t _iStart, std::size_t _iAmount, const float32* _pfData) = 0;
		virtual int readDimensionCount() = 0;
		virtual int readSize() = 0;
		virtual int readDimensionSize(int _iDim) = 0;
		virtual void setDimensionCount(int _iDims) = 0;
		virtual void setDimensionSize(int _iDim, int _iSize) = 0;
		virtual ECapability getCapability() = 0;
		virtual CFloat32DataSource* makeCopy() const = 0;
		bool isInitialized() const;
	protected:
		bool m_bInitialized;
		ECapability m_eMode;	// operation mode when opening the file
	};

	inline bool CFloat32DataSource::isInitialized() const { return m_bInitialized; }

	class _AstraExport CFloat32DataFile : public CFloat32DataSource {
	protected:
		std::string m_sFileName;	///< The name of the file currently opened
		std::FILE* m_pFile;
	public:
		CFloat32DataFile();
		virtual ~CFloat32DataFile();
		virtual void open(std::string _sFileName, CFloat32DataSource::ECapability _eMode);
		virtual void close();
	};

	/**
	* This data source expects the data to be in binary encoded with the same endianness as the machine.
	* It is the most basic data encoding:
	* uint32 n: number of dimensions
	* uint32[n]: size in each dimension.
	* float32[]: The uncompressed data in column major ordering
	*/
	class _AstraExport CFloat32RawDataFile : public  CFloat32DataFile {
	protected:
		std::uint32_t m_uiDimensions;
		std::vector<std::uint32_t> m_vSizes;
		int m_iSize;
		std::int64_t m_offset;
	public:
		CFloat32RawDataFile();
		virtual ~CFloat32RawDataFile();
		virtual void open(std::string _sFileName, CFloat32DataSource::ECapability _eMode);
		virtual void close();
		virtual void readData(std::size_t _iStart, std::size_t _iAmount, float32* _pfData);
		virtual void writeData(std::size_t _iStart, std::size_t _iAmount, const float32* _pfData);
		virtual int readDimensionCount();
		virtual int readSize();
		virtual int readDimensionSize(int _iDim);
		virtual void setDimensionCount(int _iDims);
		virtual void setDimensionSize(int _iDim, int _iSize);
		virtual ECapability getCapability();
		virtual CFloat32DataSource* makeCopy() const;
	};

	template<template<typename> class FutureType = std::future>
	struct CFloat32DataBuffer {
		std::size_t m_iCurrentHead;
		std::size_t m_iBaseOffset;
		float32* m_ppfBuffers[2];
		FutureType<void> m_future;
		unsigned char m_changedFlags;
		CFloat32DataSource* m_pSource;

		CFloat32DataBuffer() : m_iCurrentHead(0),
			m_iBaseOffset(0),
			m_ppfBuffers(),
			m_changedFlags(0),
			m_pSource(0) {

		}
	};

	/**
	* This data source will turn a regular Data Source into one that buffers the data along the last dimension.
	*/
	class _AstraExport CFloat32DataBufferedFile {
	protected:
		bool m_bInitialized;
		bool m_bChanged;			///whether or not the current data in buffer had been changed
		std::size_t m_iBufferSize;	///size of buffers in slices
		std::size_t m_iReadSize;	///size of buffers in float32
		std::size_t m_iSliceSize;	///size of a single slice in float32
		std::size_t m_iSize;		///size of the entire image in float32
		CFloat32DataBuffer<std::future> m_buffer;

		static const int iDefaultBufferSize = 5;	///default buffer size in slices

		void _allocate();
		void _freeData();
		void _clear();
		void _unInit();
		void _update2ndBuffer(std::size_t _iWriteStart, std::size_t _iReadStart);
	public:
		CFloat32DataBufferedFile();
		CFloat32DataBufferedFile(CFloat32DataSource* _pSource);
		~CFloat32DataBufferedFile();
		bool initialize(CFloat32DataSource* _pSource);
		/**
		* Returns a pointer to the internal buffer.
		* note that a consecutive read may overwrite this data.
		*/
		virtual float32* readSlice(std::size_t _iSliceIndex);

		//note: very slow!
		float32 readValue(std::size_t _iValueIndex);
		void writeSlice(std::size_t _iSliceIndex, const float32* _pfData);
		//note: very slow!
		void writeValue(std::size_t _iValueIndex, float32 _fValue);

		int readDimensionCount();
		int readSize();
		int readDimensionSize(int _iDim);
		CFloat32DataSource::ECapability getCapability();

		std::size_t getBufferSize() const;
		void setBufferSize(std::size_t _iBufferSize);
	};

	//----------------------------------------------------------------------------------------
	// Set the buffer size.
	inline void CFloat32DataBufferedFile::setBufferSize(std::size_t _iBufferSize) {
		// Changing the buffer size should be impossible when the buffers are
		// already initialized;
		ASTRA_ASSERT(!m_bInitialized);
		m_iBufferSize = _iBufferSize;
	}

	//----------------------------------------------------------------------------------------
	// Retrieve the buffer size.
	inline std::size_t CFloat32DataBufferedFile::getBufferSize() const { return m_iBufferSize; }

	inline int CFloat32DataBufferedFile::readDimensionCount() {
		ASTRA_ASSERT(m_bInitialized);
		return m_buffer.m_pSource->readDimensionCount();
	}
	inline int CFloat32DataBufferedFile::readSize() {
		ASTRA_ASSERT(m_bInitialized);
		return m_buffer.m_pSource->readSize();
	}
	inline int CFloat32DataBufferedFile::readDimensionSize(int _iDim) {
		ASTRA_ASSERT(m_bInitialized);
		return m_buffer.m_pSource->readDimensionSize(_iDim);
	}
	inline CFloat32DataSource::ECapability CFloat32DataBufferedFile::getCapability() {
		ASTRA_ASSERT(m_bInitialized);
		return m_buffer.m_pSource->getCapability();
	}

	/**
	* This data source will turn a regular Data Source into one that buffers
	* the data along the two last dimensions.
	* Note that it is best for data with a very small last dimension, such as DVF data,
	* because a separate buffer will be kept for each part of the data with the same
	* index along this last dimension.
	*/
	class _AstraExport CFloat32DataMultiBufferedFile {
	protected:
		bool m_bInitialized;
		std::size_t m_iBufferSize;	///size of buffers in slices
		std::size_t m_iReadSize;	///size of buffers in float32
		std::size_t m_iSliceSize;	///size of a single slice in float32
		std::size_t m_iSize;		///size of the entire image in float32
		std::vector<CFloat32DataBuffer<std::shared_future>> m_buffers;
		CFloat32DataSource* m_pSource;

		static const int iDefaultBufferSize = 5;	///default buffer size in slices

		void _allocate();
		void _freeData();
		void _clear();
		void _unInit();

	public:
		CFloat32DataMultiBufferedFile();
		CFloat32DataMultiBufferedFile(CFloat32DataSource* _pSource);
		~CFloat32DataMultiBufferedFile();
		bool initialize(CFloat32DataSource* _pSource);
		/**
		* Returns a pointer to the internal buffer.
		* note that a consecutive read may overwrite this data.
		*/
		float32* readSlice(std::size_t _iBufferIndex, std::size_t _iSliceIndex);

		//note: very slow!
		float32 readValue(std::size_t _iBufferIndex, std::size_t _iValueIndex);
		void writeSlice(std::size_t _iBufferIndex, std::size_t _iSliceIndex, const float32* _pfData);
		//note: very slow!
		void writeValue(std::size_t _iBufferIndex, std::size_t _iValueIndex, float32 _fValue);

		int readDimensionCount();
		int readSize();
		int readDimensionSize(int _iDim);
		CFloat32DataSource::ECapability getCapability();

		std::size_t getBufferSize() const;
		void setBufferSize(std::size_t _iBufferSize);
	};

	inline std::size_t CFloat32DataMultiBufferedFile::getBufferSize() const { return m_iBufferSize; }
	inline void CFloat32DataMultiBufferedFile::setBufferSize(std::size_t _iBufferSize) {
		// Changing the buffer size should be impossible when the buffers are
		// already initialized;
		ASTRA_ASSERT(!m_bInitialized);
		m_iBufferSize = _iBufferSize;
	}

	inline int CFloat32DataMultiBufferedFile::readDimensionCount() {
		ASTRA_ASSERT(m_bInitialized);
		return m_pSource->readDimensionCount();
	}
	inline int CFloat32DataMultiBufferedFile::readSize() {
		ASTRA_ASSERT(m_bInitialized);
		return m_pSource->readSize();
	}
	inline int CFloat32DataMultiBufferedFile::readDimensionSize(int _iDim) {
		ASTRA_ASSERT(m_bInitialized);
		return m_pSource->readDimensionSize(_iDim);
	}
	inline CFloat32DataSource::ECapability CFloat32DataMultiBufferedFile::getCapability() {
		ASTRA_ASSERT(m_bInitialized);
		return m_pSource->getCapability();
	}
} // end namespace

#endif