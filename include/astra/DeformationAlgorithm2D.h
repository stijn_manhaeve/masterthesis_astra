/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#ifndef _INC_ASTRA_DEFORMATIONALGORITHM2D
#define _INC_ASTRA_DEFORMATIONALGORITHM2D

#include "Globals.h"
#include "Interpolation.h"
#include "Config.h"
#include "Algorithm.h"
#include "Float32DVFData2D.h"
#include "Float32VolumeData2D.h"

namespace astra {

	/**
	* This class contains the interface for an algorithm implementation.
	*/
	class _AstraExport CDeformationAlgorithm2D: public CAlgorithm {

	public:

		/** Default constructor, containing no code.
		*/
		CDeformationAlgorithm2D();

		/** Destructor.
		*/
		virtual ~CDeformationAlgorithm2D();

		/** Initialize the algorithm with a config object.
		*
		* @param _cfg Configuration Object
		* @return initialization successful?
		*/
		bool initialize(const Config& _cfg);
		bool initialize(CFloat32VolumeData2D* _pSource,
						CFloat32DVFData2D* _pDVF,
						CFloat32VolumeData2D* _pOutput);

		/** Get all information parameters
		*
		* @return map with all boost::any object
		*/
		map<string, boost::any> getInformation();

		/** Get a single piece of information represented as a boost::any
		*
		* @param _sIdentifier identifier string to specify which piece of information you want
		* @return boost::any object
		*/
		boost::any getInformation(std::string _sIdentifier);

		/** Perform a number of iterations.
		*
		* @param _iNrIterations amount of iterations to perform.
		*/
		void run(int _iNrIterations = 0);

		/** get a description of the class
		*
		* @return description string
		*/
		std::string description() const;

		/** Get Source Data
		*
		* @return source
		*/
		CFloat32VolumeData2D* getSource() const;

		/** Get Deformation Vector Field Data
		*
		* @return deformation vector field
		*/
		CFloat32DVFData2D* getDVF() const;

		/** Get Output Data
		*
		* @return output
		*/
		CFloat32VolumeData2D* getOutput() const;

		/** Get interpolation mode
		*
		* @return interpolation mode
		*/
		inline EInterpolationMode getInterpolationMode() const { return m_eMode; }

		/** Set interpolation mode
		*
		* @param _mode The new interpolation mode
		*/
		inline void setInterpolationMode(EInterpolationMode _mode) { m_eMode = _mode; }
	protected:
		CFloat32VolumeData2D* m_pSource;	///< The source image
		CFloat32DVFData2D* m_pDVF;			///< The vector deformation field
		CFloat32VolumeData2D* m_pOutput;	///< The output image

		EInterpolationMode m_eMode;
		/** Check this object.
		 *
		 * @return object initialized
		 */
		bool _check() const;

		virtual void runNearest() = 0;
		virtual void runLinear() = 0;
		virtual void runCubic() = 0;
	};

	// inline functions
	inline std::string CDeformationAlgorithm2D::description() const { return "2D deformation Algorithm"; };
	inline CFloat32VolumeData2D* CDeformationAlgorithm2D::getSource() const { return m_pSource; }
	inline CFloat32DVFData2D* CDeformationAlgorithm2D::getDVF() const { return m_pDVF; }
	inline CFloat32VolumeData2D* CDeformationAlgorithm2D::getOutput() const { return m_pOutput; }

	class _AstraExport CDeformationAlgorithm2DFwd : public CDeformationAlgorithm2D {

	protected:
		void runNearest();
		void runLinear();
		void runCubic();
	};

	class _AstraExport CDeformationAlgorithm2DBckwd : public CDeformationAlgorithm2D {

	protected:
		void runNearest();
		void runLinear();
		void runCubic();
	};


#ifdef _OPENMP
	class _AstraExport COpenMPDeformationAlgorithm2DBckwd : public CDeformationAlgorithm2D {

	protected:
		void runNearest();
		void runLinear();
		void runCubic();
	};
#endif
} // end namespace
#endif
