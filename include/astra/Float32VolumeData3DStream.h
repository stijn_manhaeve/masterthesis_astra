/*
-----------------------------------------------------------------------
Copyright: 2010-2015, iMinds-Vision Lab, University of Antwerp
2014-2015, CWI, Amsterdam

Contact: astra@uantwerpen.be
Website: http://sf.net/projects/astra-toolbox

This file is part of the ASTRA Toolbox.


The ASTRA Toolbox is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The ASTRA Toolbox is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the ASTRA Toolbox. If not, see <http://www.gnu.org/licenses/>.

-----------------------------------------------------------------------
$Id$
*/

#ifndef _INC_ASTRA_FLOAT32VOLUMEDATA3DSTREAM
#define _INC_ASTRA_FLOAT32VOLUMEDATA3DSTREAM

#include "Float32Data3DStream.h"
#include "VolumeGeometry3D.h"
#include "Float32VolumeData3D.h"

namespace astra {

	/**
	* This class represents three-dimensional Volume Data where the data is read from a stream.
	*/
	class _AstraExport CFloat32VolumeData3DStream : public CFloat32Data3DStream, public CFloat32VolumeData3D {
	public:

		/** Default constructor. Sets all numeric member variables to 0 and all pointer member variables to NULL.
		*
		* If an object is constructed using this default constructor, it must always be followed by a call
		* to one of the init() methods before the object can be used. Any use before calling init() is not allowed,
		* except calling the member function isInitialized().
		*
		*/
		CFloat32VolumeData3DStream();

		/** Constructor. Create an instance of the CFloat32VolumeData3DMemory class with initialization of the data.
		*
		* Memory is allocated for the data block and the contents of the memory pointed to by
		* _pfData is copied into the allocated memory.
		* The size of the data is determined by the specified volume geometry object.
		*
		* @param _pGeometry Volume Geometry object.  This object will be HARDCOPIED into this class.
		* @param _pfData pointer to a one-dimensional float32 data block
		*/
		CFloat32VolumeData3DStream(CVolumeGeometry3D* _pGeometry, CFloat32DataBufferedFile* _pDataSource);

		/** Destructor.
		*/
		virtual ~CFloat32VolumeData3DStream();

		/** Initialization. Initializes an instance of the CFloat32VolumeData3DSource class with initialization of the data.
		*
		* Memory is allocated for the data block and the contents of the memory pointed to by
		* _pfData is copied into the allocated memory.
		* The size of the data is determined by the specified volume geometry object.
		*
		* @param _pGeometry Volume Geometry of the data. This object will be HARDCOPIED into this class.
		* @param _pfData pointer to a one-dimensional float32 data block
		*/
		bool initialize(CVolumeGeometry3D* _pGeometry, CFloat32DataBufferedFile* _pDataSource);

		/** Which type is this class?
		*
		* @return DataType: VOLUME
		*/
		virtual CFloat32Data3D::EDataType getType() const;

		/** Get the volume geometry.
		*
		* @return pointer to volume geometry.
		*/
		CVolumeGeometry3D* getGeometry();

		/**
		* Gets a slice, containing all voxels with a given x (= column) index.
		*/
		CFloat32VolumeData2D * fetchSliceX(int _iColumnIndex) const;

		/**
		* Gets a slice, containing all voxels with a given y (= row) index.
		*/
		CFloat32VolumeData2D * fetchSliceY(int _iRowIndex) const;

		/**
		* Gets a slice, containing all voxels with a given z (= slice) index.
		*/
		CFloat32VolumeData2D * fetchSliceZ(int _iSliceIndex) const;

		/**
		* Allows direct read-only access to the data startin at slice z
		*/
		const float32* getDataZ(int _iSliceIndex) const;

		/**
		* Gets a slice, containing all voxels with a given x (= column) index.
		*/
		void returnSliceX(int _iColumnIndex, CFloat32VolumeData2D * _pSliceData);

		/**
		* Gets a slice, containing all voxels with a given y (= row) index.
		*/
		void returnSliceY(int _iRowIndex, CFloat32VolumeData2D * _pSliceData);

		/**
		* Copies data from a 2D slice containing all voxels with a given z (= slice) index to the
		* 3D  memory stored in this class.
		*/
		void returnSliceZ(int _iSliceIndex, CFloat32VolumeData2D * _pSliceData);

		/** This SLOW function returns a volume value stored a specific index in the array.
		*  Reading values in this way might cause a lot of unnecessary memory operations, don't
		*  use it in time-critical code.
		*
		*  @param _iIndex Index in the array if the data were stored completely in main memory
		*  @return The value the location specified by _iIndex
		*/
		virtual float32 getVoxelValue(int _iIndex);

		/** This SLOW function stores a voxel value at a specific index in the array.
		*  Writing values in this way might cause a lot of unnecessary memory operations, don't
		*  use it in time-critical code.
		*
		*  @param _iIndex Index in the array if the data were stored completely in main memory
		*  @param _fValue The value to be stored at the location specified by _iIndex
		*/
		virtual void setVoxelValue(int _iIndex, float32 _fValue);

		/**
		* Clamp data to minimum value
		*
		* @param _fMin minimum value
		* @return l-value
		*/
		virtual CFloat32Data3D& clampMin(float32& _fMin);

		/**
		* Clamp data to maximum value
		*
		* @param _fMax maximum value
		* @return l-value
		*/
		virtual CFloat32Data3D& clampMax(float32& _fMax);

		/**
		* Overloaded Operator: data = data (pointwise)
		*
		* @param _dataIn r-value
		* @return l-value
		*/
		CFloat32VolumeData3DStream& operator=(const CFloat32VolumeData3DStream& _dataIn);
	};

	//----------------------------------------------------------------------------------------
	// Get the projection geometry.
	inline CVolumeGeometry3D* CFloat32VolumeData3DStream::getGeometry() {
		ASTRA_ASSERT(m_bInitialized);
		return m_pGeometry;
	}

	//----------------------------------------------------------------------------------------
	// Get type
	inline CFloat32Data3D::EDataType CFloat32VolumeData3DStream::getType() const {
		return VOLUME;
	}


} // end namespace astra

#endif // _INC_ASTRA_FLOAT32VOLUMEDATA3DSTREAM
